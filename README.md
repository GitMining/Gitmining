# GitMining System

<img src="https://gitlab.com/GitMining/Gitmining/raw/4428c18dcd4d526dfd6e8cd0c1abb8468f37ed1f/src/main/resources/pics/GitMiningSystem.png">

## Code Analysis

In this section, I design a redundancy, analyst could decide the period between two snapshots.  
Every time analyst clicks "start" button to analyze a certain project, system would **clone** new repository and **insert a new file** in *MongoDB*,  
which do not delete the old record. So analyst could find differences between two version about code quality.

## Cooperation Analysis

After analyst clicks "Start" button, the browser would send target URLs that would be analyzed to GitMining server.  
GitMining would clone specific projects from matching platform such as GitHub, GitLab or others to the File System of Operation System. 
Having gotten specific projects, GitMining would extract project **Cooperation Information** that we want, and start analyzing code style.