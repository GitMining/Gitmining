package cn.nju.iSE.GitMining.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSONArray;

import cn.nju.iSE.GitMining.common.util.Downloader;

/**
 * @author : Magister
// * @fileName : cn.nju.iSE.GitMining.common.util.TestDownloader.java
 * 
 *           2018年4月21日 下午9:55:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDownloader {

	@Test
	public void testDownload() {
		String page = "http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL";
		String textType = "";
		System.out.println("Start Download.");
		String string = Downloader.getContent(page, textType);
		System.out.println(string);
	}
	
	@Test
	public void testConvertString2JsonObject() {
		String page = "http://114.215.188.21/api/v3/groups/1152/projects/?private_token=5oR7mkTB5o9izwB3dwxL";
		String textType = "";
		System.out.println("Start Download.");
		JSONArray element = Downloader.getJsonArray(page, textType);
		System.out.println(element);
//		JsonObject object = element.getAsJsonObject();
//		System.out.println(object);
	}
}
