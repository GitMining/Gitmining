package cn.nju.iSE.GitMining.common;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.model.system.Project;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.common.TestSometing.java
 * <p>
 * 2018年4月22日	下午1:00:40
 */

public class TestSometing_1 {

    @Test
    public void testUTCDate2Long() throws ParseException {
        String time = "2018-05-15T18:53:36.298+08:00";
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
        dFormat.setTimeZone(TimeZone.getDefault());
        System.out.println(dFormat.parse(time.substring(0, time.indexOf('+'))).getTime());
    }

    @Test
    public void testListLongContains() {
        List<Long> ls = new ArrayList<Long>();
        List<Long> l2 = new ArrayList<Long>();
        l2.add(1l);
        l2.add(2l);
        ls.add(1L);
        ls.add(2l);
        for (Long l : l2) {
            System.out.println(ls.contains(l));
        }
    }

    @Test
    public void testListStringContains() {
        List<String> ls = new ArrayList<String>();
        List<String> l2 = new ArrayList<String>();
        l2.add("afedsafeafxczxzgewhsdzxq1312rssdas1l");
        l2.add("jlzjxcihsksnalndoh1l1lmspodfsdmvmzxkhiw91");
        ls.add("afedsafeafxczxzgewhsdzxq1312rssdas1l");
        ls.add("jlzjxcihsksnalndoh1l1lmspodfsdmvmzxkhiw91");
        for (String l : l2) {
            System.out.println(ls.contains(l));
        }
    }

    @Test
    public void testSplit() {
        String route = "aaa/sss/ddd/";
        String string = route + "fghhj/aaa/.git";
        String[] str = string.split(route);
        System.out.println(str[1].substring(0, str[1].lastIndexOf("/")));
    }

    @Test
    public void testSbuString() {
        String str = ".java";
        System.out.println(str.substring(str.lastIndexOf('.')));
    }

    @Test
    public void testMultiMapInList() {
        @SuppressWarnings("rawtypes")
        List<Map> list = new ArrayList<>();
        Map<String, Integer> map1 = new HashMap<>();
        Map<String, List<String>> map2 = new HashMap<>();
        map1.put("a", 1);
        map1.put("b", 2);
        List<String> l1 = new ArrayList<>();
        l1.add("a1");
        l1.add("b1");
        List<String> l2 = new ArrayList<>();
        l2.add("a2");
        l2.add("b2");
        map2.put("map2_1", l1);
        map2.put("map2_2", l2);
        list.add(map1);
        list.add(map2);
        System.out.println(list);
    }

    @Test
    public void testArrayListAddAt() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(0, 1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(5, 6);
        System.out.println(list);
        System.out.println(list.size());
    }

    @Test
    public void testGetterSetter() {
        Project project = new Project();
        project.setFullName("fullname");
        project.setNamespace("namespace");
        project.setName("");
        System.out.println(project.getId());
    }

    @Test
    public void testStringSbuString() {
        String temp = "abcdefg";
        System.out.print(temp.lastIndexOf("bcd"));
    }

    @Test
    public void testList2Json() {
        List<SysPermission> strs = new ArrayList<>();
        SysPermission permission1 = new SysPermission(1, "p1", "user:add", "", null, 1, null, false);
        SysPermission permission2 = new SysPermission(2, "p2", "user:list", "", null, 2, null, false);
        SysPermission permission3 = new SysPermission(3, "p3", "user:delete", "", null, 3, null, false);
        strs.add(permission1);
        strs.add(permission2);
        strs.add(permission3);
        List<Integer> pIds = new ArrayList<>();
        pIds.add(1);
        pIds.add(2);
        pIds.add(3);
        SysRole role = new SysRole(1, "", "aaa", pIds);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("permissions", (JSONArray) JSON.toJSON(strs));
        System.out.println(jsonObject);

    }

    @Test
    public void testJson() {
        String json = "[{\"id\":1159,\"name\":\"myTest\",\"path\":\"myTest\",\"description\":\"\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/myTest\"},{\"id\":1158,\"name\":\"Living\",\"path\":\"Living\",\"description\":\"\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/Living\"},{\"id\":1152,\"name\":\"161250148_CSEIII\",\"path\":\"161250148_CSEIII\",\"description\":\"\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250148_CSEIII\"},{\"id\":1149,\"name\":\"141250103_GroupXTags\",\"path\":\"141250103_GroupXTags\",\"description\":\"141250103 任兵兵\\r\\n141250056 姜新炀\\r\\n141250015 陈亚军\\r\\n141250006 曹正斌\",\"avatar_url\":\"http://114.215.188.21/uploads/group/avatar/1149/PSX_20180130_093127.jpg\",\"web_url\":\"http://114.215.188.21/groups/141250103_GroupXTags\"},{\"id\":1146,\"name\":\"141250006_2018SE3\",\"path\":\"141250006_2018SE3\",\"description\":\"钱浩 131250015\\r\\n曹正斌 141250006\\r\\n陈亚军 141250015\\r\\n顾锦峰 141250040\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/141250006_2018SE3\"},{\"id\":1140,\"name\":\"151250062_secIII\",\"path\":\"151250062_secIII\",\"description\":\"\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/151250062_secIII\"},{\"id\":1135,\"name\":\"161250202_Discounts\",\"path\":\"161250202_Discounts\",\"description\":\"尚柯宇161250111\\r\\n宋金泽161250121\\r\\n肖云帆161250161\\r\\n张翔宇161250202\",\"avatar_url\":\"http://114.215.188.21/uploads/group/avatar/1135/%E6%9A%B4%E9%A3%8E%E6%88%AA%E5%9B%BE201691981463859.jpg\",\"web_url\":\"http://114.215.188.21/groups/161250202_Discounts\"},{\"id\":1134,\"name\":\"161250085_COUNTStar\",\"path\":\"161250085_COUNTStar\",\"description\":\"陆梦霖161250085\\r\\n刘雪莹161250079\\r\\n马帝161250087\\r\\n马学源161250089\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250085_COUNTStar\"},{\"id\":1124,\"name\":\"151250098_CrowdsourcingTags\",\"path\":\"151250098_CrowdsourcingTags\",\"description\":\"刘伟 151250098\\r\\n黎奥邦 151250075\\r\\n邱昌政 151250120\\r\\n何阳 151250053\\r\\n\\r\\n\\r\\n\\r\\n\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/151250098_CrowdsourcingTags\"},{\"id\":1118,\"name\":\"161250040_KylinSECIII\",\"path\":\"161250040_KylinSECIII\",\"description\":\"洪铨健161250040\\r\\n刘汉乙161250072\\r\\n林鹏161250069\\r\\n殷煜161250184\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250040_KylinSECIII\"},{\"id\":1115,\"name\":\"161250147_NeverGiveUp\",\"path\":\"161250147_NeverGiveUp\",\"description\":\"王一名 161250147\\r\\n王中石 151250153\\r\\n吴俊锋 161250154\\r\\n严浩浩 161250175\\r\\n\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250147_NeverGiveUp\"},{\"id\":1109,\"name\":\"151250027_se3\",\"path\":\"151250027_se3\",\"description\":\"崔伯暘 151250027\\r\\n张浩兴 161250195\\r\\n卢乐凡 131250206\\r\\n王宇 151250151\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/151250027_se3\"},{\"id\":1103,\"name\":\"161250075_lt\",\"path\":\"161250075_lt\",\"description\":\"161250073 刘露莹\\r\\n161250075 刘芮洪\\r\\n161250076 刘苏豫\\r\\n161250077 刘天尺\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250075_lt\"},{\"id\":1102,\"name\":\"161250128_STZ\",\"path\":\"161250128_STZ\",\"description\":\"陈光明161250008\\r\\n唐诗林161250128\\r\\n童正阳161250131\\r\\n张杰华161250196\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250128_STZ\"},{\"id\":1099,\"name\":\"161250178_ArgusTags\",\"path\":\"161250178_ArgusTags\",\"description\":\"杨屹劼161250178\\r\\n于永琦161250187\\r\\n张逸晟161250203\\r\\n张雨豪161250206\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250178_ArgusTags\"},{\"id\":1098,\"name\":\"161250102_AlphaCat\",\"path\":\"161250102_AlphaCat\",\"description\":\"钱美缘161250102\\r\\n郝睿161250037\\r\\n恽叶霄161250192\\r\\n万嘉雯151099124\",\"avatar_url\":\"http://114.215.188.21/uploads/group/avatar/1098/psb.jpg\",\"web_url\":\"http://114.215.188.21/groups/161250102_AlphaCat\"},{\"id\":1096,\"name\":\"161250144_CWZZ\",\"path\":\"161250144_CWZZ\",\"description\":\"曹振飞 161250005\\r\\n王旭   161250144\\r\\n张萍   161250201\\r\\n钟洁   161250211\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250144_CWZZ\"},{\"id\":1095,\"name\":\"161250127_COUNTS\",\"path\":\"161250127_COUNTS\",\"description\":\"唐佳未161250127\\r\\n许杨161250173\\r\\n王轩161250145\\r\\n毛瑜161250090\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250127_COUNTS\"},{\"id\":1094,\"name\":\"161250013_Tager\",\"path\":\"161250013_Tager\",\"description\":\"陈思彤 161250013\\r\\n刘智彪 161250083\\r\\n吴光彧 161250153\\r\\n周传林 161250214\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250013_Tager\"},{\"id\":1091,\"name\":\"161250143_SocialEngineers\",\"path\":\"161250143_SocialEngineers\",\"description\":\"王瑞华 161250143\\r\\n石胜杰 161250118\\r\\n高毓彬 161250028\\r\\n于瑞召 161250185\",\"avatar_url\":null,\"web_url\":\"http://114.215.188.21/groups/161250143_SocialEngineers\"}]";
        JSONArray element;
        element = JSON.parseArray(json);
        System.out.println(element);
    }

    @Test
    public void testTrim() {
        String s = "\n";
        System.out.println(s.trim().length() == 0);
        System.out.println(s.equals("\n"));
    }

    @Test
    public void testLambda() {
        Project project = new Project();
        project.setId(0);
        Project project1 = new Project();
        project1.setId(1);
        Project project2 = new Project();
        project2.setId(2);
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        projects.add(project1);
        projects.add(project2);
        projects.forEach(p -> System.out.println(p.getId()));
    }

    @Test
    public void testString() {
        String str1 = "/opt/GitMining/repositories/161250013_Tager/ImageMaster_Phase_I";
        String str2 = "opt.GitMining.repositories.161250013_Tager.ImageMaster_Phase_I.ImageMaster" +
                ".src.main.service.IOServiceImpl.read";
        str1 = str1.replaceAll("/", ".").substring(1);
        System.out.println("After replace: " + str1);
        System.out.println(str2.substring(str1.length() + 1));
    }
}
