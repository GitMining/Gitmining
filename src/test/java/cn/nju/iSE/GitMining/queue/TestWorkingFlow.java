package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.GroupLogic;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestWorkingFlow {
    @Autowired
    private CloneSender cloneSender;

    @Autowired
    private GroupLogic groupLogic;

    private ProjectVO projectVO;

    private ProjectVO projectVO1;

    private ProjectVO projectVO2;

    @Before
    public void setup(){
        GroupVO groupVO = new GroupVO();
        groupVO.setPlatform(From.GitLab_SECIII);
        groupVO.setName("161250198_COUNTS");
        groupVO.setId(1004L);
        Long[] IDS = {1128L, 907L,801L,799L,798L,797L};
        List<Long> ids = Arrays.asList(IDS);
        groupVO.setProjectIds(ids);
        groupLogic.saveUncontrolledGroup(groupVO);

        projectVO = new ProjectVO();
        projectVO.setName("PictureTag_Phase_I");
        projectVO.setNamespace("161250198_COUNTS");
        projectVO.setUrl("http://114.215.188.21/161250198_COUNTS/PictureTag_Phase_Test.git");
        projectVO.setId(907);
        projectVO.setCreateTime(1521043969398L);

        projectVO1 = new ProjectVO();
        projectVO1.setName("PictureTag_Phase_II");
        projectVO1.setNamespace("161250198_COUNTS");
        projectVO1.setUrl("http://114.215.188.21/161250198_COUNTS/PictureTag_Phase_II.git");
        projectVO1.setId(798);
        projectVO1.setCreateTime(1520767467070L);

        projectVO2 = new ProjectVO();
        projectVO2.setName("PictureTag_Phase_III");
        projectVO2.setNamespace("161250198_COUNTS");
        projectVO2.setUrl("http://114.215.188.21/161250198_COUNTS/PictureTag_Phase_III.git");
        projectVO2.setId(1128);
        projectVO2.setCreateTime(1526381616298L);
    }

    @Test
    public void integrationTest() throws InterruptedException {
        System.out.println(cloneSender.send(projectVO));
        System.out.println(cloneSender.send(projectVO1));
        System.out.println(cloneSender.send(projectVO2));
        Thread.sleep(1000*600);
    }
}
