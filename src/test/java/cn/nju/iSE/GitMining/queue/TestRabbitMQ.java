package cn.nju.iSE.GitMining.queue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//@Deprecated
public class TestRabbitMQ {
    @Autowired
    private TestSender testSender;

    @Test
    public void testSendAndReceive(){
        String content = "This is my first RabbitMQ for springboot test. #";
        for(int i = 0; i < 10; i++)
            testSender.send(content + i);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            System.out.println("Test end");
        }
    }
}
