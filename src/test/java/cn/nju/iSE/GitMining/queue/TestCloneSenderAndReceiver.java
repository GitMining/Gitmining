package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.service.code.ProjectLinesOfCode;
import cn.nju.iSE.GitMining.service.system.ProjectService;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.GroupLogic;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCloneSenderAndReceiver {

    @Autowired
    private CloneSender cloneSender;

    @Autowired
    private GroupLogic groupLogic;

    @Autowired
    private ProjectLinesOfCode projectLinesOfCode;

    private ProjectVO projectVO;

    private ProjectVO project1VO;

    private ProjectVO project2VO;

    @Before
    public void setUp(){
        System.out.println("Setup group basic information.");
        GroupVO groupVO = new GroupVO();
        groupVO.setId(1149);
        groupVO.setName("141250103_GroupXTags");
        groupVO.setPlatform(From.GitLab_SECIII);
        List<Long> projectIds = new ArrayList<>();
        projectIds.add(1002L);
        projectIds.add(1000L);
        projectIds.add(999L);
        projectIds.add(996L);
        projectIds.add(995L);
        projectIds.add(994L);
        groupVO.setProjectIds(projectIds);
        groupLogic.saveUncontrolledGroup(groupVO);

        System.out.println("Setup project basic information.");
        projectVO = new ProjectVO();
        projectVO.setId(994);
        projectVO.setCreateTime(1521687429582L);
        projectVO.setName("XTags_Phase_I");
        projectVO.setNamespace("141250103_GroupXTags");
        projectVO.setUrl("http://114.215.188.21/141250103_GroupXTags/XTags_Phase_I.git");

        System.out.println("Setup project1 basic information.");
        project1VO = new ProjectVO();
        project1VO.setId(995);
        project1VO.setCreateTime(1521687472139L);
        project1VO.setName("XTags_Phase_II");
        project1VO.setNamespace("141250103_GroupXTags");
        project1VO.setUrl("http://114.215.188.21/141250103_GroupXTags/XTags_Phase_II.git");

        System.out.println("Setup project2 basic information.");
        project2VO = new ProjectVO();
        project2VO.setId(996);
        project2VO.setCreateTime(1521687506006L);
        project2VO.setName("XTags_Phase_III");
        project2VO.setNamespace("141250103_GroupXTags");
        project2VO.setUrl("http://114.215.188.21/141250103_GroupXTags/XTags_Phase_III.git");

    }

    @Test
    public void testProcess() throws InterruptedException {
        System.out.println(cloneSender.send(projectVO));
        System.out.println(cloneSender.send(project1VO));
        System.out.println(cloneSender.send(project2VO));
        Thread.sleep(1000*600);
    }
}
