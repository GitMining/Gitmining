package cn.nju.iSE.GitMining.logic.code;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.web.data.code.ProjectLDVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.code.TestProjectLDImpl.java
 *  
 *  2018年3月30日	上午10:56:41  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectLDImpl {

	@Autowired
	private ProjectLDLogic projectLDLogic;
	
	@Test
	public void testGetCheckResult() {
		ProjectLDVO projectLDVO = projectLDLogic.getCheckResult(1);
		System.out.println(projectLDVO);
	}
}
