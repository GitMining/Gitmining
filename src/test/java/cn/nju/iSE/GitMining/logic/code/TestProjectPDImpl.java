package cn.nju.iSE.GitMining.logic.code;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.web.data.code.ProjectPDVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectPDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.code.TestProjectPDImpl.java
 *  
 *  2018年3月30日	下午8:24:58  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectPDImpl {

	@Autowired
	ProjectPDLogic projectPDLogic;
	
	@Test
	public void testCheckResult() {
		ProjectPDVO projectPDVO =projectPDLogic.getCheckResult(1);
		System.out.println(projectPDVO);
	}
}
