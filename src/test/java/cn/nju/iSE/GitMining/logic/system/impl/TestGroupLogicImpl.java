package cn.nju.iSE.GitMining.logic.system.impl;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.logic.system.GroupLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.system.impl.TestGroupLogicImpl.java
 *  
 *  2018年4月25日	下午3:55:56  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestGroupLogicImpl {

	@Autowired
	private GroupLogic groupLogic;
	
	private String url = "http://114.215.188.21/api/v3/groups";
	
	@Test
	public void testGetUncontrolledGroups() {
		List<GroupVO> groupVOs = groupLogic.getUncontrolledGroups(url, "", Constant.token);
		System.out.println(groupVOs);
		System.out.println(groupVOs.size());
	}

	@Test
	public void testGetGroupsWithScore(){
		long start = 1528273193180L;
		long end = 1528273193190L;
		JSONArray groups = groupLogic.getGroupsWithScore(start, end);
		System.out.println(groups);
	}
}
