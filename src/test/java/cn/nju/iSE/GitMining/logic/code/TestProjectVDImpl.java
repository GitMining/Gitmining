package cn.nju.iSE.GitMining.logic.code;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.web.data.code.ProjectVDVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectVDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.code.TestProjectVDImpl.java
 *  
 *  2018年3月30日	下午11:09:55  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectVDImpl {

	@Autowired
	private ProjectVDLogic projectVDLogic;
	
	@Test
	public void testCheckResult() {
		ProjectVDVO projectVDVO =  projectVDLogic.getCheckResult(1);
		System.out.println(projectVDVO);
	}
	
}
