package cn.nju.iSE.GitMining.logic.code;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.web.data.code.ProjectLOCVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLOCLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.code.TestProjectLOCImpl.java
 *  
 *  2018年3月29日	下午5:40:06  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectLOCImpl {

	@Autowired
	ProjectLOCLogic projectLOCLogic;
	
	@Test
	public void test() {
		ProjectLOCVO projectLOCVO =projectLOCLogic.getCheckResult(1);
		System.out.println(projectLOCVO);
	}
}
