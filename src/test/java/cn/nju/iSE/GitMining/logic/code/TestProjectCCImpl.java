package cn.nju.iSE.GitMining.logic.code;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.web.data.code.ProjectCCVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectCCLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.logic.code.TestProjectCCImpl.java
 *  
 *  2018年3月30日	上午9:51:07  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectCCImpl {

	
	@Autowired()
	ProjectCCLogic projectCCLogic;
	
	@Test
	public void testGetCheckResultById() {
		ProjectCCVO projectCCVO = projectCCLogic.getCheckResult(1);
		System.out.println(projectCCVO);
	}
}
