package cn.nju.iSE.GitMining.integration;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import iSESniper.cooperation.commit.extractor.CommitsExtractor;
import iSESniper.cooperation.entity.Commit;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.integration.ProjectCooperationIntegrationTestI.java
 *  
 *  2018年4月26日	下午3:42:16  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectCooperationIntegrationTestI {

	private CommitsExtractor commitsExtractor;
	
	public void testProjectSibling() {
		commitsExtractor = new CommitsExtractor("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
		
	}
}
