package cn.nju.iSE.GitMining.ctrl;

import cn.nju.iSE.GitMining.web.ctrl.system.SystemController;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSystemCtrl {
    @Autowired
    private SystemController systemController;

    @Test
    public void testListProjectCommitWithState(){
        JSONObject ans = systemController.listProjectCommitWithState("161250040_KylinSECIII");
        System.out.println(ans);
    }
}
