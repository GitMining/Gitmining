package cn.nju.iSE.GitMining.service.system.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.mongo.cooperation.CommitDetailMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.ProjectDetailMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.ProjectMongoRepository;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.service.system.CommitsExtract;
import cn.nju.iSE.GitMining.service.system.GroupDownload;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.service.system.ProjectDetailService;
import cn.nju.iSE.GitMining.service.system.ProjectDownload;
import cn.nju.iSE.GitMining.service.system.ProjectService;
import cn.nju.iSE.GitMining.service.system.RepositoryClone;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.service.system.impl.MultiPartTestDetailAndCommitService.java
 * 
 *           2018年4月23日 下午6:12:21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MultiPartTestDetailAndCommitService {

	@Autowired
	private ProjectDetailService projectDetailService;

	@Autowired
	private CommitDetailService commitDetailService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private GroupDownload groupDownload;

	@Autowired
	private ProjectDownload projectDownload;

	@Autowired
	private RepositoryClone repositoryClone;

	@Autowired
	private CommitsExtract commitExtract;

	@Autowired
	private GroupMongoRepository groupMongoRepository;

	@Autowired
	private ProjectMongoRepository projectMongoRepository;

	@Autowired
	private ProjectDetailMongoRepository projectDetailMongoRepository;

	@Autowired
	private CommitDetailMongoRepository commitDetailMongoRepository;

	String url = "http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL&per_page=1";

	String charset = "";

	String token = "5oR7mkTB5o9izwB3dwxL";

	@Before
	public void setupContext() {
		System.out.println("===================== BEFORE TEST ========================");
		groupMongoRepository.deleteAll();
		projectMongoRepository.deleteAll();
		projectDetailMongoRepository.deleteAll();
		commitDetailMongoRepository.deleteAll();
		List<Group> groups = groupDownload.downloadGroups(url, charset);
		for (Group g : groups) {
			System.out.println("Download Group: " + g);
			List<Project> projects = projectDownload.getGroupProjects(g, From.GitLab_SECIII, token);
			List<Long> pIds = new ArrayList<Long>();
			for (Project p : projects) {
				System.out.println("Start Clone Repository: " + p.getName());
				pIds.add(p.getId());
				// Clone project.
				repositoryClone.clone(p);
				String path = p.getFullName();
				// Capture project's Commits' id.
				projectService.captureCommitIds(p);
				System.out.println(p);
				// Get commit detail information for certain project.
				List<CommitDetail> commitDetails = commitExtract.projectCommitsExtract(path);
				commitDetailService.save(commitDetails);
				// Get certain project detail information.
				ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(p);
				projectDetailService.save(projectDetail);
			}
			g.setProjectIds(pIds);
			projectService.save(projects);
			groupService.save(g);
		}
	}

	@Test
	public void test() {
		System.out.println("===================== START TEST ========================");
		List<Group> groups = groupService.getGroups();
		for (Group g : groups) {
			System.out.println("Database Group: " + g);
			for (Long pid : g.getProjectIds()) {
				ProjectDetail pDetail = projectDetailService.getProjectDetail(pid);
				System.out.println("Project Detail Find By id " + pDetail);
				Project project = projectService.getProject(pid);
				int i = 0;
				for (String cid : project.getCommitIds()) {
					i++;
					System.out.println(i + ": " + commitDetailService.getCommit(cid).toString());
				}
				System.out.println("Project Commit Number: " + 
						commitDetailService.getCommits(pDetail.getNamespace() + "/" + pDetail.getName()).size());
			}
			for(ProjectDetail pd: projectDetailService.getprojectDetails(g.getOwnerName()))
				System.out.println("Project Detail Find By namespace " + pd);
		}
	}
	/*
	 * */

	@After
	public void celan() {
		System.out.println("===================== TEST END ========================");
		groupMongoRepository.deleteAll();
		projectMongoRepository.deleteAll();
		projectDetailMongoRepository.deleteAll();
		commitDetailMongoRepository.deleteAll();
		System.out.println("Group number: " + groupService.getGroups().size());
		System.out.println("Project number: " + projectService.getProjects().size());
		System.out.println("Commit number: " + commitDetailMongoRepository.count());
		System.out.println("Project Detail number: " + projectDetailMongoRepository.count());
	}
}
