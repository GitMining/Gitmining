package cn.nju.iSE.GitMining.service.system.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.service.system.GroupDownload;
import cn.nju.iSE.GitMining.service.system.ProjectDownload;
import cn.nju.iSE.GitMining.service.system.RepositoryClone;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.service.system.impl.TestRepositoryCloneImpl.java
 * 
 *           2018年4月22日 下午3:31:31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRepositoryCloneImpl {

	@Autowired
	private RepositoryClone repositoryClone;

	@Autowired
	private GroupDownload groupDownload;

	@Autowired
	private ProjectDownload projectDownload;

	private 
	List<Project> repos = new ArrayList<Project>();
	
	@Before
	public void setupContext() {
		List<Group> groups = groupDownload
				.downloadGroups("http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL&per_page=1", "utf-8");
		System.out.println("Groups:");
		List<Project> projects;
		for(Group g: groups) {
			System.out.println(g);
			projects = projectDownload.getGroupProjects(g, From.GitLab_SECIII, "5oR7mkTB5o9izwB3dwxL");
			System.out.println(g.getOwnerName() + " projects:");
			for(Project p: projects) {
				System.out.println(p);
				repos.add(p);
			}
		}
	}

	@Test
	public void testClone() {
		System.out.println("Start Clone");
		for(Project p: repos) {
			System.out.println("Start cloning " + p.getFullName());
			repositoryClone.clone(p);
		}
	}
}
