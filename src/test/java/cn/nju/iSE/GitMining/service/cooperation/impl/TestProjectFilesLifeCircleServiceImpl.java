package cn.nju.iSE.GitMining.service.cooperation.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.service.system.ProjectService;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.cooperation.impl.TestProjectFilesLifeCircleServiceImpl.java
 *  
 *  2018年4月25日	下午8:13:04  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectFilesLifeCircleServiceImpl {

	@Autowired
	private CommitDetailService commitDetailService;
	
	@Autowired
	private ProjectFilesLifeCircleService projectFilesLifeCircleService;
	
	@Autowired
	private ProjectService projectService;
	
	private String projectName = "161250151_TagMakers/building";
	
	@Test
	public void testExtractAndSave() {
		List<CommitDetail> commitDetails = commitDetailService.getCommits(projectName);		
		Project project = projectService.getProject(1020);
		System.out.println(projectFilesLifeCircleService.extractAndSave(project, commitDetails));
	}
}
