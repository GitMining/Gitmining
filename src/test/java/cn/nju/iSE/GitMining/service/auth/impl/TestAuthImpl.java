package cn.nju.iSE.GitMining.service.auth.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.mongo.auth.SysPermissionMongoRepository;
import cn.nju.iSE.GitMining.mongo.auth.SysRoleMongoRepository;
import cn.nju.iSE.GitMining.mongo.auth.UserInfoMongoRepository;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.service.auth.SysRoleService;
import cn.nju.iSE.GitMining.service.auth.UserInfoService;
import cn.nju.iSE.GitMining.web.constant.Route;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAuthImpl {

    @Autowired
    private SysPermissionService sysPermissionService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoMongoRepository userInfoMongoRepository;

    @Autowired
    private SysPermissionMongoRepository sysPermissionMongoRepository;

    @Autowired
    private SysRoleMongoRepository sysRoleMongoRepository;

    @Before
    public void setUp() {
        userInfoMongoRepository.deleteAll();
        sysPermissionMongoRepository.deleteAll();
        sysRoleMongoRepository.deleteAll();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("permissionName", "GroupAdmin_get_controlled_list");
        jsonObject.put("permission", "group:list_controlled");
        jsonObject.put("url", Route.System.CertainPeriodGroups);
        jsonObject.put("resourceType", "button");
        jsonObject.put("parentId", 0);
        jsonObject.put("parentIds", "0/");
        jsonObject.put("roleName", "admin");
        jsonObject.put("description", "Do it.");
        List<Integer> pIds = new ArrayList<>();
        pIds.add(1);
        jsonObject.put("permissionIds", pIds);
        sysPermissionService.addPermission(jsonObject);
        sysRoleService.addRole(jsonObject);
    }

    @Test
    public void testIntegration() {
    	JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", "gitmining");
        jsonObject.put("password", "gitmining");
        jsonObject.put("salt", "excellent");
        jsonObject.put("isAdmin", true);
        userInfoService.addUser(jsonObject);
        JSONObject userIno = userInfoService.checkUser("gitmining", "gitmining");
        System.out.print("UserInfo: \n");
        System.out.println(userIno);
        SysRole role = sysRoleService.getRole(userIno.getInteger("roleId"));
        JSONObject jsonArray = sysPermissionService.getPermissions(role);
        System.out.println(jsonArray);

    }
}
