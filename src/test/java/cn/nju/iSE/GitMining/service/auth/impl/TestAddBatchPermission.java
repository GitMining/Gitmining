package cn.nju.iSE.GitMining.service.auth.impl;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.service.auth.SysRoleService;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAddBatchPermission {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysPermissionService sysPermissionService;
    @Before
    public void addBatchPermissions(){
        SysPermission permission = new SysPermission();
        permission.setId(3);
        permission.setName("GroupAdmin_add_any_group");
        permission.setParentId(2);
        permission.setParentIds("0/2/");
        permission.setUrl(Route.System.AddNewGroups);
        permission.setPermission("group:super_add");
        permission.setAvailable(true);
        permission.setResourceType("button");
        List<SysPermission> permissions = new ArrayList<>();
        permissions.add(permission);
        SysPermission permission1 = new SysPermission(4, "GroupAdmin_add_group", "button",
                "", "group:single_add",
                2, "0/2/", true);
        permissions.add(permission1);
        SysPermission permission2 = new SysPermission(5, "GroupAdmin_detail", "table_row",
                Route.System.GetGroupProjects, "group:detail", 1, "0/1", true);
        permissions.add(permission2);
        SysPermission permission3 = new SysPermission(6, "GroupAdmin_members", "button",
                Route.System.GetGroupProjects, "group:members", 1, "0/1", true);
        permissions.add(permission3);
        sysPermissionService.addPermissions(permissions);
    }
    @Test
    public void testAddBatchPermsToRole(){
        System.out.println("==================== Before Append New Permission ====================");
        System.out.println(sysRoleService.getRole(1));
        int[] pIds = {3,4,5,6};
        sysRoleService.appendPermission(1, pIds);
        System.out.println("==================== After Append New Permission ====================");
        System.out.println(sysRoleService.getRole(1));
    }

}
