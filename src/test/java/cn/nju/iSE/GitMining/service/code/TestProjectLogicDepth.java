package cn.nju.iSE.GitMining.service.code;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import cn.nju.iSE.GitMining.mongo.code.ProjectLDMongoRepository;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.service.code.TestProjectLogicDepth.java
 * 
 *           2018年3月18日 下午1:57:16
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectLogicDepth {

	@Autowired
	ProjectLogicDepth projectLogicDepth;

	@Autowired
	ProjectLDMongoRepository projectLDMongoRepository;

	ProjectLD prjtLD = new ProjectLD();
	@Before
	public void setUp() {
		prjtLD.setId(1l);
		prjtLD.setProjectName("duplicate id");
		projectLDMongoRepository.deleteAll();
	}

	@Test
	public void test() {
		long id = projectLogicDepth.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook","groupTest", 1L);
		System.out.println(id);
		List<ProjectLD> projectLDs = projectLogicDepth
				.getProjectsByPth("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
		System.out.println(projectLDs);
	}
	
	@Test
	public void test1() {
		long id = projectLogicDepth.create("","groupTest", 1L);
		System.out.println(id);
	}
}
