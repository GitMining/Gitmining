package cn.nju.iSE.GitMining.service.auth.impl;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.service.auth.SysRoleService;
import cn.nju.iSE.GitMining.web.constant.Route;
import com.alibaba.fastjson.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAddPermission {
    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysPermissionService sysPermissionService;

    @Before
    public void newPermission() {
        System.out.println("==================== Before Insert New Permission ====================");
        System.out.println(sysPermissionService.getAllPermission());
        JSONObject jsonObject = new JSONObject();
        SysPermission permission = new SysPermission();
        permission.setId(7);
        permission.setResourceType("button");
        permission.setPermission("project:add_uncontrolled");
        permission.setUrl(Route.System.GetProjects);
        permission.setParentId(0);
        permission.setParentIds("0/");
        permission.setName("ProjectAdmin_get_uncontrolled_list");
        permission.setAvailable(true);
        sysPermissionService.addPermission(permission);
        System.out.println("==================== After Insert New Permission ====================");
        System.out.println(sysPermissionService.getAllPermission());
    }

    @Test
    public void testAddPermsToRole() {
        System.out.println("==================== Before Append New Permission ====================");
        System.out.println(sysRoleService.getRole(1));
        int[] pids = {1,2,3,4,5,6,7};
        sysRoleService.appendPermission(1, pids);
        System.out.println("==================== After Append New Permission ====================");
        System.out.println(sysRoleService.getRole(1));
    }
}
