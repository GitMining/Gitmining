package cn.nju.iSE.GitMining.service.cooperation.impl;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.service.cooperation.AuthorsCooperationService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.ProjectLogic;
import iSESniper.cooperation.entity.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAuthorCooperationServiceImpl {

    @Autowired
    private ProjectLogic projectLogic;

    @Autowired
    private CommitDetailService commitDetailService;

    @Autowired
    private AuthorsCooperationService authorsCooperationService;

    @Test
    public void testCountImportance(){
        String groupName = "161250119_ControllSECIII";
        List<ProjectVO> projects = projectLogic.getProjects(groupName);
        Map<ProjectVO, List<Map<Author, Double>>> diversities = new HashMap<>();
        for (ProjectVO project : projects) {
            List<CommitDetail> cDetails = commitDetailService.getCommits(groupName + "/" + project.getName());
            diversities.put(project, authorsCooperationService.getAuthorsImportance(cDetails));
        }
        System.out.print(diversities);
    }
}
