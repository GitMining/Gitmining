package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.service.code.ProjectCyclomaticComplexity;
import cn.nju.iSE.GitMining.service.code.ProjectDuplicationCode;
import cn.nju.iSE.GitMining.service.code.ProjectEmptyMethod;
import cn.nju.iSE.GitMining.service.code.ProjectLineOfAnnotation;
import cn.nju.iSE.GitMining.service.code.ProjectLinesOfCode;
import cn.nju.iSE.GitMining.service.code.ProjectLogicDepth;
import cn.nju.iSE.GitMining.service.code.ProjectParameters;
import cn.nju.iSE.GitMining.service.code.ProjectVariablesDeclaration;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.service.system.CommitsExtract;
import cn.nju.iSE.GitMining.service.system.ProjectDetailService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MultiTestForUpdate {

    @Autowired
    private CommitsExtract commitsExtract;

    @Autowired
    private CommitDetailService commitDetailService;

    @Autowired
    private ProjectDetailService projectDetailService;

    @Autowired
    private ProjectFilesLifeCircleService projectFilesLifeCircleService;

    @Autowired
    private ProjectCyclomaticComplexity projectCyclomaticComplexity;

    @Autowired
    private ProjectLogicDepth projectLogicDepth;

    @Autowired
    private ProjectLinesOfCode projectLinesOfCode;

    @Autowired
    private ProjectParameters projectParameters;

    @Autowired
    private ProjectVariablesDeclaration projectVariablesDeclaration;

    @Autowired
    private ProjectDuplicationCode projectDuplicationCode;

    @Autowired
    private ProjectLineOfAnnotation projectLineOfAnnotation;

    @Autowired
    private ProjectEmptyMethod projectEmptyMethod;
    @Test
    public void testIntegration(){
        String groupName = " 161250102_AlphaCat ";
        String projectName = "AlphaCat_Phase_II";
        Project project = new Project();
        project.setName("AlphaCat_Phase_II");
        project.setNamespace("161250102_AlphaCat");
        String path = "D:\\work\\GitMining\\Repos\\samples\\161250102_AlphaCat\\AlphaCat_Phase_II";
        project.setFullName(path);
        List<CommitDetail> commitDetails = commitsExtract.projectCommitsExtract(path);
        ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(project);
        // Update project_detail information.
        projectDetailService.update(projectDetail);
        // Append new commit detail information.
        commitDetailService.append(commitDetails);
        // Update files' life circle of project.
        projectFilesLifeCircleService.extractAndSave(project, commitDetails);
        // Update code check results.
        projectCyclomaticComplexity.update(groupName, projectName);
        projectLogicDepth.update(groupName, projectName);
        projectLinesOfCode.update(groupName, projectName);
        projectParameters.update(groupName, projectName);
        projectVariablesDeclaration.update(groupName, projectName);
        projectDuplicationCode.update(groupName, projectName);
        projectLineOfAnnotation.update(groupName,projectName);
        projectEmptyMethod.update(groupName,projectName);
    }
}
