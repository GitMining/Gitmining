package cn.nju.iSE.GitMining.service.code;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import cn.nju.iSE.GitMining.mongo.code.ProjectVariablesDeclarationMongoRepository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.TestProjectVariablesDeclaration.java
 *  
 *  2018年3月19日	下午3:55:13  
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectVariablesDeclaration {
	@Autowired
	private ProjectVariablesDeclaration projectVariablesDeclaration;
	
	@Autowired
	private ProjectVariablesDeclarationMongoRepository projectVariablesDeclarationMongoRepository;
	
	@Before
	public void clearUp() {
		projectVariablesDeclarationMongoRepository.deleteAll();
	}
	
	@Test
	public void test() {
		System.out.println("Start Test.");
		long id1 = projectVariablesDeclaration.create("","groupTest", 1L);
		System.out.println(id1);
		ProjectVabsDec projectVabDec1 = projectVariablesDeclaration.getProject(1);
		long id2 = projectVariablesDeclaration.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook","groupTest", 1L);
		System.out.println(id2);
		List<ProjectVabsDec> projectVabDec2 = projectVariablesDeclaration.getProjects("jgit-cookbook");
		List<ProjectVabsDec> projectVabDec3 = projectVariablesDeclaration.getProjectsByPth("jgit-cookbook");
		ProjectVabsDec projectVabDec4 = projectVariablesDeclaration.getProject(1);
		
		System.out.println("Get Project by ID: ");
		System.out.println(projectVabDec1);
		System.out.println("Get Projects by project name: ");
		System.out.println(projectVabDec2);
		System.out.println("Get Projects by project full path: ");
		System.out.println(projectVabDec3);
		System.out.println("Get Project by ID: ");
		System.out.println(projectVabDec4);
	}
}
