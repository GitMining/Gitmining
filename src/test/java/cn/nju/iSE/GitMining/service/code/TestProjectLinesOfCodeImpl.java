package cn.nju.iSE.GitMining.service.code;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectLinesOfCodeImpl {
    @Autowired
    private ProjectLinesOfCode projectLinesOfCode;

    private String prjtPth, groupName;

    private long pId;

    @Before
    public void setUp(){
        prjtPth = "D:\\work\\GitMining\\Repos\\samples\\161250128_STZ\\PictureTag_Phase_II";
        groupName = "161250128_STZ";
        pId = 1083;
    }

    @Test
    public void testCreate(){
        projectLinesOfCode.create(prjtPth, groupName, pId);
    }
}
