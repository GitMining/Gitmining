package cn.nju.iSE.GitMining.service.code;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.mongo.code.ProjectLOCMongoRepository;
import cn.nju.iSE.GitMining.service.code.impl.ProjectLinesOfCodeImpl;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.TestProjectLinesOfCode.java
 *  
 *  2018年3月18日	下午2:46:23  
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectLinesOfCode {
	
	@Autowired
	private ProjectLinesOfCodeImpl projectLinesOfCodeImpl;
	
	@Autowired
	private ProjectLOCMongoRepository projectLOCMongoRepository;
	
	@Before
	public void SetupContext() {
		projectLOCMongoRepository.deleteAll();
	}
	
	@Test
	public void test() {
		long id = projectLinesOfCodeImpl.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook","groupTest", 1L);
		System.out.println(id);
		ProjectLOC projectLOC = projectLinesOfCodeImpl.getProject(1);
		System.out.println(projectLOC);
	}
}
