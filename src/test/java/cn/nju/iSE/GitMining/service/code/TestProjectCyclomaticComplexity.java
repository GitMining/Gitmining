package cn.nju.iSE.GitMining.service.code;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import cn.nju.iSE.GitMining.mongo.code.ProjectCCMongoRepository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.TestProjectCyclomaticComplexity.java
 *  
 *  2018年3月19日	上午10:37:31  
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectCyclomaticComplexity {
	@Autowired
	private ProjectCyclomaticComplexity projectCyclomaticComplexity;
	
	@Autowired
	private ProjectCCMongoRepository projectCCMongoRepository;
	
	@Before
	public void clearUp() {
		projectCCMongoRepository.deleteAll();
		System.out.println("Starting Test.");
	}
	
	@Test
	public void test() {
		System.out.println("Start Test.");
		long id = projectCyclomaticComplexity.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook","groupTest", 1L);
		System.out.println(id);
		ProjectCC projectCC1 = projectCyclomaticComplexity.getProject(1);
		List<ProjectCC> projectCC2 = projectCyclomaticComplexity.getProjects("jgit-cookbook");
		List<ProjectCC> projectCC3 = projectCyclomaticComplexity.getProjectsByPth("jgit-cookbook");
		
		System.out.println("Get Project by ID: ");
		System.out.println(projectCC1);
		System.out.println("Get Projects by project name: ");
		System.out.println(projectCC2);
		System.out.println("Get Projects by project full path: ");
		System.out.println(projectCC3);
		
		System.out.println(projectCCMongoRepository.exists(1L));
	}
}
