package cn.nju.iSE.GitMining.service.system.impl;

import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.GroupScore;
import cn.nju.iSE.GitMining.model.system.ProjectScore;
import cn.nju.iSE.GitMining.service.system.GroupScoreService;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.service.system.ProjectScoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestGroupScoreImpl {
    @Autowired
    private GroupScoreService groupScoreService;

    @Autowired
    private ProjectScoreService projectScoreService;

    @Autowired
    private GroupService groupService;

    private Group group;

    private List<ProjectScore> projectScores;

    @Before
    public void setup() {
        projectScores = new ArrayList<>();
        group = groupService.getGroup(1118);
        projectScores.add(projectScoreService.calculateProjectIndicators(1092));
        System.out.println(group);
        System.out.println(projectScores);
    }

    @Test
    public void testGetGroupScore(){
        GroupScore groupScore = groupScoreService.calculateGroupScore(group, projectScores);
        System.out.println(groupScore);
        groupScoreService.save(groupScore);
        System.out.println(groupScoreService.getGroupScore(1118));
    }
}
