package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.service.system.GroupDownload;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.TestGroupDownloadImpl.java
 *  
 *  2018年4月22日	上午10:31:19  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestGroupDownloadImpl {

	@Autowired
	private GroupDownload groupDownload;
	
	String url = "http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL&per_page=1";
	
	String charset = "";
	
	@Test
	public void testGetGroups() {
		List<Group> groups = groupDownload.downloadGroups(url, charset);
		for(Group g: groups) {
			System.out.println(g);	
		}
	}
}
