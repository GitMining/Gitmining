package cn.nju.iSE.GitMining.service.code;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import cn.nju.iSE.GitMining.mongo.code.ProjectParamsMongoRepository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.TestProjectParameters.java
 *  
 *  2018年3月19日	下午2:33:25  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectParameters {

	@Autowired
	private ProjectParameters projectParameters;
	
	@Autowired
	private ProjectParamsMongoRepository projectParamsMongoRepository;
	
	@Before
	public void clearUp() {
		projectParamsMongoRepository.deleteAll();
	}
	
	@Test
	public void test() {
		System.out.println("Start Test.");
		long id1 = projectParameters.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook","groupTest", 1L);
		System.out.println(id1);
		ProjectParams projectParams1 = projectParameters.getProject(1);
//		long id2 = projectParameters.create("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
//		System.out.println(id2);
		List<ProjectParams> projectParams2 = projectParameters.getProjects("jgit-cookbook");
		List<ProjectParams> projectParams3 = projectParameters.getProjectsByPth("jgit-cookbook");
		ProjectParams projectParams4 = projectParameters.getProject(1);
		
		System.out.println("Get Project by ID: ");
		System.out.println(projectParams1);
		System.out.println("Get Projects by project name: ");
		System.out.println(projectParams2);
		System.out.println("Get Projects by project full path: ");
		System.out.println(projectParams3);
		System.out.println("Get Project by ID: ");
		System.out.println(projectParams4);
	}
}
