package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.service.system.GroupService;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.TestGroupServiceImpl.java
 *  
 *  2018年4月25日	下午3:48:08  
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestGroupServiceImpl {

	private String url = "http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL";
	
	@Autowired
	private GroupService groupService;
	
	@Test
	public void testDownloadGroups() {
		List<Group> groups = groupService.downloadGroups(url, "");
		System.out.println(groups);
		System.out.println(groups.size());
	}

	@Test
	public void testFind(){
		long id = 1118;
		Group group = groupService.getGroup(1118);
		System.out.println(group);
	}
}
