package cn.nju.iSE.GitMining.service.system.impl;

import cn.nju.iSE.GitMining.model.system.ProjectScore;
import cn.nju.iSE.GitMining.service.system.ProjectScoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectScoreImpl {
    @Autowired
    private ProjectScoreService projectScoreService;

    @Test
    public void testCalculate(){
        ProjectScore projectScore = projectScoreService.calculateProjectIndicators(1092);
        System.out.println(projectScore);
        System.out.println("CC: " + projectScore.getCcScore().isScore());
        System.out.println("LD: " + projectScore.getLdScore().isScore());
        System.out.println("PD: " + projectScore.getPdScore().isScore());
        System.out.println("VD: " + projectScore.getVdScore().isScore());
        System.out.println("EM: " + projectScore.getEmScore().isScore());
        System.out.println("DC: " + projectScore.getDcScore().isScore());
        System.out.println("LOC: "+ projectScore.getLocScore().isScore());
        System.out.println("LOA: "+ projectScore.getLoaScore().isScore());
    }
}
