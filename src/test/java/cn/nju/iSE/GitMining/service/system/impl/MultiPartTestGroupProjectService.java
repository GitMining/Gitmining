package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.ProjectMongoRepository;
import cn.nju.iSE.GitMining.service.system.GroupDownload;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.service.system.ProjectDownload;
import cn.nju.iSE.GitMining.service.system.ProjectService;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.service.system.impl.TestProjectService.java
 * 
 *           2018年4月23日 下午4:34:21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MultiPartTestGroupProjectService {

	@Autowired
	private GroupService groupService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private GroupDownload groupDownload;
	
	@Autowired
	private ProjectDownload projectDownload;
	
	@Autowired
	private GroupMongoRepository groupMongoRepository;
	
	@Autowired
	private ProjectMongoRepository projectMongoRepository;

	String url = "http://114.215.188.21/api/v3/groups/?private_token=5oR7mkTB5o9izwB3dwxL&per_page=1";

	String charset = "";
	
	String token = "5oR7mkTB5o9izwB3dwxL";

	@Before
	public void setupContext() {
		System.out.println("===================== BEFORE TEST ========================");
		groupMongoRepository.deleteAll();
		projectMongoRepository.deleteAll();
		List<Group> groups = groupDownload.downloadGroups(url, charset);
		groupService.save(groups);
		for(Group g: groups) {
			System.out.println(g);	
			List<Project> projects = projectDownload.getGroupProjects(g, From.GitLab_SECIII, token);
			for(Project p: projects)
				System.out.println(p);
			projectService.save(projects);
		}
	}

	@Test
	public void test() {
		System.out.println("===================== START TEST ========================");
		List<Group> groups = groupService.getGroups();
		System.out.println(groups);
		System.out.println(projectService.getProjects());
	}
	
	@After
	public void clean() {
		System.out.println("===================== TEST END ========================");
		groupMongoRepository.deleteAll();
		projectMongoRepository.deleteAll();
		System.out.println(groupService.getGroups().size());
		System.out.println(projectService.getProjects().size());
	}
}
