package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.service.system.ProjectDownload;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.TestProjectDownloadImpl.java
 *  
 *  2018年4月22日	下午1:40:11  
*/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectDownloadImpl {

	@Autowired
	private ProjectDownload projectDownload;
	
	String token = "5oR7mkTB5o9izwB3dwxL";
	
	@Test
	public void testGetProjects() {
		Group group = new Group();
		group.setId(1109);
		List<Project> projects = projectDownload.getGroupProjects(group, From.GitLab_SECIII, token);
		for(Project p: projects) 
			System.out.println(p);
		System.out.println(group.getProjectIds());
	}
}
