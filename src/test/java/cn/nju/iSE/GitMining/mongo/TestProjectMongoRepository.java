package cn.nju.iSE.GitMining.mongo;

import java.util.List;

import cn.nju.iSE.GitMining.mongo.code.ProjectCCMongoRepository;
import cn.nju.iSE.GitMining.mongo.code.ProjectLOCMongoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.TestProjectMongoRepository.java
 *  
 *  2018年3月17日	上午10:27:50  
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestProjectMongoRepository {

	@Autowired
	private ProjectLOCMongoRepository projectLOCMongoRepository;
	
	@Autowired
	private ProjectCCMongoRepository projectCCMongoRepository;
	
	@Before
	public void setUp() {
//		projectLOCMongoRepository.deleteAll();
//		projectCCMongoRepository.deleteAll();
	}
	
	@Test
	public void testInertLOC() {
		try {
			ProjectLOC projectLOC = new ProjectLOC(1l, "D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
			projectLOCMongoRepository.save(projectLOC);
			ProjectLOC result = projectLOCMongoRepository.findById(1l);
			System.out.println(result);
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInsertCC() {
		try {
			ProjectCC projectCC = new ProjectCC(1l, "D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
			projectCCMongoRepository.save(projectCC);
			ProjectCC pc1 = projectCCMongoRepository.findById(1l);
			List<ProjectCC> pc2 = projectCCMongoRepository.findByProjectName("jgit-cookbook");
			List<ProjectCC> pc3 = projectCCMongoRepository.findByProjectPath("D:\\work\\GitMining\\Repos\\samples\\jgit-cookbook");
			
			System.out.println(pc1);
			System.out.println("===================================");
			System.out.println(pc2);
			System.out.println("===================================");
			System.out.println(pc3);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}
}
