package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.common.enumeration.State;
import cn.nju.iSE.GitMining.dao.system.AnalyzingStateDao;
import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import cn.nju.iSE.GitMining.mongo.system.AnalyzingStateMongoRepository;
import cn.nju.iSE.GitMining.service.system.RepositoryClone;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CloneReceiver {

    @Autowired
    private AnalyzingStateMongoRepository analyzingStateMongoRepository;

    @Autowired
    private AnalyzingStateDao analyzingStateDao;

    @Autowired
    private RepositoryClone repositoryClone;

    @Autowired
    private AnalyzeSender analyzeSender;

    @RabbitListener(queues = "clone")
    public void process(ProjectVO projectVO) {
        System.out.println("================CLONE RECEIVER================" +projectVO.getName() );
        // Start cloning
        startClone(projectVO);
        // Send project to analyze.
        analyzeSender.send(projectVO);
    }

    private void startClone(ProjectVO projectVO){
        // Update project analyzing progress.
        long id = projectVO.getId();
        AnalyzingState analyzingState = analyzingStateMongoRepository.findById(id);
        if (analyzingState.getState() == State.WAITINGCLONE)
            analyzingState.setState(State.CLONING);
        else
            analyzingState.setState(State.WAITINGERROR);
        analyzingStateDao.updateState(analyzingState);
        repositoryClone.clone(projectVO.getUrl());
        // Cloning work completed, and start updating project analyzing progress and sending message to analyzing_queue.
        analyzingState = analyzingStateMongoRepository.findById(id);
        if (analyzingState.getState() == State.CLONING)
            analyzingState.setState(State.WAITINGANALYZE);
        else
            analyzingState.setState(State.CLONINGERROR);
        analyzingStateDao.updateState(analyzingState);
    }
}
