package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.common.enumeration.State;
import cn.nju.iSE.GitMining.dao.system.AnalyzingStateDao;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.mongo.system.AnalyzingStateMongoRepository;
import cn.nju.iSE.GitMining.service.code.*;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.service.system.CommitsExtract;
import cn.nju.iSE.GitMining.service.system.ProjectDetailService;
import cn.nju.iSE.GitMining.service.system.ProjectService;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.ProjectLogic;
import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class AnalyzeReceiver {

    @Autowired
    private ProjectLogic projectLogic;

    @Autowired
    private CommitsExtract commitsExtract;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CommitDetailService commitDetailService;

    @Autowired
    private ProjectDetailService projectDetailService;

    @Autowired
    private ProjectFilesLifeCircleService projectFilesLifeCircleService;

    @Autowired
    private ProjectCyclomaticComplexity projectCyclomaticComplexity;

    @Autowired
    private ProjectLogicDepth projectLogicDepth;

    @Autowired
    private ProjectLinesOfCode projectLinesOfCode;

    @Autowired
    private ProjectParameters projectParameters;

    @Autowired
    private ProjectVariablesDeclaration projectVariablesDeclaration;

    @Autowired
    private ProjectDuplicationCode projectDuplicationCode;

    @Autowired
    private ProjectLineOfAnnotation projectLineOfAnnotation;

    @Autowired
    private ProjectEmptyMethod projectEmptyMethod;

    @Autowired
    private AnalyzingStateMongoRepository analyzingStateMongoRepository;

    @Autowired
    private AnalyzingStateDao analyzingStateDao;

    @RabbitListener(queues = "analyze")
    public void process(ProjectVO projectVO){
        System.out.println("================ANALYZE RECEIVER================" +projectVO.getName() );
        Project project = projectLogic.transferPOVO(projectVO);
        System.out.println(startAnalyzing(project));
    }

    private JSONObject startAnalyzing(Project project) {
        // Update project analyzing progress.
        long id = project.getId();
        AnalyzingState analyzingState = analyzingStateMongoRepository.findById(id);
        if (analyzingState.getState() == State.WAITINGANALYZE)
            analyzingState.setState(State.ANALYZING);
        else
            analyzingState.setState(State.CLONINGERROR);
        analyzingStateDao.updateState(analyzingState);

        // Start analyzing.
        JSONObject result = new JSONObject();
        result.put("StartTime", new Date());
        result.put("AnalyzingObject", project.getUrl());
        projectService.captureCommitIds(project);
        if(project.getCommitIds() == null || project.getCommitIds().size() == 0) {
            result.put("RepoStatus", "No Commits");
            result.put("EndTime", new Date());
            projectService.save(project);
            return result;
        }
        // Extract and store commits.
        String path = project.getFullName();
        List<CommitDetail> commitDetails = commitsExtract.projectCommitsExtract(path);
        commitDetailService.save(commitDetails);
        // Compute and store project details.
        ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(project);
        projectDetailService.save(projectDetail);

        // Start analyze project cooperation style.
        projectFilesLifeCircleService.extractAndSave(project, commitDetails);

        // Start analyze project code style
        String namespace = project.getNamespace();
        long pId = project.getId();
        projectCyclomaticComplexity.create(path, namespace, pId);
        projectLinesOfCode.create(path, namespace, pId);
        projectLogicDepth.create(path, namespace, pId);
        projectVariablesDeclaration.create(path, namespace, pId);
        projectParameters.create(path, namespace, pId);
        projectLineOfAnnotation.create(path, namespace, pId);
        projectEmptyMethod.create(path, namespace, pId);
        projectDuplicationCode.create(path, namespace, pId);
        projectService.save(project);
        result.put("EndTime", new Date());

        // Analyzing work completed, and start updating project analyzing progress
        analyzingState = analyzingStateMongoRepository.findById(id);
        if(analyzingState.getState() == State.ANALYZING)
            analyzingState.setState(State.FINISHED);
        else
            analyzingState.setState(State.ANALYZINGERROR);
        analyzingStateDao.updateState(analyzingState);

        return  result;
    }
}
