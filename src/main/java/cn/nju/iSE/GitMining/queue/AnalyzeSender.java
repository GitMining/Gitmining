package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Component
public class AnalyzeSender implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

//    @PostConstruct
//    public void init(){
//        rabbitTemplate.setReturnCallback(this);
//        rabbitTemplate.setConfirmCallback(this);
//    }

    public void send(ProjectVO projectVO){
        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
        JSONObject result = new JSONObject();
        result.put("SendObject", projectVO.getId());
        result.put("SendTime", new Date());
        result.put("ID", correlationId);
        System.out.println("================ANALYZE SEND================"+result);
        rabbitTemplate.convertAndSend("topicExchange", "analyze", projectVO, correlationId);
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if(ack)
            System.out.println("[CONFIRM] - Message sent successfully: " + correlationData);
        else
            System.out.println("[CONFIRM] - Message sent failed: " + cause);
    }

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("[RETURN] - " + message.getMessageProperties().getCorrelationIdString() + " send failed");
    }

}
