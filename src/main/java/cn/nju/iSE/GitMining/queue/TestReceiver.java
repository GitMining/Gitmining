package cn.nju.iSE.GitMining.queue;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RabbitListener(queues = "test")
public class TestReceiver {

    @RabbitHandler
    public void process(JSONObject msg){
        System.out.println("RECEIVER: " + msg.get("CONTENT") + " TIME: " + new Date());
    }
}
