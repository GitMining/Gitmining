package cn.nju.iSE.GitMining.queue;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TestSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String content){
        JSONObject msg = new JSONObject();
        Date dt = new Date();
        msg.put("CONTENT", content);
        msg.put("TIME", dt);
        System.out.println("SEND: " + content + " TIME: " + dt);
        this.amqpTemplate.convertAndSend("test", msg);
    }
}
