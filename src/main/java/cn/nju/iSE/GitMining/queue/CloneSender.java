package cn.nju.iSE.GitMining.queue;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.common.enumeration.State;
import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import cn.nju.iSE.GitMining.mongo.system.AnalyzingStateMongoRepository;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Component
public class CloneSender implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AnalyzingStateMongoRepository analyzingStateMongoRepository;

//    @PostConstruct
//    public void init(){
//        rabbitTemplate.setConfirmCallback(this);
//        rabbitTemplate.setReturnCallback(this);
//    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if(ack)
            System.out.println("[CONFIRM] - Message sent successfully: " + correlationData);
        else
            System.out.println("[CONFIRM] - Message sent failed: " + cause);
    }

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("[RETURN] - " + message.getMessageProperties().getCorrelationIdString() + " send failed");
    }

    public JSONObject send(ProjectVO projectVO){
        CorrelationData correlationId = new CorrelationData(UUID.randomUUID().toString());
        setState(projectVO);
        JSONObject result = new JSONObject();
        result.put("SendObject", projectVO.getId());
        result.put("SendTime", new Date());
        result.put("ID", correlationId);
        rabbitTemplate.convertAndSend("topicExchange", "clone", projectVO, correlationId);
        return result;
    }

    private void setState(ProjectVO projectVO){
        AnalyzingState analyzingState = new AnalyzingState();
        analyzingState.setId(projectVO.getId());
        analyzingState.setPlatform(From.GitLab_SECIII);
        analyzingState.setProjectName(projectVO.getName());
        analyzingState.setProjectNamespace(projectVO.getNamespace());
        analyzingState.setState(State.WAITINGCLONE);
        analyzingState.setLastUpdate(new Date().getTime());
        analyzingStateMongoRepository.save(analyzingState);
    }
}
