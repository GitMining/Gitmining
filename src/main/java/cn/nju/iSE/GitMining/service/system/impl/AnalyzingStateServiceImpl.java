package cn.nju.iSE.GitMining.service.system.impl;

import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import cn.nju.iSE.GitMining.mongo.system.AnalyzingStateMongoRepository;
import cn.nju.iSE.GitMining.service.system.AnalyzingStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AnalyzingStateServiceImpl implements AnalyzingStateService {

    @Resource
    private AnalyzingStateMongoRepository analyzingStateMongoRepository;

    @Override
    public AnalyzingState getProjectState(long id) {
        return analyzingStateMongoRepository.findById(id);
    }
}
