package cn.nju.iSE.GitMining.service.system.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.common.util.Downloader;
import cn.nju.iSE.GitMining.dao.system.GroupDao;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;
import cn.nju.iSE.GitMining.service.system.GroupService;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.GroupServiceImpl.java
 *  
 *  2018年4月23日	上午10:30:47  
*/
@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupMongoRepository groupMongoRepository;
	
	@Autowired
	private GroupDao groupDao;
	
	@Override
	public List<Group> getGroups() {
		return groupMongoRepository.findAll();
	}

	@Override
	public List<Group> getGroups(long start, long end) {
		return groupMongoRepository.findAllByTimeBetween(start, end);
	}

	@Override
	public void save(Group group) {
		groupMongoRepository.save(group);
	}

	@Override
	public void save(List<Group> groups) {
		groupMongoRepository.save(groups);
	}

	@Override
	public void appendGroupProjects(Group group) {
		groupDao.appendGroupProjects(group);
	}

	@Override
	public Group getGroup(long id) {
		return groupMongoRepository.findById(id);
	}

	@Override
	public Group getGroup(String name) {
		return groupMongoRepository.findByOwnerName(name);
	}

	@Override
	public Page<Group> getGroups(Pageable pageable) {
		return groupMongoRepository.findAll(pageable);
	}

	@Override
	public Page<Group> getGroups(long start, long end, Pageable pageable) {
		return groupMongoRepository.findAllByTimeBetween(start, end, pageable);
	}
	
	/**
	 * This method get groups' basic information which is not added into database
	 * without informations of members and projects' id.
	 * @param url list group url, like this "http://114.215.188.21/api/v3/groups/?private_token=:token&page=:page&per_page=:perpage".
	 * @param charset 
	 * @return return the list of groups with basic information.
	 */
	@Override
    public List<Group> downloadGroups(String url, String charset){
        List<Group> groups = new ArrayList<Group>();
        Group group;
        JSONArray arrays = Downloader.getJsonArray(url, charset);
        for(int i = 0; i< arrays.size(); i++) {
        	JSONObject object = arrays.getJSONObject(i);
        	group = new Group();
        	long id = object.getLong("id");
        	String ownerName = object.getString("name");
        	String web_url = object.getString("web_url");
        	group.setId(id);
        	group.setOwnerId(id);
        	group.setOwnerName(ownerName);
        	group.setTime(new Date().getTime());
        	group.setPlatform(From.GitLab_SECIII);
        	group.setOwnerURL(web_url);
        	if(!groupMongoRepository.exists(id))
        		groups.add(group);
        }
        return groups;
    }

}
