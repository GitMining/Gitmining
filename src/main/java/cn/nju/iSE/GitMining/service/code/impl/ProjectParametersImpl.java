package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectParamsDao;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsParams;
import cn.nju.iSE.GitMining.model.code.vab.MethodParams;
import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import cn.nju.iSE.GitMining.model.code.vab.Variable;
import cn.nju.iSE.GitMining.mongo.code.ProjectParamsMongoRepository;
import cn.nju.iSE.GitMining.service.code.ProjectParameters;
import iSESniper.code.entity.vab.MethodParameters;
import iSESniper.code.entity.vab.MethodParamsInFile;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.impl.ProjectParametersImpl.java
 *  
 *  2018年3月19日	上午11:16:24  
*/
@Service
public class ProjectParametersImpl implements ProjectParameters {
	
	@Autowired
	private ProjectParamsMongoRepository projectParamsMongoRepository;

	@Autowired
	private ProjectParamsDao projectParamsDao;

	@Override
	public long create(String prjtPth, String groupName, long pId){
		try {
			if(projectParamsMongoRepository.exists(pId))
				projectParamsMongoRepository.delete(pId);
			if(prjtPth == null || prjtPth.trim().equals(""))
				return -1;
			ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer(prjtPth);
			String name = projectAnalyzer.getPrjtName();
			long version = 0L;
			List<MethodParamsInFile> methods = projectAnalyzer.getMethodsParams();
			List<FileMethodsParams> filesMethodsParams = extract(methods);
			ProjectParams projectParams = new ProjectParams(pId, prjtPth, name, groupName, version, filesMethodsParams);
			projectParamsMongoRepository.save(projectParams);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectParams> getProjects(String name) {
		return projectParamsMongoRepository.findByProjectName(name);
	}

	@Override
	public List<ProjectParams> getProjectsByPth(String pth) {
		return projectParamsMongoRepository.findByProjectPath(pth);
	}

	@Override
	public ProjectParams getProject(long id) {
		return projectParamsMongoRepository.findById(id);
	}

	@Override
	public ProjectParams getProject(String groupName, String projectName) {
		return projectParamsMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectParamsMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectParams oldParams = projectParamsMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			long version = oldParams.getVersion() + 1;
			long pId = oldParams.getId();
			List<MethodParamsInFile> methods = pAnalyzer.getMethodsParams();
			List<FileMethodsParams> filesMethodsParams = extract(methods);
			ProjectParams projectParams = new ProjectParams(pId, prjtPth, projectName, groupName, version, filesMethodsParams);
			projectParamsDao.update(projectParams);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectParams projectParams) {
		List<FileMethodsParams> fileMethodsParams = projectParams.getFiles();
		List<Object> awfuls = new ArrayList<>();
		for(FileMethodsParams methodsParams: fileMethodsParams) {
			List<MethodParams> methodParamsList = methodsParams.getMethods();
			for(MethodParams methodParams: methodParamsList){
				int len = methodParams.getParams().size();
				if(len > Constant.CodeQualityIndicator.PD_WARNING_AWFUL)
					awfuls.add(methodParams);
			}
		}
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectParams projectParams) {
		List<FileMethodsParams> fileMethodsParams = projectParams.getFiles();
		List<Object> warnings = new ArrayList<>();
		for(FileMethodsParams methodsParams: fileMethodsParams) {
			List<MethodParams> methodParamsList = methodsParams.getMethods();
			for(MethodParams methodParams: methodParamsList){
				int len = methodParams.getParams().size();
				if(len <= Constant.CodeQualityIndicator.PD_WARNING_AWFUL
						&& len > Constant.CodeQualityIndicator.PD_INFO_WARNING)
					warnings.add(methodParams);
			}
		}
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectParams projectParams) {
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<Object> awfuls = new ArrayList<>();
		List<Object> warnings = new ArrayList<>();
		List<FileMethodsParams> fileMethodsParams = projectParams.getFiles();
		for(FileMethodsParams methodsParams: fileMethodsParams) {
			List<MethodParams> methodParamsList = methodsParams.getMethods();
			for(MethodParams methodParams: methodParamsList){
				int len = methodParams.getParams().size();
				if(len <= Constant.CodeQualityIndicator.PD_WARNING_AWFUL
						&& len > Constant.CodeQualityIndicator.PD_INFO_WARNING)
					warnings.add(methodParams);
				else if(len > Constant.CodeQualityIndicator.PD_WARNING_AWFUL)
					awfuls.add(methodParams);
			}
		}
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

	private List<FileMethodsParams> extract(List<MethodParamsInFile> methods){
		List<FileMethodsParams> filesMethodsParams = new ArrayList<FileMethodsParams>();
		for(MethodParamsInFile m: methods) {
			String jpth = m.getPth();
			List<MethodParams> methodParameters = new ArrayList<MethodParams>();
			for(MethodParameters mps: m.getMsParams()) {
				List<Variable> variables = new ArrayList<Variable>();
				for(iSESniper.code.entity.vab.Variable v: mps.getParams()) {
					Variable variable = new Variable();
					variable.setClazz(v.getClazz());
					variable.setvName(v.getvName());
					variables.add(variable);
				}
				String mName = mps.getMethodName();
				methodParameters.add(new MethodParams(mName, variables));
			}
			filesMethodsParams.add(new FileMethodsParams(jpth, methodParameters));
		}
		return  filesMethodsParams;
	}

}
