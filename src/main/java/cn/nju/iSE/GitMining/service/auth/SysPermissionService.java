package cn.nju.iSE.GitMining.service.auth;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import cn.nju.iSE.GitMining.model.auth.SysRole;

public interface SysPermissionService {
    List<SysPermission> getPermissions(List<Integer> ids);

    JSONObject getPermissions(SysRole role);

    /**
     * Add new permission.
     *
     * @param jsonObject JsonObject at least includes
     */
    public void addPermission(JSONObject jsonObject);

    public void addPermission(SysPermission permission);

    public void addPermissions(List<SysPermission> permissions);

    public boolean checkPermissionId(int id);

    public List<SysPermission> getAllPermission();
}
