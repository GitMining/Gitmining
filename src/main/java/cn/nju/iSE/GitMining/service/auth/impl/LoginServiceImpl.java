package cn.nju.iSE.GitMining.service.auth.impl;

import cn.nju.iSE.GitMining.common.util.CommonUtil;
import cn.nju.iSE.GitMining.service.auth.LoginService;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Override
    public JSONObject authLogin(JSONObject jsonObject) {
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        JSONObject returnData = new JSONObject();
        Subject currentUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            currentUser.login(token);
            returnData.put("result", "success");
            returnData.put("token", currentUser.getSession().getId());
        } catch (AuthenticationException e) {
            returnData.put("result", "fail");
        }
        return CommonUtil.successJson(returnData);
    }

    @Override
    public JSONObject logout(JSONObject jsonObject) {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return CommonUtil.successJson();
    }
}
