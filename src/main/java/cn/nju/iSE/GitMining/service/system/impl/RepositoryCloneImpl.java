package cn.nju.iSE.GitMining.service.system.impl;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.service.system.RepositoryClone;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.cooperation.helper.RepositoryDownloader;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.RepositoryCloneImpl.java
 *  
 *  2018年4月22日	下午3:21:38  
*/
@Service
public class RepositoryCloneImpl implements RepositoryClone {

	@Override
	public void clone(String repoHTTPUrl) {
		String user = "ljt";
		String pwd = "git#ljt16";
		RepositoryDownloader repositoryDownloader = new RepositoryDownloader(Route.CLONE_PATH, user, pwd);
		try {
			repositoryDownloader.cloneRemoteRepo(repoHTTPUrl);
		} catch (GitAPIException | IOException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void clone(Project project) {
		clone(project.getUrl());
	}

}
