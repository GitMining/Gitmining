package cn.nju.iSE.GitMining.service.system;

import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.GroupScore;
import cn.nju.iSE.GitMining.model.system.ProjectScore;

import java.util.List;

public interface GroupScoreService {

    public GroupScore calculateGroupScore(Group group, List<ProjectScore> projectScores);

    public GroupScore initGroupScore(Group group);

    public void save(GroupScore groupScore);

    public void save(List<GroupScore> groupScores);

    public GroupScore getGroupScore(long id);

    public List<GroupScore> getGroupScore(List<Long> ids);
}
