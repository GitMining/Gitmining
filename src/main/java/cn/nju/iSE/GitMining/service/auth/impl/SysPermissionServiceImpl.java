package cn.nju.iSE.GitMining.service.auth.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.mongo.auth.SysPermissionMongoRepository;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.common.util.Constant;

@Service
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionMongoRepository sysPermissionMongoRepository;

    @Override
    public List<SysPermission> getPermissions(List<Integer> ids) {
        List<SysPermission> permissions = new ArrayList<>();
        SysPermission permission;
        for (Integer i : ids) {
            permission = sysPermissionMongoRepository.findById(i);
            if (permission != null)
                permissions.add(permission);
        }
        return permissions;
    }

    @Override
    public JSONObject getPermissions(SysRole role) {
    	JSONObject jsonObject = new JSONObject();
        List<SysPermission> permissions = getPermissions(role.getPermissionIds());
        jsonObject.put(Constant.SESSION_USER_PERMISSION, (JSONArray)JSON.toJSON(permissions));
        return jsonObject;
    }

    @Override
    public void addPermission(JSONObject jsonObject) {
        long pId = sysPermissionMongoRepository.count() + 1;
        String name = jsonObject.getString("permissionName");
        String permission = jsonObject.getString("permission");
        String url = jsonObject.getString("url");
        String resourceType = jsonObject.getString("resourceType");
        long parentId = jsonObject.getLong("parentId");
        String parentIds = jsonObject.getString("parentIds");
        SysPermission sysPermission = new SysPermission();
        sysPermission.setId(pId);
        sysPermission.setName(name);
        sysPermission.setParentId(parentId);
        sysPermission.setParentIds(parentIds);
        sysPermission.setPermission(permission);
        sysPermission.setUrl(url);
        sysPermission.setResourceType(resourceType);
        sysPermissionMongoRepository.save(sysPermission);
    }

    @Override
    public void addPermission(SysPermission permission) {
        sysPermissionMongoRepository.save(permission);
    }

    @Override
    public void addPermissions(List<SysPermission> permissions) {
        sysPermissionMongoRepository.save(permissions);
    }

    @Override
    public boolean checkPermissionId(int id) {
        boolean flag = false;
        flag = sysPermissionMongoRepository.findById(id) != null;
        return flag;
    }

    @Override
    public List<SysPermission> getAllPermission() {
        return sysPermissionMongoRepository.findAll();
    }
}
