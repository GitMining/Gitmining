package cn.nju.iSE.GitMining.service.cooperation.impl;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.service.cooperation.AuthorsCooperationService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.common.util.Constant;
import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.FileDiff;
import iSESniper.cooperation.helper.CooperationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author : Magister
 * @fileName :
 * cn.nju.iSE.GitMining.service.cooperation.impl.AuthorsCooperationServiceImpl.java
 * <p>
 * 2018年4月27日 上午11:06:27
 */

@Service
public class AuthorsCooperationServiceImpl implements AuthorsCooperationService {

    @Autowired
    private CommitDetailService commitDetailService;

    @Override
    public List<CommitDetail> getAuthorCommitsDetail(Author author) {
        return commitDetailService.getCommits(author);
    }

    @Override
    public List<Map<Author, Double>> getAuthorsImportance(List<CommitDetail> commitDetails) {
        List<Map<Author, Double>> importances = new ArrayList<Map<Author, Double>>();
        Map<Author, Double> commitImportance = new HashMap<Author, Double>();
        Map<Author, Double> linesImportance = new HashMap<Author, Double>();
        countImportance(commitDetails, commitImportance, linesImportance);
        importances.add(commitImportance);
        importances.add(linesImportance);
        return importances;
    }

    @Override
    public Map<Author, Integer> getAuthorPie(String project) {
        List<CommitDetail> commitDetails = commitDetailService.getCommits(project);
        Map<Author, Integer> commitTimes = new HashMap<>();
        for (CommitDetail cDetail : commitDetails) {
            Author author = cDetail.getAuthor();
            Integer times = commitTimes.get(author);
            times = times == null ? 1 : times + 1;
            commitTimes.put(author, times);
        }
        return commitTimes;
    }

    /**
     * Count the importance of authors process. Authors' diversity.
     *
     * @param commitDetails
     * @param commitImportance
     * @param linesImportance
     */
    private void countImportance(List<CommitDetail> commitDetails, Map<Author, Double> commitImportance,
                                 Map<Author, Double> linesImportance) {
        if (commitDetails == null || commitDetails.size() == 0)
            return;
        Map<Author, List<Integer>> commitsInport = new HashMap<Author, List<Integer>>();
        Map<Author, List<Long>> linesInport = new HashMap<Author, List<Long>>();
        List<String> files = new ArrayList<String>();
        Author author;
        int fileIdx;
        List<Integer> cmtsInpt;
        List<Long> linesInpt;
        List<String> types = new ArrayList<>();
        Collections.addAll(types, Constant.FileTypes);
        for (CommitDetail cDetail : commitDetails) {
            author = cDetail.getAuthor();
            List<FileDiff> fileDiffs = cDetail.getFiles();
            for (FileDiff fDiff : fileDiffs) {
                // Find the contribution of developer j to file i.
                cmtsInpt = commitsInport.get(author);
                cmtsInpt = cmtsInpt == null ? new ArrayList<Integer>() : cmtsInpt;
                linesInpt = linesInport.get(author);
                linesInpt = linesInpt == null ? new ArrayList<Long>() : linesInpt;

                String fileName = fDiff.getNewPth();
                if (!CooperationHelper.fileTypeIsLegal(fileName, types)) {
                    continue;
                }
                if (files.contains(fileName)) {
                    fileIdx = files.indexOf(fileName);
                } else {
                    fileIdx = files.size();
                    files.add(fileName);
                }
                countCommitContribution(cmtsInpt, fileIdx);
                countLinesContribution(linesInpt, fileIdx, fDiff);

                commitsInport.put(author, cmtsInpt);
                linesInport.put(author, linesInpt);
            }
        }
        // Compute the diversity index of author A.
        Iterator<Author> iterator = commitsInport.keySet().iterator();
        BigDecimal b;
        while (iterator.hasNext()) {
            double linesDiversity = 0;
            double commitsDiversity = 0;
            double developerLines = 0;
            double developerCommits = 0;
            Author developer = iterator.next();
            // count commits contribution.
            List<Integer> commits = commitsInport.get(developer);
            for (Integer i : commits)
                developerCommits += i;
            for (Integer i : commits) {
                if (i == 0)
                    continue;
                commitsDiversity = commitsDiversity + (i / developerCommits * Math.log(i / developerCommits));
            }
            commitsDiversity *= -1;
            b = new BigDecimal(commitsDiversity);
            commitsDiversity = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            // count lines contribution.
            List<Long> lines = linesInport.get(developer);
            for (Long l : lines)
                developerLines += l;
            for (Long l : lines) {
                if (l == 0)
                    continue;
                linesDiversity = linesDiversity + (l / developerLines * Math.log(l / developerLines));
            }
            linesDiversity *= -1;
            b = new BigDecimal(linesDiversity);
            linesDiversity = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            commitImportance.put(developer, commitsDiversity);
            linesImportance.put(developer, linesDiversity);
        }
    }

    private void countLinesContribution(List<Long> linesInpt, int fileIdx, FileDiff fDiff) {
        if (linesInpt.size() <= fileIdx)
            for (int i = fileIdx - linesInpt.size(); i >= 0; i--)
                linesInpt.add(0L);
        long count = linesInpt.get(fileIdx) + fDiff.getAdded() + fDiff.getRemoved();
        linesInpt.remove(fileIdx);
        linesInpt.add(fileIdx, count);
    }

    private void countCommitContribution(List<Integer> cmtsInpt, int fileIdx) {
        if (cmtsInpt.size() <= fileIdx)
            for (int i = fileIdx - cmtsInpt.size(); i >= 0; i--)
                cmtsInpt.add(0);
        int count = cmtsInpt.get(fileIdx);
        cmtsInpt.remove(fileIdx);
        cmtsInpt.add(fileIdx, count + 1);
    }

}
