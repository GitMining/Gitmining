package cn.nju.iSE.GitMining.service.cooperation;

import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesSibling;
import iSESniper.cooperation.entity.Author;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.service.cooperation.ProjectFilesSiblingService.java
 * 
 *           2018年4月25日 下午9:23:01
 */

public interface ProjectFilesSiblingService {

	public ProjectFilesSibling getSiblings(ProjectFilesLifeCircle projectFilesLifeCircle);

	public void getContributeRelation(ProjectFilesLifeCircle projectFilesLifeCircle,
			ProjectFilesSibling projectFilesSibling);
	
	public Set<String> getFileTypes(List<String> files);
	
	public Map<Author, Map<String, List<String>>> getTypesFiles(Map<Author, List<String>> siblings);
}
