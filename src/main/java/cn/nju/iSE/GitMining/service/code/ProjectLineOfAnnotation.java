package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

import java.util.List;
import java.util.Map;

/**
 * @author VousAttendezrer
 */
public interface ProjectLineOfAnnotation extends ProjectCodeCheck<ProjectLOA> {
    Map<AnalysisRank, List<LineOfAnnotateInSingleFileEntity>> getRanks(ProjectLOA projectLOA);
}
