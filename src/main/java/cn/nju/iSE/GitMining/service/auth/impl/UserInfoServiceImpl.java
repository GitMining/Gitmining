package cn.nju.iSE.GitMining.service.auth.impl;

import java.util.List;

import javax.annotation.Resource;

import cn.nju.iSE.GitMining.common.util.CommonUtil;
import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.web.constant.ErrorEnum;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.UserInfo;
import cn.nju.iSE.GitMining.mongo.auth.UserInfoMongoRepository;
import cn.nju.iSE.GitMining.service.auth.UserInfoService;
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Resource
    private UserInfoMongoRepository userInfoRepository;

    @Override
    public UserInfo checkUser(String username) {
        return userInfoRepository.findByUsername(username);
    }

    @Override
    public JSONObject checkUser(String username, String password) {
        UserInfo userInfo = userInfoRepository.findByUsernameAndPassword(username, password);
        return (JSONObject) JSON.toJSON(userInfo);
    }

    @Override
    public List<UserInfo> listAllUsers() {
        return userInfoRepository.findAll();
    }

    @Override
    public JSONObject addUser(JSONObject jsonObject) {
        String username = jsonObject.getString("username");
        if(checkUsernameIsDuplicated(username))
            return CommonUtil.errorJson(ErrorEnum.E_10009);
        String password = jsonObject.getString("password");
        String salt = jsonObject.getString(Constant.AuthProperties.UserProperties.salt);
        int roleId = jsonObject.getBoolean(Constant.AuthProperties.UserProperties.isAdmin) ? 1 : 2;
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(username);
        userInfo.setPassword(password);
        userInfo.setSalt(salt);
        userInfo.setState((byte)0);
        userInfo.setRoleId(roleId);
        userInfoRepository.save(userInfo);
        return CommonUtil.successJson(username);
    }

    @Override
    public void updateUser(JSONObject jsonObject) {

    }

    @Override
    public boolean checkUsernameIsDuplicated(String user) {
        UserInfo userInfo = userInfoRepository.findByUsername(user);
        return userInfo != null;
    }
}
