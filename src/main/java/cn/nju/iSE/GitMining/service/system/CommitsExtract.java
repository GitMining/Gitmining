package cn.nju.iSE.GitMining.service.system;

import java.util.List;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.CommitsExtract.java
 *  
 *  2018年4月23日	下午2:05:54  
*/

public interface CommitsExtract {

	/**
	 * Extract project's commit information from project full name.
	 * @param path
	 * @return
	 */
	public List<CommitDetail> projectCommitsExtract(String path);
}
