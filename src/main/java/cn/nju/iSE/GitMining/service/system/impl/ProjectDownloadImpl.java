package cn.nju.iSE.GitMining.service.system.impl;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.common.util.Downloader;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.mongo.system.ProjectMongoRepository;
import cn.nju.iSE.GitMining.service.system.ProjectDownload;
import cn.nju.iSE.GitMining.web.constant.Route;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.ProjectDownloadImpl.java
 *  @deprecated 
 *  2018年4月22日	上午11:14:34  
*/
@Service
public class ProjectDownloadImpl implements ProjectDownload {
	
	@Autowired
	ProjectMongoRepository projectMongoRepository;

	@Override
	public List<Project> getGroupProjects(Group group, From platform, String token) {
		String baseUrl = null;
		switch (platform) {
		case GitLab:
			baseUrl = "";
			break;
		case GitHub:
			baseUrl = "";
			break;
		case GitLab_SECIII:
			baseUrl = "http://114.215.188.21/api/v3/groups/{0}/projects?private_token={1}";
		default:
			break;
		}
		if(baseUrl == null || baseUrl.length() == 0) 
			baseUrl = "http://114.215.188.21/api/v3/groups/{0}/projects?private_token={1}";
		String url = MessageFormat.format(baseUrl, String.valueOf(group.getId()), token);
		JSONArray array = Downloader.getJsonArray(url, "");
		List<Project> projects = new ArrayList<Project>();
		List<Long> projectIds = new ArrayList<Long>();
		Project project;
		for(int i = 0; i < array.size(); i++) {
			JSONObject object = array.getJSONObject(i);
			long id = object.getLong("id");
			if(projectMongoRepository.exists(id))
				continue;
			projectIds.add(id);
			String name = object.getString("name");
			String pathWithNamespace = object.getString("path_with_namespace");
			String fullName = Route.CLONE_PATH + pathWithNamespace;
			String namespace = pathWithNamespace.substring(0, pathWithNamespace.indexOf('/'));
			String webUrl = object.getString("http_url_to_repo");
			String createdTime = object.getString("created_at");
			long time = 0;
			try {
				time= UTC2TimeStamp(createdTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			project = new Project();
			project.setId(id);
			project.setName(name);
			project.setFullName(fullName);
			project.setNamespace(namespace);
			project.setTime(time);
			project.setUrl(webUrl);
			projects.add(project);
		}
		group.getProjectIds().addAll(projectIds);
		return projects;
	}

	private long UTC2TimeStamp(String time) throws ParseException {
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");
		dFormat.setTimeZone(TimeZone.getDefault());
		return dFormat.parse(time.substring(0, time.indexOf('+'))).getTime();
	}

	/**
	 * This method helps analyst get certain project details.
	 * @param url certain project's url like this "http://114.215.188.21/api/v3/projects/1091/"
	 * @param charset
	 * @param token url and token constitute target url "http://114.215.188.21/api/v3/projects/1091/?private_token=:token" 
	 */
	@Override
	public Project getProject(String url, String charset, String token) {
		Project project = new Project();
		String page = url + "?private_token=" + token;
		JSONObject object = Downloader.getJsonContent(page, charset);
		project.setId(object.getLong("id"));
		project.setName(object.getString("name"));
		project.setFullName(Route.CLONE_PATH + object.getString("path_with_namespace"));
		long time = 0;
		try {
			time = UTC2TimeStamp(object.getString("created_at"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		project.setTime(time);
		project.setUrl(object.getString("http_url_to_repo"));
		return project;
	}

	@Override 
	public void saveProjects(List<Project> projects) {
		projectMongoRepository.save(projects);
	}

	@Override
	public void saveProject(Project project) {
		projectMongoRepository.save(project);
	}

}
