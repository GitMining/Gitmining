package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.dao.system.ProjectDetailDao;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.mongo.system.ProjectDetailMongoRepository;
import cn.nju.iSE.GitMining.service.system.CommitsExtract;
import cn.nju.iSE.GitMining.service.system.ProjectDetailService;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.ProjectDetailServiceImpl.java
 *  
 *  2018年4月23日	下午3:41:47  
*/

@Service
public class ProjectDetailServiceImpl implements ProjectDetailService {

	@Autowired
	private CommitsExtract commitExtract;
	
	@Autowired
	private ProjectDetailMongoRepository projectDetailMongoRepository;
	
	@Autowired
	private ProjectDetailDao projectDetailDao;
	
	@Override
	public ProjectDetail projectCommitsExtract(Project project) {
		ProjectDetail projectDetail = new ProjectDetail();
		String path = project.getFullName();
		List<CommitDetail> commitDetails = commitExtract.projectCommitsExtract(path);
		projectDetail.setId(project.getId());
		projectDetail.setCommitCount(commitDetails.size());
		projectDetail.setCreateTime(project.getTime());
		projectDetail.setName(project.getName());
		projectDetail.setNamespace(project.getNamespace());
		projectDetail.setUrl(project.getUrl());
		long addedLines = 0;
		long removedLines = 0;
		long addedFiles = 0;
		long deletedFiles = 0;
		long modifiedFiles = 0;
		for (CommitDetail cd : commitDetails) {
			addedFiles += cd.getAllAdded();
			removedLines += cd.getAllRemoved();
			addedFiles += cd.getAddedFiles();
			deletedFiles += cd.getDeletedFiles();
			modifiedFiles += cd.getModifiedFiles();
		}
		projectDetail.setAddedLines(addedLines);
		projectDetail.setRemovedLines(removedLines);
		projectDetail.setAddedFiles(addedFiles);
		projectDetail.setDeletedFiles(deletedFiles);
		projectDetail.setModifiedFiles(modifiedFiles);
		return projectDetail;
	}

	@Override
	public ProjectDetail getProjectDetail(long id) {
		return projectDetailMongoRepository.findById(id);
	}

	@Override
	public List<ProjectDetail> getprojectDetails(String namespace) {
		return projectDetailMongoRepository.findByNamespace(namespace);
	}

	@Override
	public void save(List<ProjectDetail> projectDetails) {
		projectDetailMongoRepository.save(projectDetails);
	}

	@Override
	public void save(ProjectDetail projectDetail) {
		projectDetailMongoRepository.save(projectDetail);
	}

	@Override
	public ProjectDetail getProjectDetail(String namespace, String name) {
		return projectDetailMongoRepository.findByNamespaceAndName(namespace, name);
	}

	@Override
	public void update(ProjectDetail projectDetail) {
		projectDetailDao.update(projectDetail);
	}

}
