package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectParameters.java
 *  
 *  2018年3月19日	上午11:15:30  
*/

public interface ProjectParameters extends ProjectCodeCheck<ProjectParams> {

}
