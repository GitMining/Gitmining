package cn.nju.iSE.GitMining.service.system;

import java.util.List;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.ProjectDownload.java
 *  @deprecated
 *  2018年4月22日	上午11:10:23  
*/

public interface ProjectDownload {

	List<Project> getGroupProjects(Group group, From platform, String token);
	
	/**
	 * This method helps analyst get certain project details.
	 * @param url certain project's url like this "http://114.215.188.21/api/v3/projects/1091/"
	 * @param charset
	 * @param token url and token constitute target url "http://114.215.188.21/api/v3/projects/1091/?private_token=:token" 
	 */
	Project getProject(String url, String charset, String token);
	
	void saveProjects(List<Project> projects);
	
	void saveProject(Project project);
}
