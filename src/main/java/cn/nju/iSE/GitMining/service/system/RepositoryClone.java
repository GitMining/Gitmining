package cn.nju.iSE.GitMining.service.system;

import cn.nju.iSE.GitMining.model.system.Project;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.RepositoryClone.java
 *  
 *  2018年4月22日	下午3:19:26  
*/

public interface RepositoryClone {

	public void clone(String repoHTTPUrl);
	
	public void clone(Project project);
}
