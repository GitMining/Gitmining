package cn.nju.iSE.GitMining.service.auth;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.UserInfo;

public interface UserInfoService {

    public UserInfo checkUser(String username);

    public JSONObject checkUser(String username, String password);

    public List<UserInfo> listAllUsers();

    /**
     * Add new user.
     * @param jsonObject JsonObject At least includes "username", "password", "salt", "isAdmin" properties.
     */
    public JSONObject addUser(JSONObject jsonObject);

    public void updateUser(JSONObject jsonObject);

    /**
     * If the username has existed in database, return true.
     * @param user username that provided by pre-layer.
     * @return true or false.
     */
    public boolean checkUsernameIsDuplicated(String user);
}
