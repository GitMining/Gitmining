package cn.nju.iSE.GitMining.service.system;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.ProjectService.java
 *  
 *  2018年4月23日	下午12:49:30  
*/

public interface ProjectService {
	
	/**
	 * Get certain project by project id.
	 * @param id
	 * @return
	 */
	Project getProject(long id);
	
	/**
	 * Get certain projects by project namespace.
	 * @param name
	 * @return
	 */
	List<Project> getProjects(String namespace);
	
	/**
	 * Get certain project by project name with namespace.
	 * @param name
	 * @param namespace
	 * @return
	 */
	Project getProjects(String name, String namespace);

	/**
	 * Get all projects that are added into database.
	 * @return
	 */
	List<Project> getProjects();
	
	/**
	 * Get certain number of projects.
	 * @param pageable
	 * @return
	 */
	Page<Project> getProjects(Pageable pageable);
	
	/**
	 * Get all projects that are created between start time and end time.
	 * @param start
	 * @param end
	 * @return
	 */
	List<Project> getProjects(long start, long end);
	
	/**
	 * Get certain number projects that are created between start time and end time.
	 * @param start
	 * @param end
	 * @param pageable
	 * @return
	 */
	Page<Project> getProjects(long start, long end, Pageable pageable);
	
	/**
	 * Save list of projects.
	 * @param projects
	 */
	void save(List<Project> projects);
	
	/**
	 * Save certain project.
	 * @param project
	 */
	void save(Project project);
	
	/**
	 * Update project's commits' id list.
	 * Make sure the project instance is similar with old data except CommitIds property.
	 * @param project
	 */
	void appendProjectCommits(Project project);
	
	/**
	 * Capture commits' id list from commit extractor for certain project.
	 * @param project
	 * @return
	 */
	void captureCommitIds(Project project);
	
	/**
	 * This method helps analyst get certain project details.
	 * @param url certain project's url like this "http://114.215.188.21/api/v3/projects/1091/"
	 * @param charset
	 * @param token url and token constitute target url "http://114.215.188.21/api/v3/projects/1091/?private_token=:token" 
	 */
	Project downloadProject(String url, String charset, String token);
	

	List<Project> downloadProject(Group group, From platform, String token);
}
