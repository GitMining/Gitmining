package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.em.ProjectEM;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import iSESniper.code.entity.em.EmptyMethodEntity;

import java.util.List;
import java.util.Map;

/**
 * @author VousAttendezrer
 *
 */
public interface ProjectEmptyMethod extends ProjectCodeCheck<ProjectEM>{
    Map<AnalysisRank, List<EmptyMethodEntity>> getRanks(ProjectEM projectEM);
}
