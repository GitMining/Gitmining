package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectLinesOfCodeDao;
import cn.nju.iSE.GitMining.model.code.loc.MethodLOCEntity;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.loc.JavaLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.NonJavaLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.mongo.code.ProjectLOCMongoRepository;
import cn.nju.iSE.GitMining.service.code.ProjectLinesOfCode;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;
import iSESniper.code.entity.loc.JavaLinesOfCodeEntity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.impl.ProjectLinesOfCodeImpl.java
 *  
 *  2018年3月18日	下午2:32:29  
*/
@Service
public class ProjectLinesOfCodeImpl implements ProjectLinesOfCode {
	
	@Autowired
	private ProjectLOCMongoRepository projectLOCMongoRepository;

	@Autowired
	private ProjectLinesOfCodeDao projectLinesOfCodeDao;

	@Override
	public long create(String prjtPth, String groupName, long pId) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(projectLOCMongoRepository.exists(pId))
				projectLOCMongoRepository.delete(pId);
			if( prjtPth == null || prjtPth.trim().equals("")) {
				return -1;
			}
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			long version = 0L;
			String name = pAnalyzer.getPrjtName();
			List<NonJavaLOCEntity> nonJavaLOCEntities = new ArrayList<NonJavaLOCEntity>();
			List<FileLinesOfCodeEntity> nonjavaLinesOfCodeEntities = pAnalyzer.getNJLOC();
			for(FileLinesOfCodeEntity fe: nonjavaLinesOfCodeEntities) {
				if(fe == null)
					continue;
				nonJavaLOCEntities.add(new NonJavaLOCEntity(fe));
			}
			List<JavaLOCEntity> javaLOCEntities = new ArrayList<JavaLOCEntity>();
			List<JavaLinesOfCodeEntity> javaLinesOfCodeEntities = pAnalyzer.getJLOC();
			for(JavaLinesOfCodeEntity je: javaLinesOfCodeEntities) {
				javaLOCEntities.add(new JavaLOCEntity(je));
			}
			ProjectLOC projectLOC = new ProjectLOC(pId, prjtPth, name, groupName, version, nonJavaLOCEntities, javaLOCEntities);
			projectLOCMongoRepository.save(projectLOC);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectLOC> getProjects(String name) {
		return projectLOCMongoRepository.findByProjectName(name);
	}

	@Override
	public List<ProjectLOC> getProjectsByPth(String pth) {
		return projectLOCMongoRepository.findByProjectPath(pth);
	}

	@Override
	public ProjectLOC getProject(long id) {
		return projectLOCMongoRepository.findById(id);
	}

	@Override
	public ProjectLOC getProject(String groupName, String projectName) {
		return projectLOCMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectLOCMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectLOC oldLOC = projectLOCMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			if(oldLOC == null)
				return;
			long version = oldLOC.getVersion() + 1;
			long pId = oldLOC.getId();
			List<NonJavaLOCEntity> nonJavaLOCEntities = new ArrayList<NonJavaLOCEntity>();
			List<FileLinesOfCodeEntity> nonjavaLinesOfCodeEntities = pAnalyzer.getNJLOC();
			for(FileLinesOfCodeEntity fe: nonjavaLinesOfCodeEntities) {
				nonJavaLOCEntities.add(new NonJavaLOCEntity(fe));
			}
			List<JavaLOCEntity> javaLOCEntities = new ArrayList<JavaLOCEntity>();
			List<JavaLinesOfCodeEntity> javaLinesOfCodeEntities = pAnalyzer.getJLOC();
			for(JavaLinesOfCodeEntity je: javaLinesOfCodeEntities) {
				javaLOCEntities.add(new JavaLOCEntity(je));
			}
			ProjectLOC projectLOC = new ProjectLOC(pId, prjtPth, projectName, groupName, version, nonJavaLOCEntities, javaLOCEntities);
			projectLinesOfCodeDao.update(projectLOC);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectLOC projectLOC) {
		List<JavaLOCEntity> entities =  projectLOC.getJavaLOCEntities();
		List<Object> awfuls = new ArrayList<>();
		for (JavaLOCEntity entity: entities){
			List<MethodLOCEntity> locEntities = entity.getMethodList();
			for(MethodLOCEntity locEntity : locEntities)
				if(locEntity.getLines() > Constant.CodeQualityIndicator.LOC_WARNING_AWFUL)
					awfuls.add(locEntity);
		}
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectLOC projectLOC) {
		List<JavaLOCEntity> entities =  projectLOC.getJavaLOCEntities();
		List<Object> warnings = new ArrayList<>();
		for (JavaLOCEntity entity: entities){
			List<MethodLOCEntity> locEntities = entity.getMethodList();
			for(MethodLOCEntity locEntity : locEntities)
				if(locEntity.getLines() > Constant.CodeQualityIndicator.LOC_INFO_WARNING
						&& locEntity.getLines() <= Constant.CodeQualityIndicator.LOC_WARNING_AWFUL)
					warnings.add(locEntity);
		}
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectLOC projectLOC) {
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<JavaLOCEntity> entities =  projectLOC.getJavaLOCEntities();
		List<Object> awfuls = new ArrayList<>();
		List<Object> warnings = new ArrayList<>();
		for (JavaLOCEntity entity: entities){
			List<MethodLOCEntity> locEntities = entity.getMethodList();
			for(MethodLOCEntity locEntity : locEntities)
				if(locEntity.getLines() > Constant.CodeQualityIndicator.LOC_WARNING_AWFUL)
					awfuls.add(locEntity);
				else if(locEntity.getLines() > Constant.CodeQualityIndicator.LOC_INFO_WARNING
						&& locEntity.getLines() <= Constant.CodeQualityIndicator.LOC_WARNING_AWFUL)
					warnings.add(locEntity);
		}
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

}
