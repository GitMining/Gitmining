package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.code.ProjectDuplicationCodeDao;
import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;
import cn.nju.iSE.GitMining.mongo.code.ProjectDCMongoRepositity;
import cn.nju.iSE.GitMining.service.code.ProjectDuplicationCode;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.code.analyzer.cpd.DuplicatedCodeDetector;
import iSESniper.code.analyzer.cpd.impl.DuplicatedCodeDetectorImpl;
import iSESniper.code.entity.cpd.CpdEntity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 * @author VousAttendezrer
 *
 */
@Service
public class ProjectDuplicationCodeImpl implements ProjectDuplicationCode {

	@Autowired
	private ProjectDCMongoRepositity projectDCMongoRepository;
	@Autowired
	private ProjectDuplicationCodeDao projectDuplicationCodeDao;
	@Override
	public long create(String prjtPth, String groupName, long pId) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(projectDCMongoRepository.exists(pId)) {
				projectDCMongoRepository.delete(pId);
			}
			if(prjtPth == null || prjtPth.trim().equals("")) {
				return -1;
			}
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			long version = 0;
			String name = pAnalyzer.getPrjtName();
			String myProjectPath = prjtPth.replaceAll("/","\\\\");
			DuplicatedCodeDetector codeDetector = new DuplicatedCodeDetectorImpl();
			List<CpdEntity> cpdEntities = codeDetector.findDuplicatedCodeList(myProjectPath);
			ProjectDC projectDC = new ProjectDC(pId, prjtPth, name, version, cpdEntities);
			projectDCMongoRepository.save(projectDC);
			return version;
		}catch (PathNotFoundException |FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectDC> getProjects(String name) {
		return projectDCMongoRepository.findByProjectName(name);
	}

	@Override
	public List<ProjectDC> getProjectsByPth(String pth) {
		return projectDCMongoRepository.findByProjectPath(pth);
	}

	@Override
	public ProjectDC getProject(long id) {
		return projectDCMongoRepository.findById(id);
	}

	@Override
	public ProjectDC getProject(String groupName, String projectName) {
		return projectDCMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectDCMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectDC oldLD = projectDCMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			long version = oldLD.getVersion() + 1;
			long pId = oldLD.getId();
			String name = pAnalyzer.getPrjtName();
			DuplicatedCodeDetector codeDetector = new DuplicatedCodeDetectorImpl();
			List<CpdEntity> cpdEntities = codeDetector.findDuplicatedCodeList(prjtPth);
			ProjectDC projectDC = new ProjectDC(pId, prjtPth, name, groupName, version, cpdEntities);
			projectDuplicationCodeDao.update(projectDC);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectDC projectDC) {
		List<CpdEntity> entities = projectDC.getCpdEntities();
		List<Object> awfuls = new ArrayList<>();
		for(CpdEntity entity: entities)
			if(entity.getDuplicateLineCount() > Constant.CodeQualityIndicator.DC_WARNING_AWFUL)
				awfuls.add(entity);
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectDC projectDC) {
		List<CpdEntity> entities = projectDC.getCpdEntities();
		List<Object> warnings = new ArrayList<>();
		for(CpdEntity entity: entities)
			if(entity.getDuplicateLineCount() <= Constant.CodeQualityIndicator.DC_WARNING_AWFUL)
				warnings.add(entity);
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectDC projectDC) {
		List<CpdEntity> entities = projectDC.getCpdEntities();
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<Object> warnings = new ArrayList<>();
		List<Object> awfuls = new ArrayList<>();
		for(CpdEntity entity: entities) {
			if (entity.getDuplicateLineCount() <= Constant.CodeQualityIndicator.DC_WARNING_AWFUL)
				warnings.add(entity);
			else
				awfuls.add(entity);
		}
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

	@Override
	public Map<AnalysisRank, List<CpdEntity>> getRanks(ProjectDC projectDC) {
		Map<AnalysisRank, List<CpdEntity>> ranks = new HashMap<>();
		List<CpdEntity> cpdEntities = projectDC.getCpdEntities();
		for(CpdEntity entity: cpdEntities){
			int lines = entity.getDuplicateLineCount();
			if(lines >= 20){
				List<CpdEntity> awfulList = ranks.get(AnalysisRank.AWFUL);
				awfulList = awfulList == null ? new ArrayList<>(): awfulList;
				awfulList.add(entity);
			}else{
				List<CpdEntity> warningList = ranks.get(AnalysisRank.WARNING);
				warningList = warningList == null ? new ArrayList<>(): warningList;
				warningList.add(entity);
			}
		}
		return ranks;
	}
}
