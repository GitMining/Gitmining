package cn.nju.iSE.GitMining.service.system.impl;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;
import cn.nju.iSE.GitMining.model.code.em.ProjectEM;
import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;
import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.IndicatoreScore;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectScore;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.ProjectMongoRepository;
import cn.nju.iSE.GitMining.mongo.system.ProjectScoreMongoRepository;
import cn.nju.iSE.GitMining.service.code.*;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.service.system.ProjectScoreService;
import cn.nju.iSE.GitMining.service.system.ProjectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProjectScoreServiceImpl implements ProjectScoreService {
    @Resource
    private ProjectScoreMongoRepository projectScoreMongoRepository;
    @Resource
    private ProjectService projectService;
    @Resource
    private GroupService groupService;
    @Resource
    private ProjectLinesOfCode projectLinesOfCode;
    @Resource
    private ProjectCyclomaticComplexity projectCyclomaticComplexity;
    @Resource
    private ProjectLogicDepth projectLogicDepth;
    @Resource
    private ProjectParameters projectParameters;
    @Resource
    private ProjectVariablesDeclaration projectVariablesDeclaration;
    @Resource
    private ProjectDuplicationCode projectDuplicationCode;
    @Resource
    private ProjectEmptyMethod projectEmptyMethod;
    @Resource
    private ProjectLineOfAnnotation projectLineOfAnnotation;


    @Override
    public ProjectScore calculateProjectIndicators(long id) {

        ProjectScore projectScore = new ProjectScore();
        Project project = projectService.getProject(id);
        projectScore.setId(id);
        projectScore.setGroupName(project.getNamespace());
        projectScore.setProjectName(project.getName());
        projectScore.setProjectUrl(project.getUrl());
        setIndicators(projectScore, id);
        return projectScore;
    }

    @Override
    public ProjectScore calculateProjectIndicators(String projectName, String groupName) {
        Project project = projectService.getProjects(projectName, groupName);
        ProjectScore projectScore = new ProjectScore();
        projectScore.setId(project.getId());
        projectScore.setGroupName(project.getNamespace());
        projectScore.setProjectName(project.getName());
        projectScore.setProjectUrl(project.getUrl());
        setIndicators(projectScore, project.getId());
        return projectScore;
    }

    private void setIndicators(ProjectScore projectScore, long id) {
        List<IndicatoreScore> indicatoreScores = new ArrayList<>();

        ProjectLOC projectLOC = projectLinesOfCode.getProject(id);
        Map<String, List<Object>> issueLists = projectLinesOfCode.getIssueLists(projectLOC);
        IndicatoreScore locIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.LOC, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setLocScore(locIndicator);
        indicatoreScores.add(locIndicator);

        ProjectCC projectCC = projectCyclomaticComplexity.getProject(id);
        issueLists = projectCyclomaticComplexity.getIssueLists(projectCC);
        IndicatoreScore ccIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.CC, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setCcScore(ccIndicator);
        indicatoreScores.add(ccIndicator);

        ProjectLD projectLD = projectLogicDepth.getProject(id);
        issueLists = projectLogicDepth.getIssueLists(projectLD);
        IndicatoreScore ldIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.CC, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setLdScore(ldIndicator);
        indicatoreScores.add(ldIndicator);

        ProjectParams projectParams = projectParameters.getProject(id);
        issueLists = projectParameters.getIssueLists(projectParams);
        IndicatoreScore pdIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.PD, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setPdScore(pdIndicator);
        indicatoreScores.add(pdIndicator);

        ProjectVabsDec projectVabsDec = projectVariablesDeclaration.getProject(id);
        issueLists = projectVariablesDeclaration.getIssueLists(projectVabsDec);
        IndicatoreScore vdIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.VD, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setVdScore(vdIndicator);
        indicatoreScores.add(vdIndicator);

        ProjectLOA projectLOA = projectLineOfAnnotation.getProject(id);
        issueLists = projectLineOfAnnotation.getIssueLists(projectLOA);
        IndicatoreScore loaIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.LOA, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setLoaScore(loaIndicator);
        indicatoreScores.add(loaIndicator);

        ProjectEM projectEM = projectEmptyMethod.getProject(id);
        issueLists = projectEmptyMethod.getIssueLists(projectEM);
        IndicatoreScore emIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.EM, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setEmScore(emIndicator);
        indicatoreScores.add(emIndicator);

        ProjectDC projectDC = projectDuplicationCode.getProject(id);
        issueLists = projectDuplicationCode.getIssueLists(projectDC);
        IndicatoreScore dcIndicator = new IndicatoreScore(Constant.CodeQualityIndicator.DC, true,
                Constant.CodeQualityIndicator.THRESHOLD,
                issueLists.get(Constant.CodeQualityIndicator.WARNING),
                issueLists.get(Constant.CodeQualityIndicator.AWFUL));
        projectScore.setDcScore(dcIndicator);
        indicatoreScores.add(dcIndicator);

        projectScore.setIndicatoreScores(indicatoreScores);
    }

    @Override
    public List<ProjectScore> calculateProjectsIndicators(String groupName) {
        Group group = groupService.getGroup(groupName);
        List<ProjectScore> projectScores = new ArrayList<>();
        for(Long id: group.getProjectIds())
            projectScores.add(calculateProjectIndicators(id));
        return projectScores;
    }

    @Override
    public List<ProjectScore> calculateProjectsIndicators(long gId) {
        Group group = groupService.getGroup(gId);
        List<ProjectScore> projectScores = new ArrayList<>();
        for(Long id: group.getProjectIds())
            projectScores.add(calculateProjectIndicators(id));
        return projectScores;
    }

    @Override
    public ProjectScore getProjectIndicators(long id) {
        return projectScoreMongoRepository.findById(id);
    }

    @Override
    public ProjectScore getProjectIndicators(String groupName, String projectName) {
        return projectScoreMongoRepository.findByProjectNameAndGroupName(projectName, groupName);
    }

    @Override
    public List<ProjectScore> getProjectsIndicators(String groupName) {
        return projectScoreMongoRepository.findByGroupName(groupName);
    }

    @Override
    public ProjectScore save(ProjectScore projectScore) {
        return projectScoreMongoRepository.save(projectScore);
    }

    @Override
    public List<ProjectScore> save(List<ProjectScore> projectScores) {
        return projectScoreMongoRepository.save(projectScores);
    }
}
