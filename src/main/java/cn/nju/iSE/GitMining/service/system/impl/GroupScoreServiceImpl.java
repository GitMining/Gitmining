package cn.nju.iSE.GitMining.service.system.impl;

import cn.nju.iSE.GitMining.model.system.*;
import cn.nju.iSE.GitMining.mongo.system.GroupScoreMongoRepository;
import cn.nju.iSE.GitMining.service.system.GroupScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupScoreServiceImpl implements GroupScoreService {

    @Autowired
    private GroupScoreMongoRepository groupScoreMongoRepository;

    @Override
    public GroupScore calculateGroupScore(Group group, List<ProjectScore> projectScores) {
        GroupScore groupScore = initGroupScore(group);
        for(ProjectScore projectScore: projectScores){
            if(groupScore.getPhaseI() < 0)
                getPhasePoint(groupScore, projectScore, 1);
            else if(groupScore.getPhaseII() < 0)
                getPhasePoint(groupScore, projectScore, 2);
            else if(groupScore.getPhaseIII() < 0)
                getPhasePoint(groupScore, projectScore, 3);
        }
        return groupScore;
    }

    private void getPhasePoint(GroupScore groupScore, ProjectScore projectScore, int i) {
        int point = 0;
        for(IndicatoreScore indicatoreScore: projectScore.getIndicatoreScores()){
            if(indicatoreScore.isScore())
                point++;
        }
        switch (i){
            case 1: groupScore.setPhaseI(point);break;
            case 2: groupScore.setPhaseII(point);break;
            case 3: groupScore.setPhaseIII(point);break;
        }
    }

    @Override
    public GroupScore initGroupScore(Group group) {
        GroupScore groupScore = new GroupScore();
        if(group == null)
            return groupScore;
        groupScore.setId(group.getId());
        groupScore.setGroupName(group.getOwnerName());
        groupScore.setGroupUrl(group.getOwnerURL());
        groupScore.setPhaseI(-1);
        groupScore.setPhaseII(-1);
        groupScore.setPhaseIII(-1);
        groupScore.setWeightI(1);
        groupScore.setWeightII(1.5);
        groupScore.setWeightIII(3);
        return groupScore;
    }

    @Override
    public void save(GroupScore groupScore) {
        groupScoreMongoRepository.save(groupScore);
    }

    @Override
    public void save(List<GroupScore> groupScores) {
        groupScoreMongoRepository.save(groupScores);
    }

    @Override
    public GroupScore getGroupScore(long id) {
        return  groupScoreMongoRepository.findById(id);
    }

    @Override
    public List<GroupScore> getGroupScore(List<Long> ids) {
        return groupScoreMongoRepository.findAllByIdIn(ids);
    }
}
