package cn.nju.iSE.GitMining.service.cooperation.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.cooperation.ProjectFilesLifeCircleDao;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;
import cn.nju.iSE.GitMining.model.cooperation.wrapper.CommitDetailWrapper;
import cn.nju.iSE.GitMining.mongo.cooperation.ProjectFilesLCMongoRepository;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.common.util.Constant;
import iSESniper.cooperation.commit.extractor.FileLifeCircleExtractor;
import iSESniper.cooperation.entity.FileLifeCircle;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.service.code.impl.ProjectFilesLifeCircleServiceImpl.java
 * 
 *           2018年4月25日 下午7:13:03
 */
@Service
public class ProjectFilesLifeCircleServiceImpl implements ProjectFilesLifeCircleService {

	@Autowired
	private CommitDetailWrapper commitDetailWrapper;

	@Autowired
	private ProjectFilesLCMongoRepository projectFilesLCMongoRepository;

	@Autowired
	private ProjectFilesLifeCircleDao projectFilesLifeCircleDao;

	@Override
	public ProjectFilesLifeCircle getProjectFilesLifeCircle(long id) {
		return projectFilesLCMongoRepository.findById(id);
	}

	@Override
	public ProjectFilesLifeCircle getProjectFilesLifeCircle(String projectName, String groupName) {
		return projectFilesLCMongoRepository.findByProjectNameAndGroupName(projectName, groupName);
	}

	@Override
	public ProjectFilesLifeCircle extractAndSave(Project project, List<CommitDetail> commits) {
		if (project == null)
			return null;
		ProjectFilesLifeCircle projectFilesLifeCircle = new ProjectFilesLifeCircle();
		FileLifeCircleExtractor fileLifeCircleExtractor = new FileLifeCircleExtractor(project.getFullName());
		List<String> fileTypes = new ArrayList<String>();
		fillTypeList(fileTypes);
		List<String> types = new ArrayList<String>();
		Collections.addAll(types, Constant.FileTypes);
		fileLifeCircleExtractor.extractFileLifeCircle(commitDetailWrapper.unwrap(commits), types);
		List<FileLifeCircle> fileLifeCircles = fileLifeCircleExtractor.getFileLifeCircles();
		projectFilesLifeCircle.setFileLifeCircles(fileLifeCircles);
		projectFilesLifeCircle.setId(project.getId());
		projectFilesLifeCircle.setGroupName(project.getNamespace());
		projectFilesLifeCircle.setProjectName(project.getName());
		projectFilesLCMongoRepository.save(projectFilesLifeCircle);
//		else
//			projectFilesLifeCircleDao.update(projectFilesLifeCircle);
		return projectFilesLifeCircle;
	}

	private void fillTypeList(List<String> fileTypes) {
		fileTypes = fileTypes == null ? new ArrayList<String>() : fileTypes;
		Collections.addAll(fileTypes, Constant.FileTypes);
	}
}
