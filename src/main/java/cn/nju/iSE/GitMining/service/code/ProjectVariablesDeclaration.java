package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectVariablesDeclaration.java
 *  
 *  2018年3月19日	下午3:36:01  
*/

public interface ProjectVariablesDeclaration extends ProjectCodeCheck<ProjectVabsDec> {

}
