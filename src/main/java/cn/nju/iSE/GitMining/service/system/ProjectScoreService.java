package cn.nju.iSE.GitMining.service.system;

import cn.nju.iSE.GitMining.model.system.ProjectScore;

import java.util.List;

public interface ProjectScoreService {
    public ProjectScore calculateProjectIndicators(long id);

    public ProjectScore calculateProjectIndicators(String projectName, String groupName);

    public List<ProjectScore> calculateProjectsIndicators(String groupName);

    public List<ProjectScore> calculateProjectsIndicators(long gId);

    public ProjectScore getProjectIndicators(long id);

    public ProjectScore getProjectIndicators(String groupName, String projectName);

    public List<ProjectScore> getProjectsIndicators(String groupName);

    public ProjectScore save(ProjectScore projectScore);

    public List<ProjectScore> save(List<ProjectScore> projectScores);
}
