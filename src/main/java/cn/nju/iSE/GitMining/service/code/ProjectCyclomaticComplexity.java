package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectCyclomaticComplexity.java
 *  
 *  2018年3月19日	上午9:40:33  
*/

public interface ProjectCyclomaticComplexity extends ProjectCodeCheck<ProjectCC> {
	
}
