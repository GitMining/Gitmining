package cn.nju.iSE.GitMining.service.system;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.nju.iSE.GitMining.model.system.Group;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.GroupService.java
 *  
 *  2018年4月23日	上午10:21:05  
*/

public interface GroupService {
	
	/**
	 * Get group by group id.
	 * @param id
	 * @return
	 */
	Group getGroup(long id);
	
	/**
	 * Get group by group name;
	 * @param name
	 * @return
	 */
	Group getGroup(String name);
	
	/**
	 * This method get all groups that under analyst controls.
	 * @return all groups list
	 */
	List<Group> getGroups();
	
	/**
	 * This method get groups that under analyst controls,
	 * and the number of groups depends on the parameter "pageable".
	 * @param pageable
	 * @return
	 */
	Page<Group> getGroups(Pageable pageable);
	
	/**
	 * This method get groups that were controlled between certain period. 
	 * @param start
	 * @param end
	 * @return
	 */
	List<Group> getGroups(long start, long end);
	
	/**
	 * This method get groups that were controlled between certain period. 
	 * The number of groups depends on the parameter "pageable".
	 * @param start
	 * @param end
	 * @param pageable
	 * @return
	 */
	Page<Group> getGroups(long start, long end, Pageable pageable);
	
	/**
	 * Save certain group.
	 * @param group
	 */
	void save(Group group);
	
	/**
	 * Save groups list.
	 * @param groups
	 */
	void save(List<Group> groups);
	
	/**
	 * Update certain group's projects' id list.
	 * If you need to use the method, make sure the parameters of group instance are filled.
	 * @param group
	 */
	void appendGroupProjects(Group group);
	
	/**
	 * This method get groups' basic information which is not added into database
	 * without informations of members and projects' id.
	 * @param url list group url, like this "http://114.215.188.21/api/v3/groups/".
	 * @param charset 
	 * @return return the list of groups with basic information.
	 */
    public List<Group> downloadGroups(String url, String charset);
}
