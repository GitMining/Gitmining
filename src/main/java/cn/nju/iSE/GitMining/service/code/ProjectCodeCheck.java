package cn.nju.iSE.GitMining.service.code;

import java.util.List;
import java.util.Map;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectCodeCheck.java
 *  
 *  2018年3月19日	上午9:51:08  
*/

public interface ProjectCodeCheck<T> {
	public long create(String prjtPth, String groupName, long pId);
	public List<T> getProjects(String name);
	public List<T> getProjectsByPth(String pth);
	public T getProject(long id);
	public T getProject(String groupName, String projectName);
	public long countProjectsBypath(String pth);
    void update(String groupName, String projectName);
    public List<Object> getAwfulList(T t);
    public List<Object> getWarningList(T t);
    public Map<String, List<Object>> getIssueLists(T t);
}
