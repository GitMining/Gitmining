package cn.nju.iSE.GitMining.service.auth;

import com.alibaba.fastjson.JSONObject;

public interface LoginService {
    public JSONObject authLogin(JSONObject jsonObject);

    public JSONObject logout(JSONObject jsonObject);
}
