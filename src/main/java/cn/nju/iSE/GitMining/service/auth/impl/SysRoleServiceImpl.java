package cn.nju.iSE.GitMining.service.auth.impl;

import java.util.ArrayList;
import java.util.List;

import cn.nju.iSE.GitMining.dao.auth.SysRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.mongo.auth.SysRoleMongoRepository;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.service.auth.SysRoleService;

import javax.annotation.Resource;

@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Resource
    private SysRoleMongoRepository sysRoleMongoRepository;

    @Resource
    private SysPermissionService sysPermissionService;

    @Resource
    private SysRoleDao sysRoleDao;

    @Override
    public SysRole getRole(int id) {
        return sysRoleMongoRepository.findById(id);
    }

    @Override
    public void addRole(JSONObject jsonObject) {
        String roleName = jsonObject.getString("roleName");
        String description = jsonObject.getString("description");
        JSONArray jsonArray = jsonObject.getJSONArray("permissionIds");
        List<Integer> permissionIds = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            int pId = jsonArray.getIntValue(i);
            if (sysPermissionService.checkPermissionId(pId))
                permissionIds.add(pId);
        }
        long rId = sysRoleMongoRepository.count() + 1;
        SysRole role = new SysRole();
        role.setId(rId);
        role.setRoleName(roleName);
        role.setDescription(description);
        role.setPermissionIds(permissionIds);
        sysRoleMongoRepository.save(role);
    }

    @Override
    public int appendPermission(int rId, int[] pIds) {
        SysRole role = sysRoleMongoRepository.findById(rId);
        if(role == null)
            return -1;
        int num = 0;
        List<Integer> realPerms = new ArrayList<>();
        for(int i: pIds)
            if(sysPermissionService.checkPermissionId(i)){
                realPerms.add(i);
                num++;
            }
        role.setPermissionIds(realPerms);
        sysRoleDao.update(role);
        return num;
    }

    @Override
    public int appendPermission(String rName, int[] pIds) {
        long rId = sysRoleMongoRepository.findByRoleName(rName).getId();
        return appendPermission((int)rId, pIds);
    }

    @Override
    public int removePermission(int rId, int[] pIds) {
        // TODO
        return 0;
    }

    @Override
    public int removePermission(String rName, int[] pIds) {
        // TODO
        return 0;
    }
}
