package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectLogicDepthDao;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.logic.MethodLD;
import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import cn.nju.iSE.GitMining.mongo.code.ProjectLDMongoRepository;
import cn.nju.iSE.GitMining.service.code.ProjectLogicDepth;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.service.code.impl.ProjectLogicDepthImpl.java
 * 
 *           2018年3月18日 下午1:15:45
 */
@Service
public class ProjectLogicDepthImpl implements ProjectLogicDepth {

	@Autowired
	private ProjectLDMongoRepository projectLDMongoRepository;

	@Autowired
	private ProjectLogicDepthDao projectLogicDepthDao;

	public long create(String prjtPth, String groupName, long pId){
		ProjectAnalyzer pAnalyzer;
		try {
			if(projectLDMongoRepository.exists(pId))
				projectLDMongoRepository.delete(pId);
			if(prjtPth == null || prjtPth.trim().equals("")) {
				return -1;
			}
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			long version = 0;
			String name = pAnalyzer.getPrjtName();
			List<Map<String, Integer>> methods = pAnalyzer.getMethodsLD();
			List<MethodLD> mLds = new ArrayList<MethodLD>();
			for (Map<String, Integer> m : methods) {
				String pth = m.keySet().iterator().next();
				mLds.add(new MethodLD(pth, m.get(pth)));
			}
			ProjectLD projectLD = new ProjectLD(pId, prjtPth, name, groupName, version, mLds);
			projectLDMongoRepository.save(projectLD);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectLD> getProjects(String name) {
		return projectLDMongoRepository.findByProjectName(name);
	}

	@Override
	public ProjectLD getProject(long id) {
		return projectLDMongoRepository.findById(id);
	}

	@Override
	public ProjectLD getProject(String groupName, String projectName) {
		return projectLDMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public List<ProjectLD> getProjectsByPth(String pth) {
		return projectLDMongoRepository.findByProjectPath(pth);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectLDMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectLD oldLD = projectLDMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			long version = oldLD.getVersion() + 1;
			long pId = oldLD.getId();
			String name = pAnalyzer.getPrjtName();
			List<Map<String, Integer>> methods = pAnalyzer.getMethodsLD();
			List<MethodLD> mLds = new ArrayList<MethodLD>();
			for (Map<String, Integer> m : methods) {
				String pth = m.keySet().iterator().next();
				mLds.add(new MethodLD(pth, m.get(pth)));
			}
			ProjectLD projectLD = new ProjectLD(pId, prjtPth, name, groupName, version, mLds);
			projectLogicDepthDao.update(projectLD);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectLD projectLD) {
		List<MethodLD> methodLDS = projectLD.getMethodLDs();
		List<Object> awfuls = new ArrayList<>();
		for(MethodLD methodLD: methodLDS)
			if(methodLD.getLdIdx() > Constant.CodeQualityIndicator.LD_WARNING_AWFUL)
				awfuls.add(methodLD);
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectLD projectLD) {
		List<MethodLD> methodLDS = projectLD.getMethodLDs();
		List<Object> warnings = new ArrayList<>();
		for(MethodLD methodLD: methodLDS)
			if(methodLD.getLdIdx() <= Constant.CodeQualityIndicator.LD_WARNING_AWFUL
					&& methodLD.getLdIdx() > Constant.CodeQualityIndicator.LD_INFO_WARNING)
				warnings.add(methodLD);
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectLD projectLD) {
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<Object> awfuls = new ArrayList<>();
		List<Object> warnings = new ArrayList<>();
		List<MethodLD> methodLDS = projectLD.getMethodLDs();
		for(MethodLD methodLD: methodLDS)
			if(methodLD.getLdIdx() <= Constant.CodeQualityIndicator.LD_WARNING_AWFUL
					&& methodLD.getLdIdx() > Constant.CodeQualityIndicator.LD_INFO_WARNING)
				warnings.add(methodLD);
			else if(methodLD.getLdIdx() > Constant.CodeQualityIndicator.LD_WARNING_AWFUL)
				awfuls.add(methodLD);
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

}
