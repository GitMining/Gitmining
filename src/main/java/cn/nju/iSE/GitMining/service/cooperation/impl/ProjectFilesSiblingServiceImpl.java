package cn.nju.iSE.GitMining.service.cooperation.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesSibling;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesSiblingService;
import iSESniper.cooperation.entity.Action;
import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.FileLifeCircle;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.service.cooperation.impl.ProjectFilesSiblingServiceImpl.java
 * 
 *           2018年4月25日 下午9:25:00
 */

@Service
public class ProjectFilesSiblingServiceImpl implements ProjectFilesSiblingService {

	@Override
	public ProjectFilesSibling getSiblings(ProjectFilesLifeCircle projectFilesLifeCircle) {
		ProjectFilesSibling projectFilesSibling = new ProjectFilesSibling();
		projectFilesSibling.setGroupName(projectFilesLifeCircle.getGroupName());
		projectFilesSibling.setId(projectFilesLifeCircle.getId());
		projectFilesSibling.setProjectName(projectFilesLifeCircle.getProjectName());
		List<FileLifeCircle> fileLifeCircles = projectFilesLifeCircle.getFileLifeCircles();
		Map<Author, List<String>> sblings = new HashMap<Author, List<String>>();
		List<String> files;
		for (FileLifeCircle fCircle : fileLifeCircles) {
			Author author = fCircle.getActions().get(0).getAuthor();
			files = sblings.get(author);
			files = files == null ? new ArrayList<String>() : files;
			files.add(fCircle.getFileName());
			sblings.put(author, files);
		}
		projectFilesSibling.setFilesSibling(sblings);
		return projectFilesSibling;
	}

	@Override
	public synchronized void getContributeRelation(ProjectFilesLifeCircle projectFilesLifeCircle,
			ProjectFilesSibling projectFilesSibling) {
		Map<Author, Map<Author, Integer>> contributeRelation = new HashMap<Author, Map<Author, Integer>>();
		Map<Author, Map<Author, List<String>>> contriFilesRelation = new HashMap<Author, Map<Author, List<String>>>();
		
		List<FileLifeCircle> fileLifeCircles = projectFilesLifeCircle.getFileLifeCircles();
		for (FileLifeCircle fCircle : fileLifeCircles) {
			List<Action> actions = fCircle.getActions();
			Author author = actions.get(0).getAuthor();
			String file = fCircle.getFileName();
			
			Map<Author, Integer> countributors = contributeRelation.get(author);
			Map<Author, List<String>> contributorFiles = contriFilesRelation.get(author);
			countributors = countributors == null ? new HashMap<Author, Integer>() : countributors;
			contributorFiles = contributorFiles == null ? new HashMap<Author, List<String>>(): contributorFiles;
			
			Author contributor;
			boolean flag = false;
			for (int i = 1; i < actions.size(); i++) {
				//Count the index of contributor contributes commits to author A's files.
				contributor = actions.get(i).getAuthor();
				if (contributor.equals(author))
					continue;
				Integer idx = countributors.get(contributor);
				idx = idx == null ? 1 : idx + 1;
				countributors.put(contributor, idx);
				
				// Count the contributor contributes files which belongs to author A.
				List<String> contributeFiles = contributorFiles.get(contributor);
				contributeFiles = contributeFiles == null ? new ArrayList<String>() : contributeFiles;
				if(flag)
					continue;
				contributeFiles.add(file);
				contributorFiles.put(contributor, contributeFiles);
				flag = true;
			}
			flag = false;
			contributeRelation.put(author, countributors);
			contriFilesRelation.put(author, contributorFiles);
		}
		projectFilesSibling.setContributeRel(contributeRelation);
		projectFilesSibling.setContriFilesRel(contriFilesRelation);
	}

	@Override
	public Set<String> getFileTypes(List<String> files) {
		Set<String> types;
		types = (files == null || files.size() == 0) ? null: new HashSet<String>();
		if(types == null)
			return null;
		for(String s: files) {
			int idx = s.lastIndexOf('.');
			if(idx < 0)
				continue;
			String type = s.substring(idx);
			types.add(type);
		}
		return types;
	}

	@Override
	public Map<Author, Map<String, List<String>>> getTypesFiles(Map<Author, List<String>> siblings) {
		Iterator<Author> iterator = siblings.keySet().iterator();
		Map<Author, Map<String, List<String>>> typesfiles = new HashMap<Author, Map<String, List<String>>>();
		List<String> fList;
		while(iterator.hasNext()) {
			Map<String, List<String>> typeFiles = new HashMap<>();
			Author author = iterator.next();
			List<String> files = siblings.get(author);
			for(String s: files) {
				String type = s.substring(s.lastIndexOf('.')+1).toUpperCase();
				fList = typeFiles.get(type);
				fList = fList == null ? new ArrayList<String>() : fList;
				fList.add(s);
				typeFiles.put(type, fList);
			}
			typesfiles.put(author, typeFiles);
		}
		return typesfiles;
	}

}
