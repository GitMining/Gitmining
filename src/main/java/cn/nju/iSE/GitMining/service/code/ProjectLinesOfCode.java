package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectLinesOfCode.java
 *  
 *  2018年3月18日	下午2:29:41  
*/

public interface ProjectLinesOfCode extends ProjectCodeCheck<ProjectLOC> {
}
