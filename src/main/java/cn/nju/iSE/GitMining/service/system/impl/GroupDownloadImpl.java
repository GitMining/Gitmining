package cn.nju.iSE.GitMining.service.system.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.common.util.Downloader;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;
import cn.nju.iSE.GitMining.service.system.GroupDownload;;

/**
 * @deprecated
 */
@Service
public class GroupDownloadImpl implements GroupDownload {
	
	@Autowired
	private GroupMongoRepository groupMongoRepository;
	
	/**
	 * This method get groups' basic information which is not added into database
	 * without informations of members and projects' id.
	 * @param url list group url, like this "http://114.215.188.21/api/v3/groups/?private_token=:token&page=:page&per_page=:perpage".
	 * @param charset 
	 * @return return the list of groups with basic information.
	 */
    public List<Group> downloadGroups(String url, String charset){
        List<Group> groups = new ArrayList<Group>();
        Group group;
        JSONArray arrays = Downloader.getJsonArray(url, charset);
        for(int i = 0; i< arrays.size(); i++) {
        	JSONObject object = arrays.getJSONObject(i);
        	group = new Group();
        	long id = object.getLong("id");
        	long ownerId = id;
        	String ownerName = object.getString("name");
        	group.setId(id);
        	group.setOwnerId(ownerId);
        	group.setOwnerName(ownerName);
        	group.setTime(new Date().getTime());
        	group.setPlatform(From.GitLab_SECIII);
        	if(!groupMongoRepository.exists(id))
        		groups.add(group);
        }
        return groups;
    }

    public void saveGroups(List<Group> groups){
        groupMongoRepository.save(groups);
    }

    public void saveGroup(Group group){
    	groupMongoRepository.save(group);
    }
}
