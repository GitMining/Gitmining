package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.code.ProjectEmptyMethodDao;
import cn.nju.iSE.GitMining.model.code.em.ProjectEM;
import cn.nju.iSE.GitMining.mongo.code.ProjectEMMongoResponsitity;
import cn.nju.iSE.GitMining.service.code.ProjectEmptyMethod;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.code.analyzer.em.EmptyMethod;
import iSESniper.code.analyzer.em.impl.EmptyMethodImpl;
import iSESniper.code.entity.em.EmptyMethodEntity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

@Service
public class ProjectEmptyMethodImpl implements ProjectEmptyMethod {
	@Autowired
	private ProjectEMMongoResponsitity projectEMMongoResponsitity;
	@Autowired
	private ProjectEmptyMethodDao projectEmptyMethodDao;

	@Override
	public long create(String prjtPth, String groupName, long pId) {
		ProjectAnalyzer pAnalyzer;
		try {
			if (projectEMMongoResponsitity.exists(pId)) {
				projectEMMongoResponsitity.delete(pId);
			}
			if (prjtPth == null || prjtPth.trim().equals("")) {
				return -1;
			}
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			long version = 0;
			String name = pAnalyzer.getPrjtName();
			EmptyMethod emptyMethod = new EmptyMethodImpl();
			List<EmptyMethodEntity> emptyMethodEntities = emptyMethod.findEmptyMethod(prjtPth);
			ProjectEM projectEM = new ProjectEM(pId, prjtPth, name, version, emptyMethodEntities);
			projectEMMongoResponsitity.save(projectEM);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectEM> getProjects(String name) {
		return projectEMMongoResponsitity.findByProjectName(name);
	}

	@Override
	public List<ProjectEM> getProjectsByPth(String pth) {
		return projectEMMongoResponsitity.findByProjectPath(pth);
	}

	@Override
	public ProjectEM getProject(long id) {
		return projectEMMongoResponsitity.findById(id);
	}

	@Override
	public ProjectEM getProject(String groupName, String projectName) {
		return projectEMMongoResponsitity.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectEMMongoResponsitity.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if (groupName == null || projectName == null || groupName.trim().equals("")
					|| projectName.trim().equals(""))
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectEM oldEM = projectEMMongoResponsitity.findByGroupNameAndProjectName(groupName, projectName);
			long version = oldEM.getVersion() + 1;
			long pId = oldEM.getId();
			String name = pAnalyzer.getPrjtName();
			EmptyMethod emptyMethod = new EmptyMethodImpl();
			List<EmptyMethodEntity> emptyMethodEntities = emptyMethod.findEmptyMethod(prjtPth);
			ProjectEM projectEM = new ProjectEM(pId, prjtPth, name, version, emptyMethodEntities);
			projectEmptyMethodDao.update(projectEM);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectEM projectEM) {
		return null;
	}

	@Override
	public List<Object> getWarningList(ProjectEM projectEM) {
        List<EmptyMethodEntity> entities = projectEM.getEmptyMethodEntities();
        List<Object> warnings = new ArrayList<>(entities);
        warnings.addAll(entities);
        return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectEM projectEM) {
	    Map<String, List<Object>> issueLists = new HashMap<>();
	    issueLists.put(Constant.CodeQualityIndicator.WARNING, getWarningList(projectEM));
		return issueLists;
	}

	@Override
	public Map<AnalysisRank, List<EmptyMethodEntity>> getRanks(ProjectEM projectEM) {
		List<EmptyMethodEntity> emptyMethodEntities = projectEM.getEmptyMethodEntities();
		Map<AnalysisRank, List<EmptyMethodEntity>> ranks = new HashMap<>();
		ranks.put(AnalysisRank.WARNING, new ArrayList<>());
		for(EmptyMethodEntity entity: emptyMethodEntities)
			ranks.get(AnalysisRank.WARNING).add(entity);
		return ranks;
	}
}
