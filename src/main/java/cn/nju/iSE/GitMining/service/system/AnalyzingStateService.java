package cn.nju.iSE.GitMining.service.system;

import cn.nju.iSE.GitMining.model.system.AnalyzingState;

public interface AnalyzingStateService {
    public AnalyzingState getProjectState(long id);
}
