package cn.nju.iSE.GitMining.service.code.impl;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectLineOfAnnoationDao;
import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;
import cn.nju.iSE.GitMining.mongo.code.ProjectLOAMongoResponsitity;
import cn.nju.iSE.GitMining.service.code.ProjectLineOfAnnotation;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.code.analyzer.loa.LOAInFolder;
import iSESniper.code.analyzer.loa.impl.LOAInFolderImpl;
import iSESniper.code.entity.loa.LineOfAnnotateInFilesEntity;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProjectLineOfAnnotationImpl implements ProjectLineOfAnnotation {
    @Autowired
    private ProjectLOAMongoResponsitity projectLOAMongoResponsitity;
    @Autowired
    private ProjectLineOfAnnoationDao projectLineOfAnnoationDao;

    @Override
    public long create(String prjtPth, String groupName, long pId) {
        ProjectAnalyzer pAnalyzer;
        try {
            if (projectLOAMongoResponsitity.exists(pId)) {
                projectLOAMongoResponsitity.delete(pId);
            }
            if (prjtPth == null || prjtPth.trim().equals("")) {
                return -1;
            }
            pAnalyzer = new ProjectAnalyzer(prjtPth);
            long version = 0;
            String name = pAnalyzer.getPrjtName();
            LOAInFolder loaInFolder = new LOAInFolderImpl();
            LineOfAnnotateInFilesEntity annotateInFilesEntity = loaInFolder.countLineInFolder(prjtPth);
            ProjectLOA projectLOA = new ProjectLOA(pId, prjtPth, name, version,
                    annotateInFilesEntity.getLinesOfAnnotate());
            projectLOAMongoResponsitity.save(projectLOA);
            return version;
        } catch (PathNotFoundException | FilesListIsEmptyException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public List<ProjectLOA> getProjects(String name) {
        return projectLOAMongoResponsitity.findByProjectName(name);
    }

    @Override
    public List<ProjectLOA> getProjectsByPth(String pth) {
        return projectLOAMongoResponsitity.findByProjectPath(pth);
    }

    @Override
    public ProjectLOA getProject(long id) {
        return projectLOAMongoResponsitity.findById(id);
    }

    @Override
    public ProjectLOA getProject(String groupName, String projectName) {
        return projectLOAMongoResponsitity.findByGroupNameAndProjectName(groupName, projectName);
    }

    @Override
    public long countProjectsBypath(String pth) {
        return 0;
    }

    @Override
    public void update(String groupName, String projectName) {
        ProjectAnalyzer pAnalyzer;
        try {
            if (groupName == null || projectName == null || groupName.trim().equals("")
                    || projectName.trim().equals(""))
                return;
            String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
            pAnalyzer = new ProjectAnalyzer(prjtPth);
            ProjectLOA oldLoa = projectLOAMongoResponsitity.findByGroupNameAndProjectName(groupName, projectName);
            long version = oldLoa.getVersion() + 1;
            long pId = oldLoa.getId();
            String name = pAnalyzer.getPrjtName();
            LOAInFolder loaInFolder = new LOAInFolderImpl();
            LineOfAnnotateInFilesEntity annotateInFilesEntity = loaInFolder.countLineInFolder(prjtPth);
            ProjectLOA projectLOA = new ProjectLOA(pId, prjtPth, name, version,
                    annotateInFilesEntity.getLinesOfAnnotate());
            projectLineOfAnnoationDao.update(projectLOA);
        } catch (PathNotFoundException | FilesListIsEmptyException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Object> getAwfulList(ProjectLOA projectLOA) {
        List<LineOfAnnotateInSingleFileEntity> entities = projectLOA.getLineOfAnnotateEntities();
        List<Object> awfuls = new ArrayList<>();
        for(LineOfAnnotateInSingleFileEntity entity: entities){
            double idx = (double)entity.getAnnotateCount() / (double)(entity.getTotalCount() - entity.getBlankCount());
            if(idx <= Constant.CodeQualityIndicator.LOA_WARNING_AWFUL)
                awfuls.add(entity);
        }
        return awfuls;
    }

    @Override
    public List<Object> getWarningList(ProjectLOA projectLOA) {
        List<LineOfAnnotateInSingleFileEntity> entities = projectLOA.getLineOfAnnotateEntities();
        List<Object> warnings = new ArrayList<>();
        for(LineOfAnnotateInSingleFileEntity entity: entities){
            double idx = (double)entity.getAnnotateCount() / (double)(entity.getTotalCount() - entity.getBlankCount());
            if(idx <= Constant.CodeQualityIndicator.LOA_INFO_WARNING
                    && idx > Constant.CodeQualityIndicator.LOA_WARNING_AWFUL)
                warnings.add(entity);
        }
        return warnings;
    }

    @Override
    public Map<String, List<Object>> getIssueLists(ProjectLOA projectLOA) {
        List<LineOfAnnotateInSingleFileEntity> entities = projectLOA.getLineOfAnnotateEntities();
        Map<String, List<Object>> issueLists = new HashMap<>();
        List<Object> warnings = new ArrayList<>();
        List<Object> awfuls = new ArrayList<>();
        for(LineOfAnnotateInSingleFileEntity entity : entities){
            double idx = (double)entity.getAnnotateCount() / (double)(entity.getTotalCount() - entity.getBlankCount());
            if(idx <= Constant.CodeQualityIndicator.LOA_INFO_WARNING
                    && idx > Constant.CodeQualityIndicator.LOA_WARNING_AWFUL)
                warnings.add(entity);
            else if(idx <= Constant.CodeQualityIndicator.LOA_WARNING_AWFUL)
                awfuls.add(entity);
        }
        issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
        issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
        return issueLists;
    }

    @Override
    public Map<AnalysisRank, List<LineOfAnnotateInSingleFileEntity>> getRanks(ProjectLOA projectLOA) {
        Map<AnalysisRank, List<LineOfAnnotateInSingleFileEntity>> ranks = new HashMap<>();
        List<LineOfAnnotateInSingleFileEntity> lineOfAnnotateInFilesEntities = projectLOA.getLineOfAnnotateEntities();
		for(LineOfAnnotateInSingleFileEntity entity: lineOfAnnotateInFilesEntities){
		    int anno = entity.getAnnotateCount();
		    int total = entity.getTotalCount();
		    int blk = entity.getBlankCount();
		    if((double)anno / (total-blk) == 0){
		        List<LineOfAnnotateInSingleFileEntity> awfulList = ranks.get(AnalysisRank.AWFUL);
		        awfulList = awfulList == null? new ArrayList<>(): awfulList;
		        awfulList.add(entity);
		        ranks.put(AnalysisRank.AWFUL, awfulList);
            }
            else if((double)anno / (total-blk) < 0.03){
                List<LineOfAnnotateInSingleFileEntity> warningList = ranks.get(AnalysisRank.WARNING);
                warningList = warningList == null? new ArrayList<>(): warningList;
                warningList.add(entity);
                ranks.put(AnalysisRank.WARNING, warningList);
            }
        }
        return ranks;
    }
}
