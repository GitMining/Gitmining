package cn.nju.iSE.GitMining.service.system.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.service.system.CommitsExtract;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.cooperation.commit.enumeration.Type;
import iSESniper.cooperation.commit.extractor.CommitsExtractor;
import iSESniper.cooperation.entity.Commit;
import iSESniper.cooperation.entity.FileDiff;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.service.system.impl.CommitsExtractImpl.java
 * 
 *           2018年4月23日 下午2:08:05
 */
@Service
public class CommitsExtractImpl implements CommitsExtract {

	@Override
	public List<CommitDetail> projectCommitsExtract(String path) {
		CommitsExtractor extractor = new CommitsExtractor(path);
		List<CommitDetail> commitDetails = new ArrayList<CommitDetail>();
		try {
			extractor.extractProjectCommits();
			String project = path.split(Route.CLONE_PATH)[1];
			transferCommits(extractor.getCommits(), commitDetails, project);
		} catch (IOException | NoHeadException e) {
			e.printStackTrace();
		} catch (GitAPIException e) {
			e.printStackTrace();
		}
		return commitDetails;
	}

	private void transferCommits(List<Commit> commits, List<CommitDetail> commitDetails, String project) {
		CommitDetail commitDetail = new CommitDetail();
		long[] addremove = { 0, 0 };
		for (Commit c : commits) {
			commitDetail = new CommitDetail();
			commitDetail.setId(c.getCommitId());
			commitDetail.setProject(project);
			commitDetail.setAuthor(c.getAuthor());
			commitDetail.setFiles(c.getFiles());
			commitDetail.setMessage(c.getMessage());
			commitDetail.setWhen(c.getWhen());
			commitDetail.setTimeZone(c.getTimeZone());
			addremove = c.countAddedAndRemovedLines();
			commitDetail.setAllAdded(addremove[0]);
			commitDetail.setAllRemoved(addremove[1]);
			Map<Type, List<FileDiff>> maps = c.countFilesByChangeType();
			commitDetail.setAddedFiles(maps.get(Type.ADD).size());
			commitDetail.setDeletedFiles(maps.get(Type.DELETE).size());
			commitDetail.setModifiedFiles(maps.get(Type.MODIFY).size());
			commitDetails.add(commitDetail);
		}
	}

}
