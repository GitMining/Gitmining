package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectCyclomaticComplexityDao;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.logic.MethodCC;
import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import cn.nju.iSE.GitMining.mongo.code.ProjectCCMongoRepository;
import cn.nju.iSE.GitMining.service.code.ProjectCyclomaticComplexity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.impl.ProjectCyclomaticComplexityImpl.java
 *  
 *  2018年3月19日	上午9:53:15  
*/
@Service
public class ProjectCyclomaticComplexityImpl implements ProjectCyclomaticComplexity{

	@Autowired
	private ProjectCCMongoRepository projectCCMongoRepository;

	@Autowired
	private ProjectCyclomaticComplexityDao projectCyclomaticComplexityDao;
	
	@Override
	public long create(String prjtPth, String groupName, long pId) {
		ProjectAnalyzer projectAnalyzer;
		try {
			if(projectCCMongoRepository.exists(pId))
				projectCCMongoRepository.delete(pId);
			if(prjtPth == null || prjtPth.trim().equals("") )
				return -1;
			projectAnalyzer = new ProjectAnalyzer(prjtPth);
			String name = projectAnalyzer.getPrjtName();
			long version = 0L;
			List<Map<String, Integer>> methods = projectAnalyzer.getMethodsCC();
			List<MethodCC> methodCCs = extract(methods);
			ProjectCC projectCC = new ProjectCC(pId, prjtPth, name, groupName, version, methodCCs);
			projectCCMongoRepository.save(projectCC);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectCC> getProjects(String name) {
		return projectCCMongoRepository.findByProjectName(name);
	}

	@Override
	public List<ProjectCC> getProjectsByPth(String pth) {
		return projectCCMongoRepository.findByProjectPath(pth);
	}

	@Override
	public ProjectCC getProject(long id) {
		return projectCCMongoRepository.findById(id);
	}

	@Override
	public ProjectCC getProject(String groupName, String projectName) {
		return projectCCMongoRepository.findByGroupNameAndProjectName(groupName,projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectCCMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer projectAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			projectAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectCC oldProjectCC = projectCCMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			if(oldProjectCC == null)
				return;
			long version = oldProjectCC.getVersion() + 1;
			long pId = oldProjectCC.getId();
			List<Map<String, Integer>> methods = projectAnalyzer.getMethodsCC();
			List<MethodCC> methodCCs = extract(methods);
			ProjectCC projectCC = new ProjectCC(pId, prjtPth, projectName, groupName, version, methodCCs);
			projectCyclomaticComplexityDao.update(projectCC);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectCC projectCC) {
		List<MethodCC> methods = projectCC.getMethodsCC();
		List<Object> awfuls = new ArrayList<>();
		for(MethodCC methodCC: methods)
			if(methodCC.getCcidx() > Constant.CodeQualityIndicator.CC_WARNING_AWFUL)
				awfuls.add(methodCC);
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectCC projectCC) {
		List<MethodCC> methods = projectCC.getMethodsCC();
		List<Object> warnings = new ArrayList<>();
		for(MethodCC methodCC: methods)
			if (methodCC.getCcidx() > Constant.CodeQualityIndicator.CC_INFO_WARNING
					&& methodCC.getCcidx() <= Constant.CodeQualityIndicator.CC_WARNING_AWFUL)
				warnings.add(methodCC);
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectCC projectCC) {
		List<MethodCC> methods = projectCC.getMethodsCC();
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<Object> awfuls = new ArrayList<>();
		List<Object> warnings = new ArrayList<>();
		for(MethodCC methodCC: methods){
			if(methodCC.getCcidx() <= Constant.CodeQualityIndicator.CC_WARNING_AWFUL
					&& methodCC.getCcidx() > Constant.CodeQualityIndicator.CC_INFO_WARNING)
				warnings.add(methodCC);
			else if(methodCC.getCcidx() > Constant.CodeQualityIndicator.CC_WARNING_AWFUL)
				awfuls.add(methodCC);
		}
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

	private List<MethodCC> extract(List<Map<String, Integer>> methods){
		List<MethodCC> methodCCs = new ArrayList<MethodCC>();
		for(Map<String, Integer> m: methods) {
			String rawpth = m.keySet().iterator().next();
			String pth = rawpth.replaceAll(".java", "");
			pth = pth.replaceAll("\\\\", ".");
			pth = pth.replaceAll("/", ".");
			if(pth.startsWith("."))
				pth = pth.substring(1);
			MethodCC mc = new MethodCC();
			mc.setPath(pth);
			mc.setCcidx(m.get(rawpth));
			methodCCs.add(mc);
		}
		return  methodCCs;
	}

}
