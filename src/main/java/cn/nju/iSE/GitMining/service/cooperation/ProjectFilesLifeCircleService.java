package cn.nju.iSE.GitMining.service.cooperation;

import java.util.List;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService.java
 *  
 *  2018年4月25日	下午7:10:28  
*/

public interface ProjectFilesLifeCircleService {

	ProjectFilesLifeCircle getProjectFilesLifeCircle(long id);

	ProjectFilesLifeCircle getProjectFilesLifeCircle(String projectName, String groupName);

	ProjectFilesLifeCircle extractAndSave(Project project, List<CommitDetail> commits);
}
