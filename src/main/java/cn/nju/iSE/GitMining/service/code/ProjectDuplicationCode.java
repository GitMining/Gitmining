package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import iSESniper.code.entity.cpd.CpdEntity;

import java.util.List;
import java.util.Map;

/**
 * @author VousAttendezrer
 *
 */
public interface ProjectDuplicationCode extends ProjectCodeCheck<ProjectDC>{
    Map<AnalysisRank, List<CpdEntity>> getRanks(ProjectDC projectDC);
}
