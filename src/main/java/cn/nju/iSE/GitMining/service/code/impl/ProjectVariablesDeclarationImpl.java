package cn.nju.iSE.GitMining.service.code.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.dao.code.ProjectVariablesDao;
import cn.nju.iSE.GitMining.web.constant.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsVabsDec;
import cn.nju.iSE.GitMining.model.code.vab.MethodVabsDec;
import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import cn.nju.iSE.GitMining.model.code.vab.VabDec;
import cn.nju.iSE.GitMining.mongo.code.ProjectVariablesDeclarationMongoRepository;
import cn.nju.iSE.GitMining.service.code.ProjectVariablesDeclaration;
import iSESniper.code.entity.vab.DeclarationVariable;
import iSESniper.code.entity.vab.MethodVabDeclarations;
import iSESniper.code.entity.vab.MethodVabsInFile;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.impl.ProjectVariablesDeclarationImpl.java
 *  
 *  2018年3月19日	下午3:37:00  
*/
@Service
public class ProjectVariablesDeclarationImpl implements ProjectVariablesDeclaration {
	
	@Autowired
	private ProjectVariablesDeclarationMongoRepository projectVariablesDeclarationMongoRepository;

	@Autowired
	private ProjectVariablesDao projectVariablesDao;

	@Override
	public long create(String prjtPth, String groupName, long pId) {
		try {
			if(projectVariablesDeclarationMongoRepository.exists(pId))
				projectVariablesDeclarationMongoRepository.delete(pId);
			if(prjtPth == null || prjtPth.trim().equals(""))
				return -1;
			ProjectAnalyzer projectAnalyzer = new ProjectAnalyzer(prjtPth);
			String name = projectAnalyzer.getPrjtName();
			long version = 0L;
			List<MethodVabsInFile> methods = projectAnalyzer.getMethodsVabs();
			List<FileMethodsVabsDec> fileMethodsVabsDecs = extract(methods);
			ProjectVabsDec projectParams = new ProjectVabsDec(pId, prjtPth, name, groupName, version, fileMethodsVabsDecs);
			projectVariablesDeclarationMongoRepository.save(projectParams);
			return version;
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<ProjectVabsDec> getProjects(String name) {
		return projectVariablesDeclarationMongoRepository.findByProjectName(name);
	}

	@Override
	public List<ProjectVabsDec> getProjectsByPth(String pth) {
		return projectVariablesDeclarationMongoRepository.findByProjectPath(pth);
	}

	@Override
	public ProjectVabsDec getProject(long id) {
		return projectVariablesDeclarationMongoRepository.findById(id);
	}

	@Override
	public ProjectVabsDec getProject(String groupName, String projectName) {
		return projectVariablesDeclarationMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
	}

	@Override
	public long countProjectsBypath(String pth) {
		return projectVariablesDeclarationMongoRepository.countByProjectPath(pth);
	}

	@Override
	public void update(String groupName, String projectName) {
		ProjectAnalyzer pAnalyzer;
		try {
			if(groupName == null || projectName == null || groupName.trim().equals("") || projectName.trim().equals("") )
				return;
			String prjtPth = Route.CLONE_PATH + groupName + "/" + projectName;
			pAnalyzer = new ProjectAnalyzer(prjtPth);
			ProjectVabsDec oldVabs = projectVariablesDeclarationMongoRepository.findByGroupNameAndProjectName(groupName, projectName);
			long version = oldVabs.getVersion() + 1;
			long pId = oldVabs.getId();
			List<MethodVabsInFile> methods = pAnalyzer.getMethodsVabs();
			List<FileMethodsVabsDec> fileMethodsVabsDecs = extract(methods);
			ProjectVabsDec projectParams = new ProjectVabsDec(pId, prjtPth, projectName, groupName, version, fileMethodsVabsDecs);
			projectVariablesDeclarationMongoRepository.save(projectParams);
		} catch (PathNotFoundException | FilesListIsEmptyException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Object> getAwfulList(ProjectVabsDec projectVabsDec) {
		List<FileMethodsVabsDec> fileMethodsParams = projectVabsDec.getFiles();
		List<Object> awfuls = new ArrayList<>();
		for(FileMethodsVabsDec methodsParams: fileMethodsParams) {
			List<MethodVabsDec> methodParamsList = methodsParams.getMethods();
			for(MethodVabsDec methodParams: methodParamsList){
				int len = methodParams.getVariables().size();
				if(len > Constant.CodeQualityIndicator.VD_WARNING_AWFUL)
					awfuls.add(methodParams);
			}
		}
		return awfuls;
	}

	@Override
	public List<Object> getWarningList(ProjectVabsDec projectVabsDec) {
		List<FileMethodsVabsDec> fileMethodsParams = projectVabsDec.getFiles();
		List<Object> warnings = new ArrayList<>();
		for(FileMethodsVabsDec methodsParams: fileMethodsParams) {
			List<MethodVabsDec> methodParamsList = methodsParams.getMethods();
			for(MethodVabsDec methodParams: methodParamsList){
				int len = methodParams.getVariables().size();
				if(len <= Constant.CodeQualityIndicator.VD_WARNING_AWFUL
						&& len > Constant.CodeQualityIndicator.VD_INFO_WARNING)
					warnings.add(methodParams);
			}
		}
		return warnings;
	}

	@Override
	public Map<String, List<Object>> getIssueLists(ProjectVabsDec projectVabsDec) {
		Map<String, List<Object>> issueLists = new HashMap<>();
		List<Object> awfuls = new ArrayList<>();
		List<Object> warnings = new ArrayList<>();
		List<FileMethodsVabsDec> fileMethodsParams = projectVabsDec.getFiles();
		for(FileMethodsVabsDec methodsParams: fileMethodsParams) {
			List<MethodVabsDec> methodParamsList = methodsParams.getMethods();
			for(MethodVabsDec methodParams: methodParamsList){
				int len = methodParams.getVariables().size();
				if(len <= Constant.CodeQualityIndicator.VD_WARNING_AWFUL
						&& len > Constant.CodeQualityIndicator.VD_INFO_WARNING)
					warnings.add(methodParams);
				else if(len > Constant.CodeQualityIndicator.VD_WARNING_AWFUL)
					awfuls.add(methodParams);
			}
		}
		issueLists.put(Constant.CodeQualityIndicator.WARNING, warnings);
		issueLists.put(Constant.CodeQualityIndicator.AWFUL, awfuls);
		return issueLists;
	}

	private List<FileMethodsVabsDec> extract(List<MethodVabsInFile> methods){
		List<FileMethodsVabsDec> fileMethodsVabsDecs = new ArrayList<FileMethodsVabsDec>();
		for(MethodVabsInFile m: methods) {
			String jpth = m.getPth();
			List<MethodVabsDec> methodVabsDecs = new ArrayList<MethodVabsDec>();
			for(MethodVabDeclarations mvs: m.getMvdList()) {
				List<VabDec> variables = new ArrayList<VabDec>();
				for(DeclarationVariable v: mvs.getDecs()) {
					VabDec variable = new VabDec();
					variable.setClazz(v.getClazz());
					variable.setvName(v.getvName());
					variable.setLocation(v.getLocation());
					variables.add(variable);
				}
				String mName = mvs.getMethodName();
				methodVabsDecs.add(new MethodVabsDec(mName, variables));
			}
			fileMethodsVabsDecs.add(new FileMethodsVabsDec(jpth, methodVabsDecs));
		}
		return fileMethodsVabsDecs;
	}
}
