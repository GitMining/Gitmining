package cn.nju.iSE.GitMining.service.code;

import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.code.ProjectLogicDepth.java
 *  
 *  2018年3月18日	下午1:03:58  
*/

public interface ProjectLogicDepth extends ProjectCodeCheck<ProjectLD> {
	
}
