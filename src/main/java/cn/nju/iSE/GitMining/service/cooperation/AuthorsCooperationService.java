package cn.nju.iSE.GitMining.service.cooperation;

import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import iSESniper.cooperation.entity.Author;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.cooperation.impl.AuthorsCooperationService.java
 *  
 *  2018年4月27日	上午10:59:18  
*/

public interface AuthorsCooperationService {

	/**
	 * Return the list of commits' details which belong to certain author. 
	 * @param author
	 * @return
	 */
	List<CommitDetail> getAuthorCommitsDetail(Author author);
	
	/**
	 * Count the importance index of the authors' contributions.
	 * importance[0] includes the commit importance index.
	 * importance[1] includes the lines importance index.(added_lines - removed_lines).
	 * @param commitDetails
	 * @return
	 */
	List<Map<Author, Double>> getAuthorsImportance(List<CommitDetail> commitDetails);
	
	/**
	 * Count the authors' commits number in project.
	 * @param project grouupname/projectname.
	 * @return
	 */
	public Map<Author, Integer> getAuthorPie(String project);
}
