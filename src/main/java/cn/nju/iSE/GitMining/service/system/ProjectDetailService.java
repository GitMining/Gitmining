package cn.nju.iSE.GitMining.service.system;

import java.util.List;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.ProjectDetailService.java
 *  
 *  2018年4月23日	下午3:34:37  
*/

public interface ProjectDetailService {
	
	public ProjectDetail projectCommitsExtract(Project project);
	
	public ProjectDetail getProjectDetail(long id);
	
	public ProjectDetail getProjectDetail(String namespace, String name);

	public List<ProjectDetail> getprojectDetails(String namespace);
	
	public void save(List<ProjectDetail> projectDetails);
	
	public void save(ProjectDetail projectDetail);

	/**
	 * Update projectDetail by searching for nameSpace and projectName
	 * @param projectDetail
	 */
	public void update(ProjectDetail projectDetail);
}
