package cn.nju.iSE.GitMining.service.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.mongo.cooperation.CommitDetailMongoRepository;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import iSESniper.cooperation.entity.Author;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.impl.CommitDetailServiceImpl.java
 *  
 *  2018年4月23日	下午4:27:02  
*/
@Service
public class CommitDetailServiceImpl implements CommitDetailService {
	
	@Autowired
	private CommitDetailMongoRepository commitDetailMongoRepository;
	
	@Override
	public CommitDetail getCommit(String id) {
		return commitDetailMongoRepository.findById(id);
	}

	@Override
	public List<CommitDetail> getCommits(String project) {
		return commitDetailMongoRepository.findAllByProject(project);
	}

	@Override
	public void save(CommitDetail commitDetail) {
		commitDetailMongoRepository.save(commitDetail);
	}

	@Override
	public void save(List<CommitDetail> commitDetails) {
		commitDetailMongoRepository.save(commitDetails);
	}

	@Override
	public List<CommitDetail> getCommits(Author author) {
		return commitDetailMongoRepository.findByAuthor(author);
	}

	@Override
	public void append(List<CommitDetail> commitDetails) {
		for(CommitDetail commitDetail: commitDetails) {
			if(commitDetailMongoRepository.exists(commitDetail.getId()))
				continue;
			commitDetailMongoRepository.save(commitDetail);
		}
	}

}
