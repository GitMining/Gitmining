package cn.nju.iSE.GitMining.service.auth;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysRole;

import java.util.List;

public interface SysRoleService {
    SysRole getRole(int id);

    /**
     * Add new role.
     * @param jsonObject JsonObject at least includes "roleName", "permissionIds"
     */
    public void addRole(JSONObject jsonObject);

    public int appendPermission(int rId, int[] pIds);

    public int appendPermission(String rName, int[] pIds);

    public int removePermission(int rId, int[] pIds);

    public int removePermission(String rName, int[] pIds);
}
