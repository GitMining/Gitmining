package cn.nju.iSE.GitMining.service.system;

import cn.nju.iSE.GitMining.model.system.Group;

import java.util.List;

/**
 * @deprecated
 * @author Magister
 *
 */
public interface GroupDownload {

	/**
	 * This method get groups' basic information which is not added into database
	 * without informations of members and projects' id.
	 * @param url list group url, like this "http://114.215.188.21/api/v3/groups/?private_token=:token&page=:page&per_page=:perpage".
	 * @param charset 
	 * @return return the list of groups with basic information.
	 */
    public List<Group> downloadGroups(String url, String charset);

    public void saveGroups(List<Group> groups);

    public void saveGroup(Group group);
}
