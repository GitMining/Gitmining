package cn.nju.iSE.GitMining.service.system;

import java.util.List;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import iSESniper.cooperation.entity.Author;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.service.system.CommitDetailService.java
 *  
 *  2018年4月23日	下午4:06:12  
*/

public interface CommitDetailService {

	/**
	 * Get certain commit's detail.
	 * @param id
	 * @return
	 */
	public CommitDetail getCommit(String id);
	
	/**
	 * Get the list of commits' detail by using project name.
	 * @param project project's name with namespace like this, group_1/project01.
	 * @return
	 */
	public List<CommitDetail> getCommits(String project);
	
	public List<CommitDetail> getCommits(Author author);
	
	public void save(CommitDetail commitDetail);
	
	public void save(List<CommitDetail> commitDetails);

	public void append(List<CommitDetail> commitDetails);
	
}
