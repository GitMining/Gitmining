package cn.nju.iSE.GitMining.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.common.util.Downloader.java
 *  
 *  2018年4月21日	下午9:30:01  
*/
@Service
public class Downloader {

	public static String getContent(String page, String textType) {
		URL url = null;
		String content = "";
		try {
			url = new URL(page);
		} catch (MalformedURLException  e) {
			e.printStackTrace();
		}
		if(textType.length() == 0)
			textType = "utf-8";
		int sec = 1000;
		try {
			HttpURLConnection urlConnection =(HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setReadTimeout(10 * sec);
			urlConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
			InputStream text = urlConnection.getInputStream();
			content = inputStream2String(text, textType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
	public static JSONArray getJsonArray(String page, String textType) {
		String content = getContent(page, textType);
		return JSONObject.parseArray(content);
	}
	
	public static JSONObject getJsonContent(String page, String textType) {
		String content = getContent(page, textType);
		return JSONObject.parseObject(content);
	}
	
	private static String inputStream2String(InputStream text, String charset) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(text, charset));
		StringBuffer sBuffer = new StringBuffer();
		String content = "";
		while((content = reader.readLine()) != null) {
			sBuffer.append(content);
		}
		return sBuffer.toString();
	}
}
