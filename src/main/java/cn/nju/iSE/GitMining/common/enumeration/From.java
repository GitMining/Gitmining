package cn.nju.iSE.GitMining.common.enumeration;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.common.enumeration.From.java
 *  
 *  2018年3月29日	上午10:03:50  
*/

public enum From {
	GitLab, GitHub, GitLab_SECIII
}
