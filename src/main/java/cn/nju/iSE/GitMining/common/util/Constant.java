package cn.nju.iSE.GitMining.common.util;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.common.util.Constant.java
 * 
 *           2018年4月24日 上午10:22:58
 */

public class Constant {

	public static final String token = "5oR7mkTB5o9izwB3dwxL";

	public static final String[] FileTypes = { ".jsp", ".html", ".java", ".js", ".vue", ".tsx", ".ts", ".sh",
			".css", ".txt", ".md", ".json" };

	public static final String SUCCESS_CODE = "100";
	public static final String SUCCESS_MSG = "请求成功";

	public static final String MONGO_SUCCESS_CODE = "4431";
	public static final String MONGO_SUCCESS_MSG = "MongoDB Operate Successfully";

	public static final String MONGO_FAILURE_CODE = "8766";
	public static final String MONGO_FAILURE_MSG = "MongoDB Operate Failed";

	public class CodeQualityIndicator{
		public static final String LOC = "LinesOfCode";
		public static final int LOC_INFO_WARNING = 50;
		public static final int LOC_WARNING_AWFUL = 100;
		public static final String LOA = "LinesOfAnnotation";
		public static final double LOA_INFO_WARNING = 0.03;
		public static final double LOA_WARNING_AWFUL = 0.0;
		public static final String LD = "LogicDepth";
		public static final int LD_INFO_WARNING = 2;
		public static final int LD_WARNING_AWFUL = 4;
		public static final String CC = "CyclomaticComplexity";
		public static final int CC_INFO_WARNING = 2;
		public static final int CC_WARNING_AWFUL = 4;
		public static final String VD = "NumberOfVariables";
		public static final int VD_INFO_WARNING = 10;
		public static final int VD_WARNING_AWFUL = 15;
		public static final String PD = "NumberOfParameters";
		public static final int PD_INFO_WARNING = 6;
		public static final int PD_WARNING_AWFUL = 8;
		public static final String EM = "EmptyMethod";
		public static final int EM_WARNING_AWFUL = 5;
		public static final String DC = "CodeDuplication";
		public static final int DC_WARNING_AWFUL = 20;

		public static final String WARNING = "WARNING";
		public static final String AWFUL = "AWFUL";

		public static final int THRESHOLD = 5;
	}

	/**
	 * session中存放用户信息的key值
	 */
	public static final String SESSION_USER_INFO = "userInfo";
	public static final String SESSION_USER_PERMISSION = "userPermission";

	public class AuthProperties{
		public class UserProperties{
			public static final String salt = "salt";

			public static final String isAdmin = "isAdmin";
		}
	}
}
