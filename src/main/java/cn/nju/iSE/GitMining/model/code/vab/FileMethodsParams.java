package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.FileMethodsParams.java
 *  
 *  2018年3月19日	上午11:44:18  
*/

public class FileMethodsParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3122629977222235106L;

	private String path;
	
	private List<MethodParams> methods;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<MethodParams> getMethods() {
		return methods;
	}

	public void setMethods(List<MethodParams> methods) {
		this.methods = methods;
	}

	public FileMethodsParams(String path, List<MethodParams> methods) {
		super();
		this.path = path;
		this.methods = methods;
	}

	public FileMethodsParams() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Files Profile] file: ");
		sBuffer.append(path);
		if(methods == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods:\n");
		for(MethodParams mps: methods) {
			sBuffer.append(mps.toString());
			sBuffer.append("\n");
		}
		
		return sBuffer.toString();
	}
	
}
