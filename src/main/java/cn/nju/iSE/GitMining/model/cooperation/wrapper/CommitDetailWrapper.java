package cn.nju.iSE.GitMining.model.cooperation.wrapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import iSESniper.cooperation.entity.Commit;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.cooperation.wrapper.CommitDetailWrapper.java
 *  
 *  2018年4月25日	下午7:56:57  
*/
@Service
public class CommitDetailWrapper {

	public Commit unwrap(CommitDetail commitDetail) {
		Commit commit = new Commit();
		commit.setAuthor(commitDetail.getAuthor());
		commit.setCommitId(commitDetail.getId());
		commit.setMessage(commitDetail.getMessage());
		commit.setTimeZone(commitDetail.getTimeZone());
		commit.setWhen(commitDetail.getWhen());
		commit.setFiles(commitDetail.getFiles());
		return commit;
	}
	
	public List<Commit> unwrap(List<CommitDetail> commitDetails){
		return commitDetails.stream().map(this::unwrap).collect(Collectors.toList());
	}
}
