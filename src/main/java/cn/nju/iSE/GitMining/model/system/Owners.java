package cn.nju.iSE.GitMining.model.system;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import cn.nju.iSE.GitMining.common.enumeration.From;


/**
 * Owner is the most common operation unit that analyst use in System, it can be a group or a single developer.
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.Groups.java
 *  
 *  2018年4月20日	上午10:42:13  
*/

public abstract class Owners {

	@Id
	private long id;
	
	private long ownerId;
	
	private String ownerName;
	// Projects' id including controlled and uncontrolled.
	private List<Long> projectIds = new ArrayList<Long>();
	
	private From platform;
	//The last analyze time.
	private long time;

	private String ownerURL;

	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	public From getPlatform() {
		return platform;
	}

	public void setPlatform(From platform) {
		this.platform = platform;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public List<Long> getProjectIds() {
		return projectIds;
	}

	public void setProjectIds(List<Long> projectIds) {
		this.projectIds = projectIds;
	}

	public String getOwnerURL() {
		return ownerURL;
	}

	public void setOwnerURL(String ownerURL) {
		this.ownerURL = ownerURL;
	}

	public Owners(long id, long ownerId, String ownerName, List<Long> projectIds, From platform, long time, String ownerURL) {
		super();
		this.id = id;
		this.ownerId = ownerId;
		this.ownerName = ownerName;
		this.projectIds = projectIds;
		this.platform = platform;
		this.time = time;
		this.ownerURL = ownerURL;
	}

	public Owners(long id, long ownerId, String ownerName, List<Long> projectIds, From platform, long time) {
		super();
		this.id = id;
		this.ownerId = ownerId;
		this.ownerName = ownerName;
		this.projectIds = projectIds;
		this.platform = platform;
		this.time = time;
	}

	public Owners() {
		super();
	}
	
}
