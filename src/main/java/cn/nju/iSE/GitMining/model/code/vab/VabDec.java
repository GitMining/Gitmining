package cn.nju.iSE.GitMining.model.code.vab;

import iSESniper.code.util.entity.Location;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.VabDec.java
 *  
 *  2018年3月19日	下午3:49:36  
*/

public class VabDec extends Variable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4911605100051703150L;
	private Location location; 

	public VabDec() {
		
	}
	
	public VabDec(String clazz, String vName) {
		super(clazz, vName);
	}
	
	public VabDec(String clazz, String vName, Location location) {
		super(clazz, vName);
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return  super.toString() + " location:" + location.toString();
	}
}
