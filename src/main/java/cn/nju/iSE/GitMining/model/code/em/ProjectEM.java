package cn.nju.iSE.GitMining.model.code.em;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import iSESniper.code.entity.em.EmptyMethodEntity;

/**
 * @author VousAttendezrer
 *
 */
public class ProjectEM extends ProjectInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3365295899313428772L;
	private List<EmptyMethodEntity> emptyMethodEntities;

	public ProjectEM() {
		super();
	}

	public ProjectEM(long id, String projectPath, String projectName, String groupName, long version,
			List<EmptyMethodEntity> emptyMethodEntities) {
		super(id, projectPath, projectName, groupName, version);
		this.emptyMethodEntities = emptyMethodEntities;
	}

	public ProjectEM(long id, String projectPath, String projectName, long version,
			List<EmptyMethodEntity> emptyMethodEntities) {
		super(id, projectPath, projectName, version);
		this.emptyMethodEntities = emptyMethodEntities;
	}

	public List<EmptyMethodEntity> getEmptyMethodEntities() {
		return emptyMethodEntities;
	}

	public void setEmptyMethodEntities(List<EmptyMethodEntity> emptyMethodEntities) {
		this.emptyMethodEntities = emptyMethodEntities;
	}
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Empty Method] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if (emptyMethodEntities == null) {
			sBuffer.append("EmptyMethodEntities List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", EmptyMethodEntities List: \n");
		for(EmptyMethodEntity em: emptyMethodEntities) {
			sBuffer.append("FileName:");
			sBuffer.append(em.getFileName());
			sBuffer.append(", methodName:");
			sBuffer.append(em.getMethodName());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
}
