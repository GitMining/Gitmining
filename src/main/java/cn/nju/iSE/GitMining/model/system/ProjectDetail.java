package cn.nju.iSE.GitMining.model.system;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * ProjectDetail class contains more detail information about certain project.
 * It is different with Project class.
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.system.ProjectDetail.java
 *  
 *  2018年4月23日	上午10:01:20  
*/
@Document
public class ProjectDetail implements Serializable {

	private static final long serialVersionUID = -3974290517599662014L;

	@Id
	private long id;
	
	private String name;
	
	private String namespace;
	
	private String url;
	
	private int commitCount;
	
	private long createTime;
	
	private long addedLines;
	
	private long removedLines;
	
	private long addedFiles;
	
	private long deletedFiles;
	
	private long modifiedFiles;

	public int getCommitCount() {
		return commitCount;
	}

	public void setCommitCount(int commitCount) {
		this.commitCount = commitCount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getAddedLines() {
		return addedLines;
	}

	public void setAddedLines(long addedLines) {
		this.addedLines = addedLines;
	}

	public long getRemovedLines() {
		return removedLines;
	}

	public void setRemovedLines(long removedLines) {
		this.removedLines = removedLines;
	}

	public long getAddedFiles() {
		return addedFiles;
	}

	public void setAddedFiles(long addedFiles) {
		this.addedFiles = addedFiles;
	}

	public long getDeletedFiles() {
		return deletedFiles;
	}

	public void setDeletedFiles(long deletedFiles) {
		this.deletedFiles = deletedFiles;
	}

	public long getModifiedFiles() {
		return modifiedFiles;
	}

	public void setModifiedFiles(long modifiedFiles) {
		this.modifiedFiles = modifiedFiles;
	}

	public ProjectDetail(long id, String name, String namespace, String url, int commitCount, long createTime,
			long addedLines, long removedLines, long addedFiles, long deletedFiles, long modifiedFiles) {
		super();
		this.id = id;
		this.name = name;
		this.namespace = namespace;
		this.url = url;
		this.commitCount = commitCount;
		this.createTime = createTime;
		this.addedLines = addedLines;
		this.removedLines = removedLines;
		this.addedFiles = addedFiles;
		this.deletedFiles = deletedFiles;
		this.modifiedFiles = modifiedFiles;
	}

	public ProjectDetail() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectDetail [id=" + id + ", name=" + name + ", namespace=" + namespace + ", url=" + url
				+ ", commitCount=" + commitCount + ", createTime=" + createTime + ", addedLines=" + addedLines
				+ ", removedLines=" + removedLines + ", addedFiles=" + addedFiles + ", deletedFiles=" + deletedFiles
				+ ", modifiedFiles=" + modifiedFiles + "]";
	}

}
