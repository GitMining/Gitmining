package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.FileMethodsVabsDec.java
 *  
 *  2018年3月19日	下午3:27:07  
*/

public class FileMethodsVabsDec implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4741180235250874708L;

	private String path;
	
	private List<MethodVabsDec> methods;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<MethodVabsDec> getMethods() {
		return methods;
	}

	public void setMethods(List<MethodVabsDec> methods) {
		this.methods = methods;
	}

	public FileMethodsVabsDec(String path, List<MethodVabsDec> methods) {
		super();
		this.path = path;
		this.methods = methods;
	}

	public FileMethodsVabsDec() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Files Profile] file: ");
		sBuffer.append(path);
		if(methods == null) {
			sBuffer.append(" methods list is empty.");
		}
		sBuffer.append(", Methods:\n");
		for(MethodVabsDec mvs: methods) {
			sBuffer.append(mvs.toString());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
}
