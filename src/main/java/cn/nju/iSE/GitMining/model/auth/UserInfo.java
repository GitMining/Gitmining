package cn.nju.iSE.GitMining.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "gitmining_user")
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -8981743363544201142L;
    @Id
    private String id;

    private String username;

    @JsonIgnore
    private String password;
    @JsonIgnore
    private String salt;

    private byte state;
    /**
     * TODO Add platforms' username and passwor of users, like List<pltfmUserPwd>, which contains
     * TODO platform url and specified username and password.
     */
    @JsonIgnore
    private int roleId;// 0 is admin, 1 is ordinary user.

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public UserInfo() {
    }

    public UserInfo(String id, String username, String password, String salt, byte state, int roleId) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.state = state;
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", state=" + state +
                ", roleId=" + roleId +
                '}';
    }
}
