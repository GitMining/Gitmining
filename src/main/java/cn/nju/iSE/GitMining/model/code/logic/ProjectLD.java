package cn.nju.iSE.GitMining.model.code.logic;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.logic.ProjectLD.java
 *  
 *  2018年3月18日	下午12:47:05  
*/
@Document
public class ProjectLD extends ProjectInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3571813180929515837L;
	private List<MethodLD> methodLDs;
	public List<MethodLD> getMethodLDs() {
		return methodLDs;
	}
	public void setMethodLDs(List<MethodLD> methodLDs) {
		this.methodLDs = methodLDs;
	}
	public ProjectLD() {
		super();
	}
	
	public ProjectLD(long id, String projectPath, String projectName, String groupName, long version,
			List<MethodLD> methodLDs) {
		super(id, projectPath, projectName, groupName, version);
		this.methodLDs = methodLDs;
	}
	public ProjectLD(long id, String projectPath, String projectName, long version, List<MethodLD> methodLDs) {
		super(id, projectPath, projectName, version);
		this.methodLDs = methodLDs;
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Logic Depth] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if(methodLDs == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods List: \n");
		for(MethodLD mld: methodLDs) {
			sBuffer.append(mld.getPath());
			sBuffer.append("  Cyclomatic Complexity: ");
			sBuffer.append(mld.getLdIdx());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
	
}
