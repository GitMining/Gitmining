package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.MethodVabsDec.java
 *  
 *  2018年3月19日	下午3:27:31  
*/

public class MethodVabsDec implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8919069479521491584L;

	private String methodName;
	
	private List<VabDec> variables;

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<VabDec> getVariables() {
		return variables;
	}

	public void setVariables(List<VabDec> variables) {
		this.variables = variables;
	}

	public MethodVabsDec(String methodName, List<VabDec> variables) {
		super();
		this.methodName = methodName;
		this.variables = variables;
	}

	public MethodVabsDec() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[Method] Method Name: ");
		stringBuffer.append(methodName);
		if(variables == null) {
			stringBuffer.append(" Variables list is empty.");
			return stringBuffer.toString();
		}
		stringBuffer.append(", [Variables]: [");
		for(Variable v: variables) {
			stringBuffer.append(v.toString());
			stringBuffer.append("; ");
		}
		stringBuffer.append("]");
		return stringBuffer.toString();
	}	
}
