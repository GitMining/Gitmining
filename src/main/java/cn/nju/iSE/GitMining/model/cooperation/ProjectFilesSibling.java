package cn.nju.iSE.GitMining.model.cooperation;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import iSESniper.cooperation.entity.Author;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.model.cooperation.ProjectFilesSibling.java
 * 
 *           2018年4月25日 下午9:00:30
 */

public class ProjectFilesSibling implements Serializable {

	private static final long serialVersionUID = -7358529291107945375L;

	private long id;

	private String projectName;

	private String groupName;

	private Map<Author, List<String>> filesSibling;

	// The relationship between author and contributor.
	// Contributor 1 contributes some times commit to author A's files.
	private Map<Author, Map<Author, Integer>> contributeRel;
	// Contributor 1 contributes x files that added into repository by Author A.
	private Map<Author, Map<Author, List<String>>> contriFilesRel;

	public Map<Author, Map<Author, List<String>>> getContriFilesRel() {
		return contriFilesRel;
	}

	public void setContriFilesRel(Map<Author, Map<Author, List<String>>> contriFilesRel) {
		this.contriFilesRel = contriFilesRel;
	}

	public Map<Author, Map<Author, Integer>> getContributeRel() {
		return contributeRel;
	}

	public void setContributeRel(Map<Author, Map<Author, Integer>> contributeRel) {
		this.contributeRel = contributeRel;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Map<Author, List<String>> getFilesSibling() {
		return filesSibling;
	}

	public void setFilesSibling(Map<Author, List<String>> filesSibling) {
		this.filesSibling = filesSibling;
	}

	public ProjectFilesSibling(long id, String projectName, String groupName, Map<Author, List<String>> filesSibling,
			Map<Author, Map<Author, Integer>> contributeRel, Map<Author, Map<Author, List<String>>> contriFilesRel) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.groupName = groupName;
		this.filesSibling = filesSibling;
		this.contributeRel = contributeRel;
		this.contriFilesRel = contriFilesRel;
	}

	public ProjectFilesSibling() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectFilesSibling [id=" + id + ", projectName=" + projectName + ", groupName=" + groupName
				+ ", filesSibling=" + filesSibling + "]";
	}
}
