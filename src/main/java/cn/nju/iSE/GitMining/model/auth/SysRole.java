package cn.nju.iSE.GitMining.model.auth;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document(collection = "gitmining_role")
public class SysRole {

    private long id;

    private String roleName;

    private String description;

    private List<Integer> permissionIds;

    public SysRole() {
    }

    public SysRole(long id, String roleName, String description, List<Integer> permissionIds) {
        this.id = id;
        this.roleName = roleName;
        this.description = description;
        this.permissionIds = permissionIds;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Integer> permissionIds) {
        this.permissionIds = permissionIds;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", description='" + description + '\'' +
                ", permissionIds=" + permissionIds +
                '}';
    }
}
