package cn.nju.iSE.GitMining.model.system;

import java.io.Serializable;
import java.util.List;

public class IndicatoreScore implements Serializable {
    private static final long serialVersionUID = -1323089022746860643L;

    private String indicator;

    private boolean score;

    private double threshold;

    private List<Object> warningList;

    private List<Object> awfulList;

    public IndicatoreScore() {
    }

    public IndicatoreScore(String indicator, boolean score, double threshold, List<Object> warningList, List<Object> awfulList) {
        this.indicator = indicator;
        this.threshold = threshold;
        this.warningList = warningList;
        this.awfulList = awfulList;
        this.score = check();
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public boolean isScore() {
        return score;
    }

    public void setScore(boolean score) {
        this.score = score;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public List<Object> getWarningList() {
        return warningList;
    }

    public void setWarningList(List<Object> warningList) {
        this.warningList = warningList;
    }

    public List<Object> getAwfulList() {
        return awfulList;
    }

    public void setAwfulList(List<Object> awfulList) {
        this.awfulList = awfulList;
    }

    private boolean check() {
        return awfulList == null || awfulList.size() <= threshold;
    }

    @Override
    public String toString() {
        return "IndicatoreScore{" +
                "indicator='" + indicator + '\'' +
                ", score=" + score +
                ", threshold=" + threshold +
                ", warningList=" + warningList +
                ", awfulList=" + awfulList +
                '}';
    }
}
