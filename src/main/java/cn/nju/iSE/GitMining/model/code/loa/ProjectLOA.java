package cn.nju.iSE.GitMining.model.code.loa;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;;

/**
 * @author VousAttendezrer
 *
 */
public class ProjectLOA extends ProjectInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8263122110403407265L;
	private List<LineOfAnnotateInSingleFileEntity> lineOfAnnotateEntities;

	public ProjectLOA() {
		super();
	}

	public ProjectLOA(long id, String projectPath, String projectName, String groupName, long version,
			List<LineOfAnnotateInSingleFileEntity> lineOfAnnotateEntities) {
		super(id, projectPath, projectName, groupName, version);
		this.lineOfAnnotateEntities = lineOfAnnotateEntities;
	}

	public ProjectLOA(long id, String projectPath, String projectName, long version,
			List<LineOfAnnotateInSingleFileEntity> lineOfAnnotateEntities) {
		super(id, projectPath, projectName, version);
		this.lineOfAnnotateEntities = lineOfAnnotateEntities;
	}

	public List<LineOfAnnotateInSingleFileEntity> getLineOfAnnotateEntities() {
		return lineOfAnnotateEntities;
	}

	public void setLineOfAnnotateEntities(List<LineOfAnnotateInSingleFileEntity> lineOfAnnotateEntities) {
		this.lineOfAnnotateEntities = lineOfAnnotateEntities;
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Line of Annoation] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if (lineOfAnnotateEntities == null) {
			sBuffer.append("LineOfAnnotateEntities List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", LineOfAnnotateEntities List: \n");
		for(LineOfAnnotateInSingleFileEntity loa: lineOfAnnotateEntities) {
			sBuffer.append("FileName:");
			sBuffer.append(loa.getFileName());
			sBuffer.append(", TotalCount:");
			sBuffer.append(loa.getTotalCount());
			sBuffer.append(", Annotate Count:");
			sBuffer.append(loa.getAnnotateCount());
			sBuffer.append("; Blank Count:");
			sBuffer.append(loa.getBlankCount());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}

}
