package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.MethodParams.java
 *  
 *  2018年3月19日	上午11:07:10  
*/

public class MethodParams implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6578938908800128326L;

	private String methodName;
	
	private List<Variable> params;

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<Variable> getParams() {
		return params;
	}

	public void setParams(List<Variable> params) {
		this.params = params;
	}

	public MethodParams(String methodName, List<Variable> params) {
		super();
		this.methodName = methodName;
		this.params = params;
	}

	public MethodParams() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[Method] Method Name: ");
		stringBuffer.append(methodName);
		if(params == null) {
			stringBuffer.append(" Parameters list is empty.");
			return stringBuffer.toString();
		}
		stringBuffer.append(", [Parameter]: [");
		for(Variable v: params) {
			stringBuffer.append(v.toString());
			stringBuffer.append("; ");
		}
		stringBuffer.append("]");
		return stringBuffer.toString();
	}
}
