package cn.nju.iSE.GitMining.model.system;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.common.enumeration.From;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.model.Members.java
 * 
 *           2018年4月20日 上午10:53:35
 */
@Document
public class Developer extends Owners implements Serializable {

	private static final long serialVersionUID = 9122521463066680059L;

	private String name;

	private List<String> commitIds;

	public List<String> getCommitIds() {
		return commitIds;
	}

	public void setCommitIds(List<String> commitIds) {
		this.commitIds = commitIds;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Developer(long id, long ownerId, String ownerName, List<Long> projectIds, From platform, long time,
			String name, List<String> commitIds) {
		super(id, ownerId, ownerName, projectIds, platform, time);
		this.name = name;
		this.commitIds = commitIds;
	}

	public Developer() {
		super();
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Developer] name: ");
		sBuffer.append(name);
		sBuffer.append(", id: ");
		sBuffer.append(getId());
		sBuffer.append(", user name: ");
		sBuffer.append(getOwnerName());
		sBuffer.append(", Platform: ");
		sBuffer.append(getPlatform());
		sBuffer.append(", Projects: \n");
		sBuffer.append(getProjectIds());
		return sBuffer.toString();
	}
}
