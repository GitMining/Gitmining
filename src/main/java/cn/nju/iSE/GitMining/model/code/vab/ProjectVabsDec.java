package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.ProjectVabDec.java
 *  
 *  2018年3月19日	下午3:26:39  
*/
@Document
public class ProjectVabsDec extends ProjectInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7878676131000462567L;

	private List<FileMethodsVabsDec> files;
	
	public List<FileMethodsVabsDec> getFiles() {
		return files;
	}


	public void setFiles(List<FileMethodsVabsDec> files) {
		this.files = files;
	}

	public ProjectVabsDec(long id, String projectPath, String projectName, String groupName, long version,
			List<FileMethodsVabsDec> files) {
		super(id, projectPath, projectName, groupName, version);
		this.files = files;
	}


	public ProjectVabsDec(long id, String projectPath, String projectName, long version, List<FileMethodsVabsDec> files) {
		super(id, projectPath, projectName, version);
		this.files = files;
	}

	public ProjectVabsDec() {
		
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectParameters] ");
		sBuffer.append(super.toString());
		if(files == null) {
			sBuffer.append(" Files list is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(" Files: \n");
		for(FileMethodsVabsDec fmv: files) {
			sBuffer.append(fmv.toString());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
	
}
