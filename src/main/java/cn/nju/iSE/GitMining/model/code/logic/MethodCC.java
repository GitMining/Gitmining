package cn.nju.iSE.GitMining.model.code.logic;

import java.io.Serializable;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.logic.MethodCC.java
 *  
 *  2018年3月18日	上午10:40:28  
*/

public class MethodCC implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6320659088014893336L;
	private String path;
	private int ccidx;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getCcidx() {
		return ccidx;
	}
	public void setCcidx(int ccidx) {
		this.ccidx = ccidx;
	}
	
}
