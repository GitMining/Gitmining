package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.vab.ProjectParams.java
 *  
 *  2018年3月19日	上午10:57:03  
*/
@Document
public class ProjectParams extends ProjectInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4075070125815354052L;

	private List<FileMethodsParams> files;
	
	public List<FileMethodsParams> getFiles() {
		return files;
	}

	public ProjectParams(long id, String projectPath, String projectName, String groupName, long version,
			List<FileMethodsParams> files) {
		super(id, projectPath, projectName, groupName, version);
		this.files = files;
	}


	public void setFiles(List<FileMethodsParams> files) {
		this.files = files;
	}

	public ProjectParams(long id, String projectPath, String projectName, long version, List<FileMethodsParams> files) {
		super(id, projectPath, projectName, version);
		this.files = files;
	}

	public ProjectParams() {
		
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectParameters] ");
		sBuffer.append(super.toString());
		if(files == null) {
			sBuffer.append(" Files list is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(" Files: \n");
		for(FileMethodsParams fmp: files) {
			sBuffer.append(fmp.toString());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
	
}
