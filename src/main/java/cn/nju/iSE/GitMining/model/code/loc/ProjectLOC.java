package cn.nju.iSE.GitMining.model.code.loc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import iSESniper.code.entity.loc.FileLinesOfCodeEntity;
import iSESniper.code.entity.loc.JavaLinesOfCodeEntity;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.loc.ProjectLOCEntity.java
 *  
 *  2018年3月17日	上午10:06:12  
*/
@Document
public class ProjectLOC extends ProjectInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1109976379142306052L;
	private List<NonJavaLOCEntity> nonJavaLOCEntities;
	private List<JavaLOCEntity> javaLOCEntities;
	
	public ProjectLOC() {
	}
	
	@Deprecated
	public ProjectLOC(long id, String projectPath) throws PathNotFoundException, FilesListIsEmptyException {
		super(id, projectPath);
		ProjectAnalyzer pAnalyzer = new ProjectAnalyzer(projectPath);
		pAnalyzer = new ProjectAnalyzer(projectPath);
		setProjectName(pAnalyzer.getPrjtName());
		nonJavaLOCEntities = new ArrayList<NonJavaLOCEntity>();
		List<FileLinesOfCodeEntity> nonjavaLinesOfCodeEntities = pAnalyzer.getNJLOC();
		for(FileLinesOfCodeEntity fe: nonjavaLinesOfCodeEntities) {
			nonJavaLOCEntities.add(new NonJavaLOCEntity(fe));
		}
		javaLOCEntities = new ArrayList<JavaLOCEntity>();
		List<JavaLinesOfCodeEntity> javaLinesOfCodeEntities = pAnalyzer.getJLOC();
		for(JavaLinesOfCodeEntity je: javaLinesOfCodeEntities) {
			javaLOCEntities.add(new JavaLOCEntity(je));
		}
	}
	
	public ProjectLOC(long id, String projectPath, String projectName, long version,
			List<NonJavaLOCEntity> nonJavaLOCEntities, List<JavaLOCEntity> javaLOCEntities) {
		super(id, projectPath, projectName, version);
		this.nonJavaLOCEntities = nonJavaLOCEntities;
		this.javaLOCEntities = javaLOCEntities;
	}

	public ProjectLOC(long id, String projectPath, String projectName, String groupName, long version,
			List<NonJavaLOCEntity> nonJavaLOCEntities, List<JavaLOCEntity> javaLOCEntities) {
		super(id, projectPath, projectName, groupName, version);
		this.nonJavaLOCEntities = nonJavaLOCEntities;
		this.javaLOCEntities = javaLOCEntities;
	}

	public List<NonJavaLOCEntity> getNonJavaLOCEntities() {
		return nonJavaLOCEntities;
	}
	public List<JavaLOCEntity> getJavaLOCEntities() {
		return javaLOCEntities;
	}
	public void setNonJavaLOCEntities(List<NonJavaLOCEntity> nonJavaLOCEntities) {
		this.nonJavaLOCEntities = nonJavaLOCEntities;
	}

	public void setJavaLOCEntities(List<JavaLOCEntity> javaLOCEntities) {
		this.javaLOCEntities = javaLOCEntities;
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectLOC] [id: "); 
		sBuffer.append(getId()); 
		sBuffer.append(", ProjectName: "); 
		sBuffer.append(getProjectName());
		sBuffer.append("VERSION: ");
		sBuffer.append(getVersion());
		sBuffer.append(", NonJavaLOCEntities: \n");
		for(NonJavaLOCEntity ne: nonJavaLOCEntities) {
			sBuffer.append(ne.toString());
		}
		sBuffer.append("\nJavaLOCEntities: \n");
		for(JavaLOCEntity je: javaLOCEntities) {
			sBuffer.append(je.toString());
		}
		sBuffer.append("]\n");
		return sBuffer.toString();
	}
}
