package cn.nju.iSE.GitMining.model.cooperation;

import java.io.Serializable;
import java.util.List;

import iSESniper.cooperation.entity.FileLifeCircle;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.cooperation.FileLifeCircle.java
 *  
 *  2018年4月25日	下午7:02:42  
*/

public class ProjectFilesLifeCircle implements Serializable{

	private static final long serialVersionUID = -6969023812839494815L;

	/**
	 * This is project ID.
	 */
	private long id;
	
	private String projectName;
	
	private String groupName;
	
	private List<FileLifeCircle> fileLifeCircles;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<FileLifeCircle> getFileLifeCircles() {
		return fileLifeCircles;
	}

	public void setFileLifeCircles(List<FileLifeCircle> fileLifeCircles) {
		this.fileLifeCircles = fileLifeCircles;
	}

	public ProjectFilesLifeCircle(long id, String projectName, String groupName, List<FileLifeCircle> fileLifeCircles) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.groupName = groupName;
		this.fileLifeCircles = fileLifeCircles;
	}

	public ProjectFilesLifeCircle() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectFilesLifeCircle [id=" + id + ", projectName=" + projectName + ", groupName=" + groupName
				+ ", fileLifeCircles=" + fileLifeCircles + "]";
	}
}
