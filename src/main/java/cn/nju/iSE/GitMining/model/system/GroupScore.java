package cn.nju.iSE.GitMining.model.system;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "group_score")
public class GroupScore implements Serializable {

    private static final long serialVersionUID = -3705700864301737598L;

    @Id
    private long id;

    @Field(value = "group_name")
    private String groupName;

    @Field(value = "url")
    private String groupUrl;

    private String phaseIName;
    private String phaseIIName;
    private String phaseIIIName;

    private long phaseIiD;
    private long phaseIIiD;
    private long phaseIIIiD;

    @Field(value = "phase_i_point")
    private int phaseI;

    @Field(value = "phase_ii_point")
    private int phaseII;

    @Field(value = "pahse_iii_point")
    private int phaseIII;

    private double weightI;

    private double weightII;

    private double weightIII;

    @Field(value = "final_score")
    private double finalScore;
    /*
    These scores are marked by teachers, and should be initialed by score.xml.
     */
    @Field(value = "score_i")
    private double pahseIScore;

    @Field(value = "score_ii")
    private double phaseIIScore;

    @Field(value = "score_iii")
    private double phaseIIIScore;

    public GroupScore() {
    }

    public GroupScore(long id, String groupName, String groupUrl,
                      String phaseIName, String phaseIIName, String phaseIIIName,
                      long phaseIiD, long phaseIIiD, long phaseIIIiD,
                      int phaseI, int phaseII, int phaseIII,
                      double weightI, double weightII, double weightIII, double finalScore,
                      double pahseIScore, double phaseIIScore, double phaseIIIScore) {
        this.id = id;
        this.groupName = groupName;
        this.groupUrl = groupUrl;
        this.phaseIName = phaseIName;
        this.phaseIIName = phaseIIName;
        this.phaseIIIName = phaseIIIName;
        this.phaseIiD = phaseIiD;
        this.phaseIIiD = phaseIIiD;
        this.phaseIIIiD = phaseIIIiD;
        this.phaseI = phaseI;
        this.phaseII = phaseII;
        this.phaseIII = phaseIII;
        this.weightI = weightI;
        this.weightII = weightII;
        this.weightIII = weightIII;
        this.finalScore = finalScore;
        this.pahseIScore = pahseIScore;
        this.phaseIIScore = phaseIIScore;
        this.phaseIIIScore = phaseIIIScore;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupUrl() {
        return groupUrl;
    }

    public void setGroupUrl(String groupUrl) {
        this.groupUrl = groupUrl;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPhaseIName() {
        return phaseIName;
    }

    public void setPhaseIName(String phaseIName) {
        this.phaseIName = phaseIName;
    }

    public String getPhaseIIName() {
        return phaseIIName;
    }

    public void setPhaseIIName(String phaseIIName) {
        this.phaseIIName = phaseIIName;
    }

    public String getPhaseIIIName() {
        return phaseIIIName;
    }

    public void setPhaseIIIName(String phaseIIIName) {
        this.phaseIIIName = phaseIIIName;
    }

    public long getPhaseIiD() {
        return phaseIiD;
    }

    public void setPhaseIiD(long phaseIiD) {
        this.phaseIiD = phaseIiD;
    }

    public long getPhaseIIiD() {
        return phaseIIiD;
    }

    public void setPhaseIIiD(long phaseIIiD) {
        this.phaseIIiD = phaseIIiD;
    }

    public long getPhaseIIIiD() {
        return phaseIIIiD;
    }

    public void setPhaseIIIiD(long phaseIIIiD) {
        this.phaseIIIiD = phaseIIIiD;
    }

    public int getPhaseI() {
        return phaseI;
    }

    public void setPhaseI(int phaseI) {
        this.phaseI = phaseI;
    }

    public int getPhaseII() {
        return phaseII;
    }

    public void setPhaseII(int phaseII) {
        this.phaseII = phaseII;
    }

    public int getPhaseIII() {
        return phaseIII;
    }

    public void setPhaseIII(int phaseIII) {
        this.phaseIII = phaseIII;
    }

    public double getWeightI() {
        return weightI;
    }

    public void setWeightI(double weightI) {
        this.weightI = weightI;
    }

    public double getWeightII() {
        return weightII;
    }

    public void setWeightII(double weightII) {
        this.weightII = weightII;
    }

    public double getWeightIII() {
        return weightIII;
    }

    public void setWeightIII(double weightIII) {
        this.weightIII = weightIII;
    }

    public double getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(double finalScore) {
        this.finalScore = finalScore;
    }

    public double getPahseIScore() {
        return pahseIScore;
    }

    public void setPahseIScore(double pahseIScore) {
        this.pahseIScore = pahseIScore;
    }

    public double getPhaseIIScore() {
        return phaseIIScore;
    }

    public void setPhaseIIScore(double phaseIIScore) {
        this.phaseIIScore = phaseIIScore;
    }

    public double getPhaseIIIScore() {
        return phaseIIIScore;
    }

    public void setPhaseIIIScore(double phaseIIIScore) {
        this.phaseIIIScore = phaseIIIScore;
    }

    @Override
    public String toString() {
        return "GroupScore{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", phaseI=" + phaseI +
                ", phaseII=" + phaseII +
                ", phaseIII=" + phaseIII +
                ", weightI=" + weightI +
                ", weightII=" + weightII +
                ", weightIII=" + weightIII +
                ", finalScore=" + finalScore +
                ", pahseIScore=" + pahseIScore +
                ", phaseIIScore=" + phaseIIScore +
                ", phaseIIIScore=" + phaseIIIScore +
                '}';
    }
}
