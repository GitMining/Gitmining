package cn.nju.iSE.GitMining.model.code;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.common.enumeration.From;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.code.ProjectOverview.java
 *  
 *  2018年3月29日	上午9:54:20  
*/
public class ProjectOverview extends ProjectInfo implements Serializable{
	
	private static final long serialVersionUID = 4173601617092464373L;
	
	private String owner;
	
	private From repoSource;
	
	private List<Double> scores;
	
	public From getRepoSource() {
		return repoSource;
	}

	public void setRepoSource(From repoSource) {
		this.repoSource = repoSource;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<Double> getScores() {
		return scores;
	}

	public void setScores(List<Double> scores) {
		this.scores = scores;
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectOverview] ProjectName: ");
		sBuffer.append(super.getProjectName());
		sBuffer.append("Owner: ");
		sBuffer.append(owner);
		return sBuffer.toString();
	}
	
}
