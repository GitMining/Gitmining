package cn.nju.iSE.GitMining.model.system;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.common.enumeration.State;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Document
public class AnalyzingState implements Serializable {

    private static final long serialVersionUID = 2727687744257925864L;
    @Id
    private long id;

    private long groupId;

    private String projectName;

    private String projectNamespace;

    private From platform;

    private State state;

    private long lastUpdate;

    public AnalyzingState() {
    }

    public AnalyzingState(long id, long groupId, String projectName, String projectNamespace, From platform, State state, long lastUpdate) {
        this.id = id;
        this.groupId = groupId;
        this.projectName = projectName;
        this.projectNamespace = projectNamespace;
        this.platform = platform;
        this.state = state;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectNamespace() {
        return projectNamespace;
    }

    public void setProjectNamespace(String projectNamespace) {
        this.projectNamespace = projectNamespace;
    }

    public From getPlatform() {
        return platform;
    }

    public void setPlatform(From platform) {
        this.platform = platform;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
