package cn.nju.iSE.GitMining.model.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.nju.iSE.GitMining.common.enumeration.From;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.system.Group.java
 *  
 *  2018年4月21日	下午3:23:44  
*/
@Document
public class Group extends Owners implements Serializable {

	private static final long serialVersionUID = 6368096143258010914L;

	private List<Developer> developers = new ArrayList<Developer>();

	public List<Developer> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<Developer> developers) {
		this.developers = developers;
	}

	public Group(long id, long ownerId, String ownerName, List<Long> projectIds, From platform, long time,
			List<Developer> developers) {
		super(id, ownerId, ownerName, projectIds, platform, time);
		this.developers = developers;
	}

	public Group(long id, long ownerId, String ownerName, List<Long> projectIds, From platform, long time,
				 String ownerURL, List<Developer> developers) {
		super(id, ownerId, ownerName, projectIds, platform, time, ownerURL);
		this.developers = developers;
	}

	public Group() {
		super();
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Group] id: ");
		sBuffer.append(getId());
		sBuffer.append(", Group Name: ");
		sBuffer.append(getOwnerName());
		sBuffer.append(", Platform: ");
		sBuffer.append(getPlatform());
		if(developers != null && developers.size() != 0) {
			sBuffer.append(", Members: \n");
			for(Developer d: developers) {
				sBuffer.append(d.getOwnerName());
				sBuffer.append(" ");
			}	
		}
		if(getProjectIds() != null && getProjectIds().size() != 0) {
			sBuffer.append("\nProjects: ");
			sBuffer.append(getProjectIds());	
		} 
		return sBuffer.toString();
	}
	
}
