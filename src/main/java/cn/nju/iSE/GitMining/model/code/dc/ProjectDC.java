package cn.nju.iSE.GitMining.model.code.dc;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import iSESniper.code.entity.cpd.CpdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author VousAttendezrer
 * @fileName : cn.nju.iSE.GitMining.model.loc.ProjectLOCEntity.java
 * 
 *	2018年5月3日 下午16:55:15
 */
@Document
public class ProjectDC extends ProjectInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6882823496299886492L;
	private List<CpdEntity> CpdEntities;
	
	public ProjectDC() {
		super();
	}
	
	public ProjectDC(long id, String projectPath, String projectName, String groupName, long version,
			List<CpdEntity> CpdEntities) {
		super(id, projectPath, projectName, groupName, version);
		this.CpdEntities = CpdEntities;
	}
	
	public ProjectDC(long id, String projectPath, String projectName , long version,
			List<CpdEntity> CpdEntities) {
		super(id, projectPath, projectName, version);
		this.CpdEntities = CpdEntities;
	}
	
	public List<CpdEntity> getCpdEntities() {
		return CpdEntities;
	}
	public void setCpdEntities(List<CpdEntity> cpdEntities) {
		CpdEntities = cpdEntities;
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Duplication Code] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if(CpdEntities == null) {
			sBuffer.append("CpdEntities List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", CpdEntities List: \n");
		for(CpdEntity cpd: CpdEntities) {
			sBuffer.append("DuplicationLineCount:");
			sBuffer.append(cpd.getDuplicateLineCount());
			sBuffer.append(", FileNameA:");
			sBuffer.append(cpd.getFileNameA());
			sBuffer.append(", Duplicate Code Start LineA:");
			sBuffer.append(cpd.getDuplicateCodeStartLineA());
			sBuffer.append("; FileNameB:");
			sBuffer.append(cpd.getFileNameB());
			sBuffer.append(", Duplicate Code Start LineB:");
			sBuffer.append(cpd.getDuplicateCodeStartLineB());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
	
}
