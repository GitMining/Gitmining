package cn.nju.iSE.GitMining.model.code;

import org.springframework.data.annotation.Id;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.ProjectInfo.java
 *  
 *  2018年3月18日	下午12:48:17  
*/

public abstract class ProjectInfo{
	@Id
	private long id;
	private String projectPath;
	private String projectName;
	private String groupName;
	private long version;
	public String getProjectPath() {
		return projectPath;
	}
	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Deprecated
	public String getprojectPath() {
		return projectPath;
	}
	@Deprecated
	public void setprojectPath(String projectPath) {
		this.projectPath = projectPath;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public ProjectInfo(long id, String projectPath, String projectName, String groupName, long version) {
		super();
		this.id = id;
		this.projectPath = projectPath;
		this.projectName = projectName;
		this.groupName = groupName;
		this.version = version;
	}
	public ProjectInfo(long id, String projectPath, String projectName, long version) {
		super();
		this.id = id;
		this.projectPath = projectPath;
		this.projectName = projectName;
		this.version = version;
	}
	public ProjectInfo(long id, String projectPath) {
		super();
		this.id = id;
		this.projectPath = projectPath;
	}
	public ProjectInfo(String projectPath) {
		super();
		this.projectPath = projectPath;
	}
	public ProjectInfo() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Profile] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(", VERSION: ");
		sBuffer.append(getVersion());
		return sBuffer.toString();
	}
}
