package cn.nju.iSE.GitMining.model.code.vab;

import java.io.Serializable;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.model.code.vab.Variable.java
 * 
 *           2018年3月19日 上午11:04:40
 */

public class Variable implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4871847398908529131L;

	private String clazz;

	private String vName;
	
	public Variable() {
		super();
	}

	public Variable(String clazz, String vName) {
		super();
		this.clazz = clazz;
		this.vName = vName;
	}

	public String getClazz() {
		return clazz;
	}

	public String getvName() {
		return vName;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public void setvName(String vName) {
		this.vName = vName;
	}

	@Override
	public String toString() {
		return "[Variable] " + " " + clazz + " " + vName;
	}
}
