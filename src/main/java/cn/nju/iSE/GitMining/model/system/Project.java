package cn.nju.iSE.GitMining.model.system;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.model.system.Project.java
 * 
 *           2018年4月21日 下午8:21:07
 */
@Document
public class Project implements Serializable {

	private static final long serialVersionUID = 4449311187546660697L;

	@Id
	private long id;

	private String name;

	private String fullName;

	private String namespace;

	private String url;

	private List<String> commitIds;
	// The project created time.
	private long time;

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getCommitIds() {
		return commitIds;
	}

	public void setCommitIds(List<String> commitIds) {
		this.commitIds = commitIds;
	}

	public Project(long id, String name, String fullName, String namespace, String url, List<String> commitIds,
			long time) {
		super();
		this.id = id;
		this.name = name;
		this.fullName = fullName;
		this.namespace = namespace;
		this.url = url;
		this.commitIds = commitIds;
		this.time = time;
	}

	public Project() {
		super();
	}

	@Override
	public String toString() {
		return "Project [id: " + id + ", name: " + name + ", Full Name: " + fullName + ", url: " + url +  "]";
	}
}
