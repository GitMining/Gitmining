package cn.nju.iSE.GitMining.model.system;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "project_score")
public class ProjectScore implements Serializable {
    private static final long serialVersionUID = -1358675443657321501L;

    @Id
    private long id;

    private String projectName;

    private String groupName;

    private String projectUrl;

    /**
     * The structure of code quality's xxxScore likes below:
     * {
     *     indicator: xxx,
     *     score: true or false,
     *     threshold: t,
     *     rank_warning_list: [],
     *     rank_awful_list:[]
     * }
     * threshold means that if the size of awful list above some number, the score of this indicator would be set to false.
     */
    private IndicatoreScore locScore;

    private IndicatoreScore dcScore;

    private IndicatoreScore emScore;

    private IndicatoreScore loaScore;

    private IndicatoreScore ccScore;

    private IndicatoreScore ldScore;

    private IndicatoreScore pdScore;

    private IndicatoreScore vdScore;

    private List<IndicatoreScore> indicatoreScores;

    public ProjectScore(long id, String projectName, String groupName, String projectUrl,
                        IndicatoreScore locScore, IndicatoreScore dcScore, IndicatoreScore emScore,
                        IndicatoreScore loaScore, IndicatoreScore ccScore, IndicatoreScore ldScore,
                        IndicatoreScore pdScore, IndicatoreScore vdScore) {
        this.id = id;
        this.projectName = projectName;
        this.groupName = groupName;
        this.projectUrl = projectUrl;
        this.locScore = locScore;
        this.dcScore = dcScore;
        this.emScore = emScore;
        this.loaScore = loaScore;
        this.ccScore = ccScore;
        this.ldScore = ldScore;
        this.pdScore = pdScore;
        this.vdScore = vdScore;
    }

    public ProjectScore(long id, String projectName, String groupName, String projectUrl, IndicatoreScore locScore,
                        IndicatoreScore dcScore, IndicatoreScore emScore, IndicatoreScore loaScore, IndicatoreScore ccScore,
                        IndicatoreScore ldScore, IndicatoreScore pdScore, IndicatoreScore vdScore,
                        List<IndicatoreScore> indicatoreScores) {
        this.id = id;
        this.projectName = projectName;
        this.groupName = groupName;
        this.projectUrl = projectUrl;
        this.locScore = locScore;
        this.dcScore = dcScore;
        this.emScore = emScore;
        this.loaScore = loaScore;
        this.ccScore = ccScore;
        this.ldScore = ldScore;
        this.pdScore = pdScore;
        this.vdScore = vdScore;
        this.indicatoreScores = indicatoreScores;
    }

    public ProjectScore() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getProjectUrl() {
        return projectUrl;
    }

    public void setProjectUrl(String projectUrl) {
        this.projectUrl = projectUrl;
    }

    public IndicatoreScore getLocScore() {
        return locScore;
    }

    public void setLocScore(IndicatoreScore locScore) {
        this.locScore = locScore;
    }

    public IndicatoreScore getDcScore() {
        return dcScore;
    }

    public void setDcScore(IndicatoreScore dcScore) {
        this.dcScore = dcScore;
    }

    public IndicatoreScore getEmScore() {
        return emScore;
    }

    public void setEmScore(IndicatoreScore emScore) {
        this.emScore = emScore;
    }

    public IndicatoreScore getLoaScore() {
        return loaScore;
    }

    public void setLoaScore(IndicatoreScore loaScore) {
        this.loaScore = loaScore;
    }

    public IndicatoreScore getCcScore() {
        return ccScore;
    }

    public void setCcScore(IndicatoreScore ccScore) {
        this.ccScore = ccScore;
    }

    public IndicatoreScore getLdScore() {
        return ldScore;
    }

    public void setLdScore(IndicatoreScore ldScore) {
        this.ldScore = ldScore;
    }

    public IndicatoreScore getPdScore() {
        return pdScore;
    }

    public void setPdScore(IndicatoreScore pdScore) {
        this.pdScore = pdScore;
    }

    public IndicatoreScore getVdScore() {
        return vdScore;
    }

    public void setVdScore(IndicatoreScore vdScore) {
        this.vdScore = vdScore;
    }

    public List<IndicatoreScore> getIndicatoreScores() {
        return indicatoreScores;
    }

    public void setIndicatoreScores(List<IndicatoreScore> indicatoreScores) {
        this.indicatoreScores = indicatoreScores;
    }

    @Override
    public String toString() {
        return "ProjectScore{" +
                "id=" + id +
                ", projectName='" + projectName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", projectUrl='" + projectUrl + '\'' +
                ", locScore=" + locScore +
                ", dcScore=" + dcScore +
                ", emScore=" + emScore +
                ", loaScore=" + loaScore +
                ", ccScore=" + ccScore +
                ", ldScore=" + ldScore +
                ", pdScore=" + pdScore +
                ", vdScore=" + vdScore +
                '}';
    }
}
