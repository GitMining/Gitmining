package cn.nju.iSE.GitMining.model.code.loc;

import java.io.Serializable;

import iSESniper.code.entity.loc.LinesOfCodeEntity;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.loc.LinesOfCodeEntity.java
 *  
 *  2018年3月15日	上午10:49:08  
*/

public class LOCEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7366900049976034204L;
	private String pth;
	private long lines;
	private long blankLines;
	
	public LOCEntity() {
		
	}

	public LOCEntity(String pth, long loc, long blkLoc) {
		super();
		this.pth = pth;
		this.lines = loc;
		this.blankLines = blkLoc;
	}
	
	public LOCEntity(String pth, long lines) {
		super();
		this.pth = pth;
		this.lines = lines;
	}
	
	public LOCEntity(LinesOfCodeEntity loce) {
		this.pth = loce.getPth();
		this.lines = loce.getLoc();
		this.blankLines = loce.getBlkLoc();
	}

	public String getPth() {
		return pth;
	}

	public long getLoc() {
		return lines;
	}

	public long getBlkLoc() {
		return blankLines;
	}

	public long getLines() {
		return lines;
	}

	public void setLines(long lines) {
		this.lines = lines;
	}

	public long getBlankLines() {
		return blankLines;
	}

	public void setBlankLines(long blankLines) {
		this.blankLines = blankLines;
	}

	public void setPth(String pth) {
		this.pth = pth;
	}

	@Override
	public String toString() {
		return "[LOC] module_name= [" + pth + "], lines=" + lines + ", blankLines=" + blankLines + "\n";
	}
	/**
	 * 
	 */
}
