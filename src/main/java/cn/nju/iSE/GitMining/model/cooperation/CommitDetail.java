package cn.nju.iSE.GitMining.model.cooperation;

import java.io.Serializable;
import java.util.List;
import java.util.TimeZone;

import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.FileDiff;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.cooperation.Commit.java
 *  
 *  2018年4月20日	上午10:28:35  
*/

public class CommitDetail implements Serializable {

	private static final long serialVersionUID = -1941749349744192456L;

	private String id;
	
	//project likes this "namespace/projectname" 
	private String project;
	
	private Author author;
	
	private long when;
	
	private TimeZone timeZone;
	
	private String message;
	
	private long allAdded;
	
	private long allRemoved;
	
	private long addedFiles;
	
	private long deletedFiles;
	
	private long modifiedFiles;
	
	private List<FileDiff> files;

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public long getAddedFiles() {
		return addedFiles;
	}

	public void setAddedFiles(long addedFiles) {
		this.addedFiles = addedFiles;
	}

	public long getDeletedFiles() {
		return deletedFiles;
	}

	public void setDeletedFiles(long deletedFiles) {
		this.deletedFiles = deletedFiles;
	}

	public long getModifiedFiles() {
		return modifiedFiles;
	}

	public void setModifiedFiles(long modifiedFiles) {
		this.modifiedFiles = modifiedFiles;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public long getWhen() {
		return when;
	}

	public void setWhen(long when) {
		this.when = when;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getAllAdded() {
		return allAdded;
	}

	public void setAllAdded(long allAdded) {
		this.allAdded = allAdded;
	}

	public long getAllRemoved() {
		return allRemoved;
	}

	public void setAllRemoved(long allRemoved) {
		this.allRemoved = allRemoved;
	}

	public List<FileDiff> getFiles() {
		return files;
	}

	public void setFiles(List<FileDiff> files) {
		this.files = files;
	}

	public CommitDetail(String id, String project, Author author, long when, TimeZone timeZone, String message,
			long allAdded, long allRemoved, long addedFiles, long deletedFiles, long modifiedFiles,
			List<FileDiff> files) {
		super();
		this.id = id;
		this.project = project;
		this.author = author;
		this.when = when;
		this.timeZone = timeZone;
		this.message = message;
		this.allAdded = allAdded;
		this.allRemoved = allRemoved;
		this.addedFiles = addedFiles;
		this.deletedFiles = deletedFiles;
		this.modifiedFiles = modifiedFiles;
		this.files = files;
	}

	public CommitDetail() {
		super();
	}

	@Override
	public String toString() {
		return "CommitDetail [id=" + id + ", project=" + project + ", author=" + author + ", when=" + when
				+ ", timeZone=" + timeZone + ", message=" + message + ", allAdded=" + allAdded + ", allRemoved="
				+ allRemoved + ", addedFiles=" + addedFiles + ", deletedFiles=" + deletedFiles + ", modifiedFiles="
				+ modifiedFiles + ", files=" + files + "]";
	}
}
