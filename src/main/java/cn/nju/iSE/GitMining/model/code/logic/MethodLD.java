package cn.nju.iSE.GitMining.model.code.logic;

import java.io.Serializable;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.logic.MethodLD.java
 *  
 *  2018年3月18日	下午12:45:29  
*/

public class MethodLD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7426584572849042057L;
	private String path;
	private int ldIdx;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getLdIdx() {
		return ldIdx;
	}
	public void setLdIdx(int ldIdx) {
		this.ldIdx = ldIdx;
	}
	public MethodLD(String path, int ldIdx) {
		super();
		this.path = path;
		this.ldIdx = ldIdx;
	}
	public MethodLD() {
		super();
	}
	

}
