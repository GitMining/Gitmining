package cn.nju.iSE.GitMining.model.code.loc;

import java.io.Serializable;
import java.util.ArrayList;

import iSESniper.code.entity.loc.FileLinesOfCodeEntity;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.loc.NonJavaLOCEntity.java
 *  
 *  2018年3月15日	上午10:54:34  
*/

public class NonJavaLOCEntity extends LOCEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3486341640271769247L;
	
	private ArrayList<Long> blankNum;
	
	public NonJavaLOCEntity() {
		super();
	}
	
	public NonJavaLOCEntity(String pth, long loc, long blkLoc) {
		super(pth, loc, blkLoc);
	}
	
	public NonJavaLOCEntity(String pth, long loc, long blkLoc, ArrayList<Long> blkNum) {
		super(pth, loc, blkLoc);
		this.blankNum = blkNum;
	}
	
	public NonJavaLOCEntity(FileLinesOfCodeEntity njloc) {
		super(njloc.getPth(), njloc.getLoc(), njloc.getBlkLoc());
		this.blankNum = njloc.getBlankNum();
	}

	public ArrayList<Long> getBlankNum() {
		return blankNum;
	}

	public void setBlankNum(ArrayList<Long> blankNum) {
		this.blankNum = blankNum;
	}

	@Override
	public String toString() {
		return super.toString() + "each blank number is:" + blankNum.toString() + "\n";
	}
}
