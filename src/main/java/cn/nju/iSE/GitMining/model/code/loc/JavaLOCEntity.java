package cn.nju.iSE.GitMining.model.code.loc;

import java.io.Serializable;
import java.util.ArrayList;

import iSESniper.code.entity.loc.JavaLinesOfCodeEntity;
import iSESniper.code.entity.loc.MethodLinesOfCodeEntity;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.model.loc.JavaLOCEntity.java
 * 
 *           2018年3月15日 上午11:25:53
 */

public class JavaLOCEntity extends LOCEntity implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -838671412402260520L;

	private int methodNum;

	private ArrayList<MethodLOCEntity> methodList;

	public JavaLOCEntity() {
		super();
	}
	
	public JavaLOCEntity(String pth, long loc) {
		super(pth, loc);
	}

	public JavaLOCEntity(String pth, long loc, int methodNum, ArrayList<MethodLOCEntity> methodList) {
		super(pth, loc);
		this.methodNum = methodNum;
		this.methodList = methodList;
	}
	
	public JavaLOCEntity(JavaLinesOfCodeEntity jloc) {
		super(jloc.getPth(),jloc.getLoc());
		this.methodNum = jloc.getMethodNum();
		this.methodList = new ArrayList<MethodLOCEntity>();
		for(MethodLinesOfCodeEntity me: jloc.getMethodList()) {
			this.methodList.add(new MethodLOCEntity(me));
		}
	}

	public int getMethodNum() {
		return methodNum;
	}

	public ArrayList<MethodLOCEntity> getMethodList() {
		return methodList;
	}

	public void setMethodNum(int methodNum) {
		this.methodNum = methodNum;
	}

	public void setMethodList(ArrayList<MethodLOCEntity> methodList) {
		this.methodList = methodList;
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[JavaLOC] FullName: ");
		sBuffer.append(getPth());
		sBuffer.append(", MethodNumber: ");
		sBuffer.append(methodNum);
		sBuffer.append(", MethodList: \n");
		for(MethodLOCEntity me: methodList) {
			sBuffer.append(me.toString());
			sBuffer.append("\n");
		}
		sBuffer.append("TOTAL LOC: ");
		sBuffer.append(getLoc());
		return sBuffer.toString();
	}

}
