package cn.nju.iSE.GitMining.model.code.logic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.model.code.ProjectInfo;
import iSESniper.code.scanner.exception.FilesListIsEmptyException;
import iSESniper.code.start.exception.PathNotFoundException;
import iSESniper.code.start.impl.ProjectAnalyzer;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.logic.ProjectCC.java
 *  
 *  2018年3月18日	上午9:17:27  
*/
@Document
public class ProjectCC extends ProjectInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6316764464901364688L;
	
	private List<MethodCC> methodsCC;

	public List<MethodCC> getMethodsCC() {
		return methodsCC;
	}

	public void setMethodsCC(List<MethodCC> methodsCC) {
		this.methodsCC = methodsCC;
	}

	public ProjectCC() {
		super();
	}

	public ProjectCC(long id, String projectPath, String projectName, String groupName, long version,
			List<MethodCC> methodsCC) {
		super(id, projectPath, projectName, groupName, version);
		this.methodsCC = methodsCC;
	}

	public ProjectCC(long id, String projectPath, String projectName, long version, List<MethodCC> methodsCC) {
		super(id, projectPath, projectName, version);
		this.methodsCC = methodsCC;
	}

	@Deprecated
	public ProjectCC(long id, String projectPath) throws PathNotFoundException, FilesListIsEmptyException {
		super(id, projectPath);
		this.methodsCC = new ArrayList<MethodCC>();
		ProjectAnalyzer pAnalyzer = new ProjectAnalyzer(projectPath);
		List<Map<String, Integer>> methods = pAnalyzer.getMethodsCC();
		for(Map<String, Integer> m: methods) {
			String rawpth = m.keySet().iterator().next();
			String pth = rawpth.replaceAll(".java", "");
			pth = pth.replaceAll("\\\\", ".");
			MethodCC mc = new MethodCC();
			mc.setPath(pth);
			mc.setCcidx(m.get(rawpth));
			this.methodsCC.add(mc);
		}
		setProjectName(pAnalyzer.getPrjtName());
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Cyclomatic Complexity] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if(methodsCC == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods List: \n");
		for(MethodCC mcc: methodsCC) {
			sBuffer.append(mcc.getPath());
			sBuffer.append("  Cyclomatic Complexity: ");
			sBuffer.append(mcc.getCcidx());
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
	
}
