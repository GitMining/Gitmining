package cn.nju.iSE.GitMining.model.code.loc;

import java.io.Serializable;

import iSESniper.code.entity.loc.MethodLinesOfCodeEntity;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.model.loc.MethodLOCEntity.java
 *  
 *  2018年3月17日	上午9:50:42  
*/

public class MethodLOCEntity extends LOCEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2412141751233962684L;
	private String methodName;
	
	public MethodLOCEntity() {
		super();
	}
	
	public MethodLOCEntity(String pth, long loc) {
		super(pth, loc, 0);
	}

	public MethodLOCEntity(String pth, long loc, String methodName) {
		super(pth, loc, 0);
		this.methodName = methodName;
	}
	
	public MethodLOCEntity(MethodLinesOfCodeEntity mloc) {
		super(mloc.getPth(), mloc.getLoc());
		this.methodName = mloc.getMethodName();
		
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	@Override
	@Deprecated
	public long getBlkLoc() {
		return 0;
	}

	@Override
	public String toString() {
		return "[MethodLOC] [methodName: " + methodName + ", Path: " + getPth() + ", Loc: " + getLoc()
				+ "]";
	}
	
	
}
