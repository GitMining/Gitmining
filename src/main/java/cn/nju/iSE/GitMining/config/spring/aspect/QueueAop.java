package cn.nju.iSE.GitMining.config.spring.aspect;

import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class QueueAop {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution( * cn.nju.iSE.GitMining.queue.*Sender.send(..))")
    public void sendPointcut(){}

    @Pointcut("execution(public * cn.nju.iSE.GitMining.queue.*Receiver*.process(..))")
    public void processPointcut(){}

    @Before("sendPointcut()")
    public void doBeforeSend(JoinPoint joinPoint){
        logger.info("[WorkingQueue - SENDER] - CLASS_METHOD: " + joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        Object[] objects = joinPoint.getArgs();
        StringBuffer args = new StringBuffer();
        int count = 1;
        for(Object object: objects) {
            args.append("PARAM #");
            args.append(count);
            args.append(": ");
            args.append(object);
            args.append(" ");
            count++;
        }
        logger.info("[WorkingQueue - SENDER] - PARAMETERS: " + args.toString());
    }

    @Before("processPointcut()")
    public void doBeforeProecss(JoinPoint joinPoint){
        logger.info("[WorkingQueue - RECEIVER] - CLASS_METHOD: " + joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        Object[] objects = joinPoint.getArgs();
        StringBuffer args = new StringBuffer();
        for(Object object: objects) {
            args.append("[");
            args.append(object);
            args.append("] ");
        }
        logger.info("[WorkingQueue - RECEIVER] - PARAMETERS: " + args.toString());
    }

    @After(value = "sendPointcut()")
    public void doAfterSend(){
        logger.info("[WorkingQueue - SENDER] - After: " + "OK");
    }

    @After("processPointcut()")
    public void doAfterProcess(){
        logger.info("[WorkingQueue - RECEIVER] - After: " + "OK");
    }
}
