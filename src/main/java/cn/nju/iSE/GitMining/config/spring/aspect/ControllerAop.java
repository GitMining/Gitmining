package cn.nju.iSE.GitMining.config.spring.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Aspect
@Configuration
public class ControllerAop {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution(public * cn.nju.iSE.GitMining.web.ctrl..*(..))")
    public void ctrlLog(){}

    @Before("ctrlLog()")
    public void doBefore(JoinPoint joinPoint){
        // Receive request, and record content
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // Record content
        logger.info("URL: " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD: " + request.getMethod());
        logger.info("IP: " + request.getRemoteAddr());
        logger.info("CLASS_METHOD: " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS: " + Arrays.toString(joinPoint.getArgs()));
    }

    @AfterReturning(pointcut = "ctrlLog()", returning = "rtn")
    public void doAfterReturning(Object rtn) throws Throwable{
        // After processing request completely, return value.
        logger.info("RESPONSE: " + rtn);
    }
}
