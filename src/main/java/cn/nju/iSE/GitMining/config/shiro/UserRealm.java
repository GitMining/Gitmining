package cn.nju.iSE.GitMining.config.shiro;

import cn.nju.iSE.GitMining.model.auth.UserInfo;
import com.alibaba.fastjson.JSONArray;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.model.auth.SysRole;
import cn.nju.iSE.GitMining.service.auth.SysPermissionService;
import cn.nju.iSE.GitMining.service.auth.SysRoleService;
import cn.nju.iSE.GitMining.service.auth.UserInfoService;
import cn.nju.iSE.GitMining.common.util.Constant;

import java.util.List;

public class UserRealm extends AuthorizingRealm{

    private Logger logger = LoggerFactory.getLogger(UserRealm.class);

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysPermissionService sysPermissionService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//        Session session = SecurityUtils.getSubject().getSession();
        //查询用户的权限
//        JSONObject permissions = (JSONObject) session.getAttribute(Constant.SESSION_USER_PERMISSION);
//        logger.info("permission的值为:" + permissions);
//        logger.info("本用户权限为:" +permissions);
        //为当前用户设置角色和权限
//        System.out.println(session);
//        System.out.println(permissions);
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        String username = (String) principals.getPrimaryPrincipal();
        UserInfo userInfo = userInfoService.checkUser(username);
        SysRole role = sysRoleService.getRole(userInfo.getRoleId());
        authorizationInfo.addRole(role.getRoleName());
        List<Integer> permIds = role.getPermissionIds();
        JSONObject jsonObject = sysPermissionService.getPermissions(role);
        JSONArray jsonArray = jsonObject.getJSONArray(Constant.SESSION_USER_PERMISSION);
        for(int i = 0; i < jsonArray.size(); i++) {
            JSONObject permission = jsonArray.getJSONObject(i);
            authorizationInfo.addStringPermission(permission.getString("permission"));
        }
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String loginName = (String) token.getPrincipal();
        // 获取用户密码
        String password = new String((char[]) token.getCredentials());
        JSONObject user = userInfoService.checkUser(loginName, password);
        if (user == null) {
            //没找到帐号
            throw new UnknownAccountException();
        }
        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以自定义实现
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user.getString("username"),
                user.getString("password"),
//                ByteSource.Util.bytes(user.get("salt").getAsString()),
                //ByteSource.Util.bytes("salt"), salt=username+salt,采用明文访问时，不需要此句
                getName()
        );
        //session中不需要保存密码
//        user.remove("password");
        //将用户信息放入session中
//        SysRole role = sysRoleService.getRole(user.getInteger("roleId"));
//        user.put(Constant.SESSION_USER_PERMISSION, sysPermissionService.getPermissions(role));
//        SecurityUtils.getSubject().getSession().setAttribute(Constant.SESSION_USER_INFO, user);
        return authenticationInfo;
    }
}
