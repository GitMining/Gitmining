package cn.nju.iSE.GitMining.config.exception;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.common.util.CommonUtil;
import cn.nju.iSE.GitMining.web.constant.ErrorEnum;

@Controller
public class MainsiteErrorController {

    private static final String ERROR_PATH = "/myerror";

    /**
     * 主要是登陆后的各种错误路径  404页面改为返回此json
     * 未登录的情况下,大部分接口都已经被shiro拦截,返回让用户登录了
     *
     * @return 501的错误信息json
     */
    @RequestMapping(ERROR_PATH)
    @ResponseBody
    public JSONObject handleError(){
        return CommonUtil.errorJson(ErrorEnum.E_501);
    }

    @Override
    public String toString() {
        return ERROR_PATH;
    }
}
