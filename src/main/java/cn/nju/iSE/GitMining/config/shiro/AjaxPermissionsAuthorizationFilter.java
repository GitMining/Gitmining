package cn.nju.iSE.GitMining.config.shiro;

import java.io.PrintWriter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.alibaba.fastjson.JSONObject;

import cn.nju.iSE.GitMining.web.constant.ErrorEnum;
import org.springframework.http.HttpStatus;

public class AjaxPermissionsAuthorizationFilter extends FormAuthenticationFilter {
    private Logger log = LoggerFactory.getLogger(AjaxPermissionsAuthorizationFilter.class);
//    @Override
//    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("returnCode",  ErrorEnum.E_20011.getErrorCode());
//        jsonObject.put("returnMsg", ErrorEnum.E_20011.getErrorMsg());
//        PrintWriter out = null;
//        HttpServletResponse res = (HttpServletResponse) response;
//        try{
//            res.setCharacterEncoding("UTF-8");
//            res.setContentType("application/json");
//            out = response.getWriter();
//            out.print(jsonObject);
//        }catch (Exception e){
//            e.printStackTrace();
//        }finally {
//            if(null != out){
//                out.flush();
//                out.close();
//            }
//        }
//        return false;
//    }

    @Bean
    public FilterRegistrationBean registration(AjaxPermissionsAuthorizationFilter filter){
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {

        HttpServletRequest httpRequest = WebUtils.toHttp(request);

        HttpServletResponse httpResponse = WebUtils.toHttp(response);

        if (isLoginRequest(request, response)) {

            if (isLoginSubmission(request, response)) {

                if (log.isTraceEnabled()) {
                    log.trace("Login submission detected. Attempting to execute login.");
                }
                return executeLogin(request, response);

            } else {
                if (log.isTraceEnabled()) {
                    log.trace("Login page view.");
                }
                // allow them to see the login page ;)
                return true;
            }
        } else {
            if (log.isTraceEnabled()) {
                log.trace("Attempting to access a path which requires authentication. Forwarding to the "
                        + "Authentication url [" + getLoginUrl() + "]");
            }
            // 判断session里是否有用户信息
            if (httpRequest.getHeader("X-Requested-With") != null
                    && httpRequest.getHeader("X-Requested-With").equalsIgnoreCase("XMLHttpRequest")) {
                // 如果是ajax请求响应头会有，x-requested-with
                httpResponse.sendError(HttpStatus.UNAUTHORIZED.value());
                //redirectToLogin(request, response);
            } else {
                httpResponse.sendError(HttpStatus.UNAUTHORIZED.value());
            }
            return false;
        }
    }
}
