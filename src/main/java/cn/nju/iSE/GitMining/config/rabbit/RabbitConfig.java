package cn.nju.iSE.GitMining.config.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue testQueue() {
        return new Queue("test");
    }

    @Bean
    public Queue gitCloneQueue() {
        return new Queue("clone", true);
    }// true means the queue is durable.

    @Bean
    public Queue analyzingQueue() {
        return new Queue("analyze", true);
    }

    // Declare a topic exchange.
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("topicExchange");
    }

    // Binding queues and topic exchange
    @Bean
    public Binding bindingClone(){
        return BindingBuilder.bind(gitCloneQueue()).to(topicExchange()).with("clone");
    }

    @Bean
    public Binding bindingAnalyze(){
        return BindingBuilder.bind(analyzingQueue()).to(topicExchange()).with("analyze");
    }

}
