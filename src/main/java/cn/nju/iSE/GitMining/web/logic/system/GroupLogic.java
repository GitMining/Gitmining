package cn.nju.iSE.GitMining.web.logic.system;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.nju.iSE.GitMining.web.data.system.GroupVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.system.GroupLogic.java
 *  
 *  2018年4月23日	上午10:10:30  
*/

public interface GroupLogic {

	/**
	 * This method get all groups that under analyst controls.
	 * @return all groups list
	 */
	List<GroupVO> getGroups();
	
	/**
	 * This method get groups that under analyst controls,
	 * and the number of groups depends on the parameter "pageable".
	 * @param pageable
	 * @return
	 */
	Page<GroupVO> getGroups(Pageable pageable);
	
	/**
	 * This method get groups that were controlled between certain period. 
	 * @param start
	 * @param end
	 * @return
	 */
	List<GroupVO> getGroups(long start, long end);
	
	/**
	 * This method get groups that were controlled between certain period. 
	 * The number of groups depends on the parameter "pageable".
	 * @param start
	 * @param end
	 * @param pageable
	 * @return
	 */
	Page<GroupVO> getGroups(long start, long end, Pageable pageable);
	
	/**
	 * Get groups that are not added into database.
	 * @param baseAPI API that platform provides.
	 * @param charset
	 * @return The groups that are not added into database.
	 */
	List<GroupVO> getUncontrolledGroups(String baseAPI, String charset, String token);
	
	/**
	 * Save uncontrolled groups into database.
	 * @param groupVOs
	 */
	void saveUncontrolledGroups(List<GroupVO> groupVOs);
	
	/**
	 * Save uncontrolled group into database. 
	 * @param groupVO
	 */
	void saveUncontrolledGroup(GroupVO groupVO);

	JSONArray getGroupsWithScore(long start, long end);
}
