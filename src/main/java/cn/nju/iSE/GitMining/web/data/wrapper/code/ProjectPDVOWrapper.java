package cn.nju.iSE.GitMining.web.data.wrapper.code;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsParams;
import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.FileMethodsPDVO;
import cn.nju.iSE.GitMining.web.data.code.MethodPDVO;
import cn.nju.iSE.GitMining.web.data.code.ProjectPDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.ProjectPDVOWrapper.java
 * <p>
 * 2018年3月30日	下午7:51:05
 */
@Service
public class ProjectPDVOWrapper extends BaseWrappper<ProjectPDVO, ProjectParams> {

    @Autowired
    FileMethodPDVOWrapper fileMethodPDVOWrapper;

    @Override
    public ProjectPDVO wrap(ProjectParams model) {
        ProjectPDVO projectPDVO = new ProjectPDVO();
        projectPDVO.setId(model.getId());
        projectPDVO.setVersion(model.getVersion());
        projectPDVO.setProjectName(model.getProjectName());
        List<FileMethodsParams> fileMethodsParams = model.getFiles();
        List<FileMethodsPDVO> fileMethodsPDVOs = new ArrayList<FileMethodsPDVO>();
        Map<String, AnalysisRank> ranks = new HashMap<String, AnalysisRank>();
        for (FileMethodsParams fmp : fileMethodsParams) {
            FileMethodsPDVO fileMethodsPDVO = fileMethodPDVOWrapper.wrap(fmp);
            fileMethodsPDVOs.add(fileMethodsPDVO);
            String filename = fileMethodsPDVO.getFileName();
            Map<MethodPDVO, AnalysisRank> methodPDRank = fileMethodsPDVO.getMethodPDRank();
            Set<Entry<MethodPDVO, AnalysisRank>> entrys = methodPDRank.entrySet();
            for (Entry<MethodPDVO, AnalysisRank> entry : entrys)
                ranks.put(filename + ": " + entry.getKey().getMethodName(), entry.getValue());
        }
        projectPDVO.setGroupName(model.getGroupName());
        projectPDVO.setRanks(ranks);
        projectPDVO.setFileMethodsPDVOs(fileMethodsPDVOs);
        return projectPDVO;
    }

    @Override
    public ProjectParams unwrap(ProjectPDVO vo) {
        ProjectParams projectParams = new ProjectParams();
        projectParams.setProjectName(vo.getProjectName());
        projectParams.setProjectPath(Route.CLONE_PATH + vo.getProjectName());
        List<FileMethodsPDVO> fileMethodsPDVOs = vo.getFileMethodsPDVOs();
        List<FileMethodsParams> fileMethodsParams = new ArrayList<FileMethodsParams>();
        for (FileMethodsPDVO fmp : fileMethodsPDVOs) {
            fileMethodsParams.add(fileMethodPDVOWrapper.unwrap(fmp));
        }
        projectParams.setFiles(fileMethodsParams);
        return projectParams;
    }
}
