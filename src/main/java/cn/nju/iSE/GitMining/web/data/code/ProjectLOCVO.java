package cn.nju.iSE.GitMining.web.data.code;

import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.model.code.loc.JavaLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.MethodLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.NonJavaLOCEntity;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.data.ProjectLOCVO.java
 * 
 *           2018年3月29日 上午10:16:59
 */

public class ProjectLOCVO extends ProjectInfo {

	private List<NonJavaLOCEntity> nonJavaLOCEntities;
	private List<JavaLOCEntity> javaLOCEntities;
	private Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> javaLocRanks;
	private Map<String, AnalysisRank> ranks;

	public Map<String, AnalysisRank> getRanks() {
		return ranks;
	}

	public void setRanks(Map<String, AnalysisRank> ranks) {
		this.ranks = ranks;
	}

	public Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> getJavaLocRanks() {
		return javaLocRanks;
	}

	public void setJavaLocRanks(Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> javaLocRanks) {
		this.javaLocRanks = javaLocRanks;
	}

	public List<NonJavaLOCEntity> getNonJavaLOCEntities() {
		return nonJavaLOCEntities;
	}

	public void setNonJavaLOCEntities(List<NonJavaLOCEntity> nonJavaLOCEntities) {
		this.nonJavaLOCEntities = nonJavaLOCEntities;
	}

	public List<JavaLOCEntity> getJavaLOCEntities() {
		return javaLOCEntities;
	}

	public void setJavaLOCEntities(List<JavaLOCEntity> javaLOCEntities) {
		this.javaLOCEntities = javaLOCEntities;
	}

	public ProjectLOCVO(long id, String projectName, long version, List<NonJavaLOCEntity> nonJavaLOCEntities,
			List<JavaLOCEntity> javaLOCEntities) {
		super(id, projectName, version);
		this.nonJavaLOCEntities = nonJavaLOCEntities;
		this.javaLOCEntities = javaLOCEntities;
	}

	public ProjectLOCVO(long id, String projectName, long version, List<NonJavaLOCEntity> nonJavaLOCEntities,
			List<JavaLOCEntity> javaLOCEntities, Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> javaLocRanks,
			Map<String, AnalysisRank> ranks) {
		super(id, projectName, version);
		this.nonJavaLOCEntities = nonJavaLOCEntities;
		this.javaLOCEntities = javaLOCEntities;
		this.javaLocRanks = javaLocRanks;
		this.ranks = ranks;
	}

	public ProjectLOCVO(long id, String projectName, String groupName, long version,
			List<NonJavaLOCEntity> nonJavaLOCEntities, List<JavaLOCEntity> javaLOCEntities,
			Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> javaLocRanks, Map<String, AnalysisRank> ranks) {
		super(id, projectName, groupName, version);
		this.nonJavaLOCEntities = nonJavaLOCEntities;
		this.javaLOCEntities = javaLOCEntities;
		this.javaLocRanks = javaLocRanks;
		this.ranks = ranks;
	}

	public ProjectLOCVO() {
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append(super.toString());
		sBuffer.append("[ProjectLOCVO] nonJavaLOCEntities: ");
		for (NonJavaLOCEntity njEntity : nonJavaLOCEntities) {
			sBuffer.append(njEntity.toString());
		}
		for (JavaLOCEntity jle : javaLOCEntities) {
			sBuffer.append(jle.toString());
			sBuffer.append(" Analysis Rank: ");
			List<MethodLOCEntity> mEntities = jle.getMethodList();
			Map<MethodLOCEntity, AnalysisRank> ranks = javaLocRanks.get(jle);
			for (MethodLOCEntity mLocEntity : mEntities) {
				sBuffer.append("[");
				sBuffer.append(mLocEntity.getMethodName());
				sBuffer.append(": ");
				sBuffer.append(ranks.get(mLocEntity));
				sBuffer.append("] ");
			}
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
}
