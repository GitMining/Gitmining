package cn.nju.iSE.GitMining.web.ctrl.system;

import cn.nju.iSE.GitMining.common.util.CommonUtil;
import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.queue.CloneSender;
import cn.nju.iSE.GitMining.service.code.*;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.service.system.*;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectDetailVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.GroupLogic;
import cn.nju.iSE.GitMining.web.logic.system.ProjectDetailLogic;
import cn.nju.iSE.GitMining.web.logic.system.ProjectLogic;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.ctrl.system.SystemController.java
 * <p>
 * 2018年4月24日 下午2:11:07
 */
@RestController
@RequestMapping(Route.System.LetsStart)
public class SystemController {

    @Autowired
    private GroupLogic groupLogic;

    @Autowired
    private ProjectLogic projectLogic;

    @Autowired
    private ProjectDetailLogic projectDetailLogic;

    @Autowired
    private CommitsExtract commitsExtract;

    @Autowired
    private RepositoryClone repositoryClone;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CommitDetailService commitDetailService;

    @Autowired
    private ProjectDetailService projectDetailService;

    @Autowired
    private ProjectFilesLifeCircleService projectFilesLifeCircleService;

    @Autowired
    private ProjectCyclomaticComplexity projectCyclomaticComplexity;

    @Autowired
    private ProjectLogicDepth projectLogicDepth;

    @Autowired
    private ProjectLinesOfCode projectLinesOfCode;

    @Autowired
    private ProjectParameters projectParameters;

    @Autowired
    private ProjectVariablesDeclaration projectVariablesDeclaration;

    @Autowired
    private ProjectDuplicationCode projectDuplicationCode;

    @Autowired
    private ProjectLineOfAnnotation projectLineOfAnnotation;

    @Autowired
    private ProjectEmptyMethod projectEmptyMethod;

    @Autowired
    private CloneSender cloneSender;

    @RequestMapping("/hello")
    public String hello() {
        return "Hello";
    }

    // TODO Add user privileges management.

    @RequiresPermissions("group:list_uncontrolled")
    @RequestMapping(value = Route.System.AddNewGroups, method = RequestMethod.GET)
    public List<GroupVO> getUncontrolledGroups(@RequestParam("baseAPI") @NotNull String baseAPI) {
        System.out.println(groupLogic.getUncontrolledGroups(baseAPI, "", Constant.token));
        return groupLogic.getUncontrolledGroups(baseAPI, "", Constant.token);
    }

    @RequiresPermissions("group:super_add")
    @RequestMapping(value = Route.System.AddNewGroups, method = RequestMethod.POST)
    public List<ProjectVO> saveUncontrolledGroupsAndListUncontrolledProjects(
            @RequestBody @NotNull List<GroupVO> groupVOs) {
        groupLogic.saveUncontrolledGroups(groupVOs);
        List<ProjectVO> projectVOs = new ArrayList<ProjectVO>();
        for (GroupVO groupVO : groupVOs)
            projectVOs.addAll(projectLogic.getUncontrolledProjects(groupVO, Constant.token));
        return projectVOs;
    }

    /**
     * Store new projects and start to analyze.
     *
     * @param projectVOs
     */
    @RequestMapping(value = Route.System.StartAnalyze, method = RequestMethod.POST)
    @Deprecated
    public void saveUncontrolledProjects(@RequestBody @NotNull List<ProjectVO> projectVOs) {
        List<Project> projects = new ArrayList<Project>();
        for (ProjectVO projectVO : projectVOs) {
            // Clone remote repository.
            repositoryClone.clone(projectVO.getUrl());
            // Capture commits' id.
            Project project = projectLogic.transferPOVO(projectVO);
            projectService.captureCommitIds(project);
            projects.add(project);
            // Extract and store commits.
            String path = project.getFullName();
            List<CommitDetail> commitDetails = commitsExtract.projectCommitsExtract(path);
            commitDetailService.save(commitDetails);
            // Compute and store project details.
            ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(project);
            projectDetailService.save(projectDetail);

            // Start analyze project cooperation style.
            projectFilesLifeCircleService.extractAndSave(project, commitDetails);

            // Start analyze project code style
            String namespace = projectVO.getNamespace();
            long pId = projectVO.getId();
            projectCyclomaticComplexity.create(path, namespace, pId);
            projectLinesOfCode.create(path, namespace, pId);
            projectLogicDepth.create(path, namespace, pId);
            projectVariablesDeclaration.create(path, namespace, pId);
            projectParameters.create(path, namespace, pId);
            projectLineOfAnnotation.create(path, namespace, pId);
            projectEmptyMethod.create(path, namespace, pId);
            projectDuplicationCode.create(path, namespace, pId);

        }
        projectService.save(projects);
    }


    @RequiresPermissions("project:add_uncontrolled")
    @RequestMapping(value = Route.System.GetProjects, method = RequestMethod.POST)
    public JSONObject saveUncontrolledProjectsWithQueue(@RequestBody @NotNull List<ProjectVO> projectVOs) {
        JSONObject object = new JSONObject();
        JSONArray jArray = new JSONArray();
        for (ProjectVO projectVO : projectVOs)
            jArray.add(cloneSender.send(projectVO));
        object.put("SUCCESS_CODE", Constant.SUCCESS_CODE);
        object.put("SUCCESS_MSG", Constant.SUCCESS_MSG);
        object.put("RESULT_DATA", jArray);
        return object;
    }

    @GetMapping(Route.System.Reanalyze)
    @Deprecated
    public void reAnalyzeProject(@RequestParam("groupName") @NotNull String groupName,
                                 @RequestParam("projectName") @NotNull String projectName) {
        /**
         * Because the font end does not save project id, so this method is ugly:(
         */
        repositoryClone.clone("http://114.215.188.21/" + groupName + "/" + projectName + ".git");
        Project project = new Project();
        project.setName(projectName);
        project.setNamespace(groupName);
        String path = Route.CLONE_PATH + groupName + "/" + projectName;
        project.setFullName(path);
        List<CommitDetail> commitDetails = commitsExtract.projectCommitsExtract(path);
        ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(project);
        // Update project_detail information.
        projectDetailService.update(projectDetail);
        // Append new commit detail information.
        commitDetailService.append(commitDetails);
        // Update files' life circle of project.
        projectFilesLifeCircleService.extractAndSave(project, commitDetails);
        // Update code check results.
        projectCyclomaticComplexity.update(groupName, projectName);
        projectLogicDepth.update(groupName, projectName);
        projectLinesOfCode.update(groupName, projectName);
        projectParameters.update(groupName, projectName);
        projectVariablesDeclaration.update(groupName, projectName);
        projectDuplicationCode.update(groupName, projectName);
        projectLineOfAnnotation.update(groupName, projectName);
        projectEmptyMethod.update(groupName, projectName);
    }

    @RequestMapping(value = Route.System.AnalyzingProject, method = RequestMethod.GET)
    public JSONObject reAnalyzeProjectWithQueue(@RequestParam("groupName") @NotNull String groupName,
                                                @RequestParam("projectName") @NotNull String projectName) {
        JSONObject object = new JSONObject();
        ProjectVO projectVO = projectLogic.getProject(projectName, groupName);
        object.put("SUCCESS_CODE", Constant.SUCCESS_CODE);
        object.put("SUCCESS_MSG", Constant.SUCCESS_MSG);
        object.put("RESULT_DATA", cloneSender.send(projectVO));
        return object;
    }

    /**
     * Get certain period projects.
     *
     * @param start
     * @param end
     * @return
     */
    @RequestMapping(value = Route.System.CertainPeriodProjects, method = RequestMethod.GET)
    public List<ProjectVO> checkControlledProjectsBetweenPeriod(@RequestParam("start") @NotNull long start,
                                                                @RequestParam("end") @NotNull long end) {
        return projectLogic.getProjects(start, end);
    }

    public ProjectDetailVO selectProjectDetail(@RequestParam("namespace") @NotNull String namespace,
                                               @RequestParam("name") @NotNull String name) {
        return projectDetailLogic.getProjectDetail(namespace, name);
    }

    /**
     * Using the project id get specific project's details.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = Route.System.GetProjectDetailById, method = RequestMethod.GET)
    public ProjectDetailVO checkProjectDetail(@RequestParam("") @NotNull long id) {
        return projectDetailLogic.getProjectDetail(id);
    }

    /**
     * Get projects we last update between certain period.
     *
     * @param start
     * @param end
     * @return
     */
    @RequiresPermissions("group:list_controlled")
    @RequestMapping(value = Route.System.CertainPeriodGroups, method = RequestMethod.GET)
    @Deprecated
    public List<GroupVO> checkControlledGroupsBetweenPeriod(@RequestParam("start") @NotNull long start,
                                                            @RequestParam("end") @NotNull long end) {
        return groupLogic.getGroups(start, end);
    }

    @RequiresPermissions("group:list_controlled")
    @RequestMapping(value = Route.System.CertainPeriodGroupsWithScore, method = RequestMethod.GET)
    public JSONArray checkControlledGroupsWithScoreBetweenPeriod(@RequestParam("start") @NotNull long start,
                                                                 @RequestParam("end") @NotNull long end) {
        return groupLogic.getGroupsWithScore(start, end);
    }

    /**
     * According to controlled groups, get specific groups' uncontrolled projects.
     *
     * @param group
     * @return
     */
    @RequestMapping(value = Route.System.GetGroupProjects, method = RequestMethod.POST)
    public List<ProjectVO> listUncontrolledProjects(@RequestParam("groups") @NotNull GroupVO group) {
//		List<ProjectVO> projectVOs = new ArrayList<ProjectVO>(projectLogic.getUncontrolledProjects(group, Constant.token));
//		projectVOs.addAll(projectLogic.getUncontrolledProjects(group, Constant.token));
//		return projectVOs;
        return new ArrayList<ProjectVO>(projectLogic.getUncontrolledProjects(group, Constant.token));
    }

    /**
     * According to controlled group, get group's controlled projects.
     *
     * @param namespace
     * @return
     */
    @RequiresPermissions("group:detail")
    @RequestMapping(value = Route.System.GetGroupProjects, method = RequestMethod.GET)
    public List<ProjectDetailVO> listControlledProjects(@RequestParam("namespace") @NotNull String namespace) {
        return projectDetailLogic.getprojectDetails(namespace);
    }

    /**
     * According to controlled group, get the commits detail information of the group's projects.
     *
     * @param namespace
     * @return
     */
    @RequestMapping(value = Route.System.GetProjectsCommits, method = RequestMethod.GET)
    @Deprecated
    public Map<String, List<CommitDetail>> listProjectCommits(@RequestParam("namespace") @NotNull String namespace) {
        List<ProjectVO> projects = projectLogic.getProjects(namespace);
        List<String> names = new ArrayList<String>();
        for (ProjectVO p : projects)
            names.add(p.getName());
        Map<String, List<CommitDetail>> maps = new HashMap<String, List<CommitDetail>>();
        for (String name : names) {
            String fullname = namespace + "/" + name;
            if (maps.get(fullname) != null)
                continue;
            // url.substring(22,url.indexOf(".git"));
            Project project = projectService.getProjects(name, namespace);
            long pid = project.getId();
            String originName = project.getUrl().substring(22, project.getUrl().indexOf(".git"));
            maps.put(fullname + "#" + String.valueOf(pid), commitDetailService.getCommits(originName));
        }
        return maps;
    }
    @RequestMapping(value = Route.System.GetProjectsCommitsWithState, method = RequestMethod.GET)
    public JSONObject listProjectCommitWithState(@RequestParam("namespace") @NotNull String namespace) {
        JSONObject result = new JSONObject();
        result.put("repos", projectLogic.getProjectsCommitsDetail(namespace));
        result.put("state", projectLogic.getProjectsState(namespace));
        return CommonUtil.successJson(result);
    }
}
