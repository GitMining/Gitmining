package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLOALogic;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

@RestController
@RequestMapping(Route.LineOfAnnoation.LineOfAnnoation)
public class ProjectLOAController {
	@Autowired
	ProjectLOALogic projectLOALogic;
	@RequestMapping(value = Route.LineOfAnnoation.LOA, method = RequestMethod.GET)
	public List<LineOfAnnotateInSingleFileEntity> getProjectLOAInfo(@RequestParam(value = "id") Long id) {
		return projectLOALogic.getCheckResult(id);
	}

}
