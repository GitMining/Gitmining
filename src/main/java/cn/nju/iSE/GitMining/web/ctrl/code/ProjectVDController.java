package cn.nju.iSE.GitMining.web.ctrl.code;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectVDVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectVDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.ctrl.code.ProjectVDController.java
 *  
 *  2018年3月31日	下午12:36:00  
*/
@RestController
@RequestMapping(Route.VariablesDeclaration.VariablesDeclaration)
public class ProjectVDController {

	@Autowired
	ProjectVDLogic projectVDLogic;
	
	@RequestMapping(value = Route.VariablesDeclaration.VdOverview, method = RequestMethod.GET)
	public ProjectVDVO getProjectVDOverviewInfo(@PathParam(value = "id") Long id) {
		return projectVDLogic.getCheckResult(id);
	}
	
	@RequestMapping(value = Route.VariablesDeclaration.VdDetail, method = RequestMethod.GET)
	public ProjectVDVO getProjectVDInfo(@PathParam(value = "id") Long id) {
		return projectVDLogic.getCheckResult(id);
	}
}