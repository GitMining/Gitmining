package cn.nju.iSE.GitMining.web.ctrl.cooperation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import cn.nju.iSE.GitMining.service.cooperation.AuthorsCooperationService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.logic.system.ProjectLogic;
import iSESniper.cooperation.entity.Author;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.web.ctrl.cooperation.AuthorCooperationController.java
 * 
 *           2018年4月27日 下午2:58:59
 */
@RestController
@RequestMapping(Route.DeveloperCoop.DeveloperCooperation)
public class AuthorCooperationController {

	@Autowired
	private AuthorsCooperationService authorsCooperationService;

	@Autowired
	private CommitDetailService commitDetailService;

	@Autowired
	private ProjectLogic projectLogic;

	@RequiresPermissions("group:members")
	@RequestMapping(value = Route.DeveloperCoop.CommitDetails, method = RequestMethod.POST)
	public List<CommitDetail> showDeveloperCommits(@RequestBody @NotNull Author author) {
		return commitDetailService.getCommits(author);
	}

	@RequiresPermissions("group:members")
	@RequestMapping(value = Route.DeveloperCoop.Diversity, method = RequestMethod.GET)
	public Map<ProjectVO, List<Map<Author, Double>>> showDevelopesDiversity(@RequestParam("groupName") @NotNull String groupName) {
		List<ProjectVO> projects = projectLogic.getProjects(groupName);
		Map<ProjectVO, List<Map<Author, Double>>> diversities = new HashMap<>();
		for (ProjectVO project : projects) {
			List<CommitDetail> cDetails = commitDetailService.getCommits(groupName + "/" + project.getName());
			diversities.put(project, authorsCooperationService.getAuthorsImportance(cDetails));
		}
		return diversities;
	}

	@RequiresPermissions("group:members")
	@RequestMapping(value = Route.DeveloperCoop.Contribution, method = RequestMethod.GET)
	public Map<Author, Integer> getAuthorContribution(@RequestParam("groupName") @NotNull String groupName,
			@RequestParam("projectName") @NotNull String projectName) {
		return authorsCooperationService.getAuthorPie(groupName + "/" + projectName);
	}
}
