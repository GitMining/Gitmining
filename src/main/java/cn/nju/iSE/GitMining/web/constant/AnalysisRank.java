package cn.nju.iSE.GitMining.web.constant;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.constant.AnalysisRank.java
 *  
 *  2018年4月15日	下午4:14:34  
*/

public enum AnalysisRank {
	INFO, WARNING, AWFUL
}
