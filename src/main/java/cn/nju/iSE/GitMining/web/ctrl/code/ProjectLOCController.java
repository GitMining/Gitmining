package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.model.code.loc.JavaLOCEntity;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLOCLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.ctrl.ProjectLOCController.java
 *  
 *  2018年3月29日	上午10:13:45  
*/
@RestController
@RequestMapping(Route.LinesOfCode.LinesOfCode)
public class ProjectLOCController {

	@Autowired
	ProjectLOCLogic projectLOCLogic;
	
	@RequestMapping(value = Route.LinesOfCode.LocOverview, method = RequestMethod.GET)
	public List<JavaLOCEntity> getProjectLOCOverviewInfo(@RequestParam("id") Long id) {
		return projectLOCLogic.getCheckResult(id).getJavaLOCEntities();
	}
	
	@RequestMapping(value = Route.LinesOfCode.LocDetail, method = RequestMethod.GET)
	public List<JavaLOCEntity> getProjectLOCInfo(@RequestParam("id") Long id) {
		return projectLOCLogic.getCheckResult(id).getJavaLOCEntities();
	}
}