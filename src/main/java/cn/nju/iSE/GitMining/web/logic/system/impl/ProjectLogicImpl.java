package cn.nju.iSE.GitMining.web.logic.system.impl;

import java.util.ArrayList;
import java.util.List;

import cn.nju.iSE.GitMining.service.system.AnalyzingStateService;
import cn.nju.iSE.GitMining.service.system.CommitDetailService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.service.system.ProjectService;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.data.wrapper.system.GroupVOWrapper;
import cn.nju.iSE.GitMining.web.data.wrapper.system.ProjectVOWrapper;
import cn.nju.iSE.GitMining.web.logic.system.ProjectLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.system.impl.ProjectLogicImpl.java
 *  
 *  2018年4月24日	下午1:31:11  
*/
@Service
public class ProjectLogicImpl implements ProjectLogic {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private ProjectVOWrapper projectVOWrapper;
	
	@Autowired
	private GroupVOWrapper groupVOWrapper;

	@Autowired
	private CommitDetailService commitDetailService;

	@Autowired
	private AnalyzingStateService analyzingStateService;
	
	@Override
	public List<ProjectVO> getProjects() {
		List<Project> projects = projectService.getProjects();
		return projectVOWrapper.wrap(projects);
	}

	@Override
	public Page<ProjectVO> getProjects(Pageable pageable) {
		Page<Project> projects = projectService.getProjects(pageable);
		return projects.map(new Converter<Project, ProjectVO>() {
			@Override
			public ProjectVO convert(Project model) {
				return projectVOWrapper.wrap(model);
			}
		});
	}

	@Override
	public List<ProjectVO> getProjects(long start, long end) {
		List<Project> projects = projectService.getProjects(start, end);
		List<ProjectVO> projectVOs = new ArrayList<ProjectVO>();
		projects.forEach(project->{
			ProjectVO projectVO = projectVOWrapper.wrap(project);
			projectVOs.add(projectVO);
		});
		return projectVOs;
	}

	@Override
	public Page<ProjectVO> getProjects(long start, long end, Pageable pageable) {
		Page<Project> projects = projectService.getProjects(start, end, pageable);
		return projects.map(new Converter<Project, ProjectVO>() {
			@Override
			public ProjectVO convert(Project model) {
				return projectVOWrapper.wrap(model);
			}
		});
	}

	@Override
	public List<ProjectVO> getProjects(String namespace) {
		List<Project> projects = projectService.getProjects(namespace);
		List<ProjectVO> projectVOs = new ArrayList<ProjectVO>();
		projects.forEach(project->{
			ProjectVO projectVO = projectVOWrapper.wrap(project);
			projectVOs.add(projectVO);
		});
		return projectVOs;
	}

	@Override
	public ProjectVO getProject(long id) {
		Project project = projectService.getProject(id);
		return projectVOWrapper.wrap(project);
	}

	@Override
	public ProjectVO getProject(String name, String namespace) {
		Project project = projectService.getProjects(name, namespace);
		return projectVOWrapper.wrap(project);
	}

	@Override
	public List<ProjectVO> getUncontrolledProjects(GroupVO groupVO, String token) {
		Group group = groupVOWrapper.unwrap(groupVO);
		List<Project> projects = projectService.downloadProject(group, From.GitLab_SECIII, token);
		groupService.appendGroupProjects(group);
		List<ProjectVO> projectVOs = new ArrayList<ProjectVO>();
		projects.forEach(project->{
			ProjectVO projectVO = projectVOWrapper.wrap(project);
			projectVOs.add(projectVO);
		});
		return projectVOs;
	}

	@Override
	public void saveUncontrolledProjectsByVO(List<ProjectVO> projectVOs) {
		List<Project> projects = projectVOWrapper.unwrap(projectVOs);
		for(Project p: projects) 
			projectService.captureCommitIds(p);
		projectService.save(projects);
	}

	@Override
	public void saveUncontrolledProject(ProjectVO projectVO) {
		projectService.save(projectVOWrapper.unwrap(projectVO));
	}

	@Override
	public Project transferPOVO(ProjectVO projectVO) {
		return projectVOWrapper.unwrap(projectVO);
	}

	@Override
	public ProjectVO transferPOVO(Project project) {
		return projectVOWrapper.wrap(project);
	}

	/**
	 * This implement is not good enough.
	 * @param namespace group name.
	 * @return Json type data.
	 * {
	 *     pronject1#1:[commit_details]
	 *     ...
	 * }
	 */
	@Override
	public JSONObject getProjectsCommitsDetail(String namespace) {
		JSONObject result = new JSONObject();
		List<ProjectVO> projectVOS = getProjects(namespace);
		for (ProjectVO projectVO : projectVOS) {
			String url = projectVO.getUrl();
			String sendName = namespace + "/" + projectVO.getName() + "#" + projectVO.getId();
			String originName = url.substring(22, url.indexOf(".git"));
			result.put(sendName, commitDetailService.getCommits(originName));
		}
		return result;
	}

	@Override
	public JSONObject getProjectsState(String namespace) {
		JSONObject result = new JSONObject();
		List<ProjectVO> projectVOS = getProjects(namespace);
		for (ProjectVO projectVO : projectVOS) {
			String sendName = namespace + "/" + projectVO.getName() + "#" + projectVO.getId();
			result.put(sendName, analyzingStateService.getProjectState(projectVO.getId()));
		}
		return result;
	}

	@Override
	public void saveUncontrolledProjects(List<Project> projects) {
		projectService.save(projects);
	}

}
