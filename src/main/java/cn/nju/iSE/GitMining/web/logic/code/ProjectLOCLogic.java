package cn.nju.iSE.GitMining.web.logic.code;

import cn.nju.iSE.GitMining.web.data.code.ProjectLOCVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectLOCLogic.java
 *  
 *  2018年3月29日	上午10:15:27  
*/

public interface ProjectLOCLogic extends BaseLogic<ProjectLOCVO> {
}
