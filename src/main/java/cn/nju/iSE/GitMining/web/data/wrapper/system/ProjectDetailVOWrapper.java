package cn.nju.iSE.GitMining.web.data.wrapper.system;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.web.data.system.ProjectDetailVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.system.ProjectDetailVOWrapper.java
 *  
 *  2018年4月24日	下午6:59:12  
*/
@Service
public class ProjectDetailVOWrapper extends BaseWrappper<ProjectDetailVO, ProjectDetail> {

	@Override
	public ProjectDetailVO wrap(ProjectDetail model) {
		ProjectDetailVO projectDetailVO = new ProjectDetailVO();
		ProjectVO projectVO = new ProjectVO();
		projectVO.setId(model.getId());
		projectVO.setName(model.getName());
		projectVO.setNamespace(model.getNamespace());
		projectVO.setUrl(model.getUrl());
		projectVO.setCreateTime(model.getCreateTime());
		projectDetailVO.setProjectVO(projectVO);
		projectDetailVO.setAddedFiles(model.getAddedFiles());
		projectDetailVO.setAddedLines(model.getAddedLines());
		projectDetailVO.setCommitsCount(model.getCommitCount());
		projectDetailVO.setDeletedFiles(model.getDeletedFiles());
		projectDetailVO.setModifiedFiles(model.getModifiedFiles());
		projectDetailVO.setRemovedLines(model.getRemovedLines());
		return projectDetailVO;
	}

	public ProjectDetailVO wrap(ProjectVO projectVO) {
		ProjectDetailVO projectDetailVO = new ProjectDetailVO();
		projectDetailVO.setProjectVO(projectVO);
		return projectDetailVO;
	}
	
	@Override
	public ProjectDetail unwrap(ProjectDetailVO vo) {
		ProjectDetail projectDetail = new ProjectDetail();
		projectDetail.setAddedFiles(vo.getAddedFiles());
		projectDetail.setAddedLines(vo.getAddedLines());
		projectDetail.setCommitCount(vo.getCommitsCount());
		projectDetail.setDeletedFiles(vo.getDeletedFiles());
		projectDetail.setModifiedFiles(vo.getModifiedFiles());
		projectDetail.setId(vo.getProjectVO().getId());
		projectDetail.setNamespace(vo.getProjectVO().getNamespace());
		projectDetail.setName(vo.getProjectVO().getName());
		projectDetail.setRemovedLines(vo.getRemovedLines());
		projectDetail.setUrl(vo.getProjectVO().getUrl());
		projectDetail.setCreateTime(vo.getProjectVO().getCreateTime());
		return projectDetail;
	}

}
