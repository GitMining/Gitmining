package cn.nju.iSE.GitMining.web.logic.code;

import java.util.List;

import iSESniper.code.entity.cpd.CpdEntity;

public interface ProjectDCLogic{

	List<CpdEntity> getCheckResult(Long id);

}
