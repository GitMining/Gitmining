package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectCCLogic;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.ctrl.code.ProjectCCController.java
 * 
 *           2018年3月31日 下午12:15:50
 */
@RestController
@RequestMapping(Route.CyclomaticComplexity.CyclomaticComplexity)
public class ProjectCCController {

	@Autowired
	ProjectCCLogic projectCCLogic;

	@RequestMapping(value = Route.CyclomaticComplexity.CcOverview, method = RequestMethod.GET)
	public Map<String, AnalysisRank> getProjectCCOverViewInfo(@RequestParam(value = "id") Long id) {
		return projectCCLogic.getCheckResult(id).getRankedMethodCCs();
	}
	
	@RequestMapping(value = Route.CyclomaticComplexity.CcDetail, method = RequestMethod.GET)
	public Map<String, Integer> getProjectCCInfo(@RequestParam(value = "id") Long id) {
		return projectCCLogic.getCheckResult(id).getMethodCCs();
	}
}