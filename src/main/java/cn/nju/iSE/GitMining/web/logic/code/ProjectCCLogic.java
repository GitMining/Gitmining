package cn.nju.iSE.GitMining.web.logic.code;

import cn.nju.iSE.GitMining.web.data.code.ProjectCCVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectCCLogic.java
 *  
 *  2018年3月30日	上午9:41:56  
*/

public interface ProjectCCLogic extends BaseLogic<ProjectCCVO> {

}
