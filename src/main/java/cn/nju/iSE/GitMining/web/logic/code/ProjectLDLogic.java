package cn.nju.iSE.GitMining.web.logic.code;

import cn.nju.iSE.GitMining.web.data.code.ProjectLDVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectLDLogic.java
 *  
 *  2018年3月30日	上午10:36:59  
*/

public interface ProjectLDLogic extends BaseLogic<ProjectLDVO> {

}
