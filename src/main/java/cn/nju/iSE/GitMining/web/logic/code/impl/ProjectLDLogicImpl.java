package cn.nju.iSE.GitMining.web.logic.code.impl;

import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import cn.nju.iSE.GitMining.service.code.ProjectLogicDepth;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectLDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectLDVOWrapper;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLDLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.web.logic.impl.ProjectLDLogicImpl.java
 * <p>
 * 2018年3月30日	上午10:37:49
 */
@Service
public class ProjectLDLogicImpl implements ProjectLDLogic {

    @Autowired
    private ProjectLogicDepth projectLogicDepth;

    @Autowired
    private ProjectLDVOWrapper projectLDVOWrapper;

    @Override
    public ProjectLDVO getCheckResult(long id) {
        ProjectLD projectLD = projectLogicDepth.getProject(id);
        if (projectLD == null) {
            throw new HttpBadRequestException("Can not find project by id: " + id);
        }
        return projectLDVOWrapper.wrap(projectLD);
    }

    @Override
    public ProjectLDVO getCheckResult(String groupName, String projectName) {
        ProjectLD projectLD = projectLogicDepth.getProject(groupName, projectName);
        if (projectLD == null)
            throw new HttpBadRequestException("Can not find project by group name and project name: " + groupName + " and " + projectName);
        return projectLDVOWrapper.wrap(projectLD);
    }

    @Override
    public void startCheckByProjectName(String name, String groupName, long pId) {
        String pth = Route.CLONE_PATH + name;
        projectLogicDepth.create(pth, groupName, pId);
    }

}
