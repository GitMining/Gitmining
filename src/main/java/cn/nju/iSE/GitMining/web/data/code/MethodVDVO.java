package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.vab.VabDec;
import cn.nju.iSE.GitMining.model.code.vab.Variable;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.code.MethodVDVO.java
 *  
 *  2018年3月30日	下午9:11:37  
*/

public class MethodVDVO implements Serializable{

	private static final long serialVersionUID = 7158410744238190001L;

	private String methodName;
	
	private List<VabDec> variables;

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<VabDec> getVariables() {
		return variables;
	}

	public void setVariables(List<VabDec> variables) {
		this.variables = variables;
	}

	public MethodVDVO(String methodName, List<VabDec> variables) {
		super();
		this.methodName = methodName;
		this.variables = variables;
	}
	
	public MethodVDVO() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[Method] Method Name: ");
		stringBuffer.append(methodName);
		if(variables == null) {
			stringBuffer.append("Variables list is empty.");
			return stringBuffer.toString();
		}
		stringBuffer.append(", [Variables]: [");
		for(Variable v: variables) {
			stringBuffer.append(v.toString());
			stringBuffer.append("; ");
		}
		stringBuffer.append("]");
		return stringBuffer.toString();
	}	
}
