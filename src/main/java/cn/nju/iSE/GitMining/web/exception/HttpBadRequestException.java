package cn.nju.iSE.GitMining.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.exception.HttpBadRequestException.java
 *  
 *  2018年3月29日	上午10:53:52  
*/
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class HttpBadRequestException extends RuntimeException {
    
	private static final long serialVersionUID = 4418667850466566938L;

	public HttpBadRequestException(String message) {
        super(message);
    }
}
