package cn.nju.iSE.GitMining.web.logic.system;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.system.ProjectLogic.java
 *  
 *  2018年4月24日	下午12:51:12  
*/

public interface ProjectLogic {

	/**
	 * List all projects partial information.
	 * @return
	 */
	List<ProjectVO> getProjects();
	
	/**
	 * List certain number of projects partial information.
	 * @param pageable
	 * @return
	 */
	Page<ProjectVO> getProjects(Pageable pageable);
	
	/**
	 * List projects partial information in certain period.
	 * @param start
	 * @param end
	 * @return
	 */
	List<ProjectVO> getProjects(long start, long end);
	
	/**
	 * List certain number of projects partial information in certain period.
	 * @param start
	 * @param end
	 * @param pageable
	 * @return
	 */
	Page<ProjectVO> getProjects(long start, long end, Pageable pageable);

	/**
	 * Get projects with same name.
	 * @param namespace
	 * @return
	 */
	List<ProjectVO> getProjects(String namespace);
	
	/**
	 * Get certain project by id.
	 * @param id
	 * @return
	 */
	ProjectVO getProject(long id);
	
	/**
	 * Get certain project by name and namespace.
	 * @param name
	 * @param namespace
	 * @return
	 */
	ProjectVO getProject(String name, String namespace);
	
	/**
	 * Get uncontrolled projects belong to certain groupVO.
	 * @param groupVO
	 * @return
	 */
	List<ProjectVO> getUncontrolledProjects(GroupVO groupVO, String token);
	
	/**
	 * Convert projectvos to projects and store them into database.
	 * @param projectVOs
	 */
	void saveUncontrolledProjectsByVO(List<ProjectVO> projectVOs);
	
	void saveUncontrolledProjects(List<Project> projects);
	
	/**
	 * Convert projectvo to project and store them into database.
	 * @param projectVO
	 */
	void saveUncontrolledProject(ProjectVO projectVO);
	
	Project transferPOVO(ProjectVO projectVO);
	
	ProjectVO transferPOVO(Project project);

	JSONObject getProjectsCommitsDetail(String namespace);

	JSONObject getProjectsState(String namespace);
}
