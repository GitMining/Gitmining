package cn.nju.iSE.GitMining.web.logic.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import cn.nju.iSE.GitMining.service.code.ProjectVariablesDeclaration;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectVDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectVDVOWrapper;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectVDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.impl.ProjectVDLogicImpl.java
 *  
 *  2018年3月30日	下午11:03:47  
*/
@Service
public class ProjectVDLogicImpl implements ProjectVDLogic {

	@Autowired
	private ProjectVDVOWrapper projectVDVOWrapper;
	
	@Autowired
	private ProjectVariablesDeclaration projectVariablesDeclaration;
	
	@Override
	public ProjectVDVO getCheckResult(long id) {
		ProjectVabsDec projectVabsDec = projectVariablesDeclaration.getProject(id);
		if(projectVabsDec == null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectVDVOWrapper.wrap(projectVabsDec);
	}

	@Override
	public ProjectVDVO getCheckResult(String groupName, String projectName) {
		ProjectVabsDec projectVabsDec = projectVariablesDeclaration.getProject(groupName, projectName);
		if(projectVabsDec == null)
			throw new HttpBadRequestException("Can not find project by group name and project name: " + groupName + " and " + projectName);
		return projectVDVOWrapper.wrap(projectVabsDec);
	}

	@Override
	public void startCheckByProjectName(String name, String groupName, long pId) {
		String pth = Route.CLONE_PATH + name;
		projectVariablesDeclaration.create(pth, groupName, pId);
	}


}
