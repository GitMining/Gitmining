package cn.nju.iSE.GitMining.web.logic.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.service.code.ProjectLinesOfCode;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectLOCVO;
import cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectLOCVOWrapper;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLOCLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectLOCLogicImpl.java
 *  
 *  2018年3月29日	上午10:15:12  
*/
@Service
public class ProjectLOCLogicImpl implements ProjectLOCLogic {

	@Autowired
	private ProjectLinesOfCode projectLinesOfCode;
	
	@Autowired
	private ProjectLOCVOWrapper projectLocVOWrapper;
	
	@Override
	public ProjectLOCVO getCheckResult(long id) {
		ProjectLOC  projectLOC = projectLinesOfCode.getProject(id);
		if(projectLOC == null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectLocVOWrapper.wrap(projectLOC);
	}

	@Override
	public ProjectLOCVO getCheckResult(String groupName, String projectName) {
		ProjectLOC projectLOC = projectLinesOfCode.getProject(groupName, projectName);
		if(projectLOC == null)
			throw new HttpBadRequestException("Can not find project by group name and project name: " + groupName + " and " + projectName);
		return projectLocVOWrapper.wrap(projectLOC);
	}

	@Override
	public void startCheckByProjectName(String name, String grouName, long pId) {
		String pth = Route.CLONE_PATH + name;
		projectLinesOfCode.create(pth, grouName, pId);
	}

}
