package cn.nju.iSE.GitMining.web.logic.code.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;
import cn.nju.iSE.GitMining.service.code.ProjectDuplicationCode;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectDCLogic;
import iSESniper.code.entity.cpd.CpdEntity;
@Service
public class ProjectDCLogicImpl implements ProjectDCLogic {

	@Autowired
	ProjectDuplicationCode projectDuplicationCode;
	@Override
	public List<CpdEntity> getCheckResult(Long id) {
		ProjectDC projectDC = projectDuplicationCode.getProject(id);
		if(projectDC==null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectDC.getCpdEntities();
	}

}
