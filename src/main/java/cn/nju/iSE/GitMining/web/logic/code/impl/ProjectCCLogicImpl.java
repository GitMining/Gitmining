package cn.nju.iSE.GitMining.web.logic.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import cn.nju.iSE.GitMining.service.code.ProjectCyclomaticComplexity;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectCCVO;
import cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectCCVOWrapper;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectCCLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.impl.ProjectCCLogicImpl.java
 *  
 *  2018年3月30日	上午9:42:44  
*/
@Service
public class ProjectCCLogicImpl implements ProjectCCLogic {
	
	@Autowired
	private ProjectCyclomaticComplexity projectCyclomaticComplexity;
	
	@Autowired
	private ProjectCCVOWrapper projectCCVoWrapper;
	
	@Override
	public ProjectCCVO getCheckResult(long id) {
		ProjectCC projectCC = projectCyclomaticComplexity.getProject(id);
		if(projectCC == null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectCCVoWrapper.wrap(projectCC);
	}

	@Override
	public ProjectCCVO getCheckResult(String groupName, String projectName) {
		ProjectCC projectCC = projectCyclomaticComplexity.getProject(groupName, projectName);
		if(projectCC == null)
			throw new HttpBadRequestException("Can not find project by group name and project name: " + groupName + " and " + projectName);
		return projectCCVoWrapper.wrap(projectCC);
	}

	@Override
	public void startCheckByProjectName(String name, String groupName, long pId) {
		String pth = Route.CLONE_PATH + name;
		projectCyclomaticComplexity.create(pth, groupName, pId);
	}

}
