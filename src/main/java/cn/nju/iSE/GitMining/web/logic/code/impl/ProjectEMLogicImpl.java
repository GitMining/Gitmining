package cn.nju.iSE.GitMining.web.logic.code.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.em.ProjectEM;
import cn.nju.iSE.GitMining.service.code.ProjectEmptyMethod;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectEMLogic;
import iSESniper.code.entity.em.EmptyMethodEntity;
@Service
public class ProjectEMLogicImpl implements ProjectEMLogic {

	@Autowired
	ProjectEmptyMethod projectEmptyMethod;
	@Override
	public List<EmptyMethodEntity> getCheckResult(Long id) {
		ProjectEM projectEM = projectEmptyMethod.getProject(id);
		if(projectEM==null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectEM.getEmptyMethodEntities();
	}


}
