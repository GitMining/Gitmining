package cn.nju.iSE.GitMining.web.data.wrapper;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 * The main purpose of Wrappers is to decouple Model and View Object.
 * And we can transform some data items to what we want to display.
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper.java
 *  
 *  2018年3月29日	上午10:21:46  
*/

public abstract class BaseWrappper<VO, MODEL> {

	protected final Logger LOG = LoggerFactory.getLogger(getClass());
	
	public abstract VO wrap(MODEL model);
	
	public abstract MODEL unwrap(VO vo);
	
	protected AnalysisRank rankRslt(int result) {
		return AnalysisRank.INFO;
	}

	protected AnalysisRank rankRslt(long result) {
		return AnalysisRank.INFO;
	}
	
	public List<VO> wrap(List<MODEL> models){
		return models.stream().map(this::wrap).collect(Collectors.toList());
	}
	
	public List<MODEL> unwrap(List<VO> vos){
		return vos.stream().map(this::unwrap).collect(Collectors.toList());
	}
}
