package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.code.FileMethodsVDVO.java
 *  
 *  2018年3月30日	下午9:11:26  
*/

public class FileMethodsVDVO implements Serializable {

	private static final long serialVersionUID = -3282328110784698289L;

	private String fileName;
	
	private List<MethodVDVO> methodVDVOs;
	
	private Map<MethodVDVO, AnalysisRank> methodVDRank;

	public Map<MethodVDVO, AnalysisRank> getMethodVDRank() {
		return methodVDRank;
	}

	public void setMethodVDRank(Map<MethodVDVO, AnalysisRank> methodVDRank) {
		this.methodVDRank = methodVDRank;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<MethodVDVO> getMethodVDVOs() {
		return methodVDVOs;
	}

	public void setMethodVDVOs(List<MethodVDVO> methodVDVOs) {
		this.methodVDVOs = methodVDVOs;
	}

	public FileMethodsVDVO(String fileName, List<MethodVDVO> methodVDVOs) {
		super();
		this.fileName = fileName;
		this.methodVDVOs = methodVDVOs;
	}
	
	public FileMethodsVDVO(String fileName, List<MethodVDVO> methodVDVOs, Map<MethodVDVO, AnalysisRank> methodVDRank) {
		super();
		this.fileName = fileName;
		this.methodVDVOs = methodVDVOs;
		this.methodVDRank = methodVDRank;
	}

	public FileMethodsVDVO() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Files Profile] file: ");
		sBuffer.append(fileName);
		if(methodVDVOs == null) {
			sBuffer.append(" methods list is empty.");
		}
		sBuffer.append(", Methods:\n");
		for(MethodVDVO mvs: methodVDVOs) {
			sBuffer.append(mvs.toString());
		}
		return sBuffer.toString();
	}
}
