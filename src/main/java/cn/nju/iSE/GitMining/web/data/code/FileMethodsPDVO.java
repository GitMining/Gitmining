package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.FileMethodsPDVO.java
 *  
 *  2018年3月30日	上午11:18:51  
*/

public class FileMethodsPDVO implements Serializable {

	private static final long serialVersionUID = -7054505454646722688L;

	private String fileName;
	
	private List<MethodPDVO> methodPDVOs;
	
	private Map<MethodPDVO, AnalysisRank> methodPDRank;

	public Map<MethodPDVO, AnalysisRank> getMethodPDRank() {
		return methodPDRank;
	}

	public void setMethodPDRank(Map<MethodPDVO, AnalysisRank> methodPDRank) {
		this.methodPDRank = methodPDRank;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<MethodPDVO> getMethodPDVOs() {
		return methodPDVOs;
	}

	public void setMethodPDVOs(List<MethodPDVO> methodPDVOs) {
		this.methodPDVOs = methodPDVOs;
	}

	public FileMethodsPDVO(String fileName, List<MethodPDVO> methodPDVOs) {
		super();
		this.fileName = fileName;
		this.methodPDVOs = methodPDVOs;
	}
	
	public FileMethodsPDVO(String fileName, List<MethodPDVO> methodPDVOs, Map<MethodPDVO, AnalysisRank> methodPDRank) {
		super();
		this.fileName = fileName;
		this.methodPDVOs = methodPDVOs;
		this.methodPDRank = methodPDRank;
	}

	public FileMethodsPDVO() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Files Profile] file: ");
		sBuffer.append(fileName);
		if(methodPDVOs == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods:\n");
		for(MethodPDVO mps: methodPDVOs) {
			sBuffer.append(mps.toString());
		}
		
		return sBuffer.toString();
	}
}
