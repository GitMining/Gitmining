package cn.nju.iSE.GitMining.web.data.wrapper.system;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.system.ProjectVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.system.ProjectVOWrapper.java
 *  
 *  2018年4月23日	上午1:26:15  
*/

@Service
public class ProjectVOWrapper extends BaseWrappper<ProjectVO, Project> {

	@Override
	public ProjectVO wrap(Project model) {
		ProjectVO projectVO = new ProjectVO();
		projectVO.setId(model.getId());
		projectVO.setName(model.getName());
		projectVO.setNamespace(model.getNamespace());
		projectVO.setUrl(model.getUrl());
		projectVO.setCreateTime(model.getTime());
		return projectVO;
	}

	@Override
	public Project unwrap(ProjectVO vo) {
		Project project = new Project();
		project.setId(vo.getId());
		project.setNamespace(vo.getNamespace());
		project.setTime(vo.getCreateTime());
		project.setName(vo.getName());
		project.setUrl(vo.getUrl());
		String url = vo.getUrl();
		String fullName = Route.CLONE_PATH + url.substring(22,url.indexOf(".git"));
		project.setFullName(fullName);
		return project;
	}

}
