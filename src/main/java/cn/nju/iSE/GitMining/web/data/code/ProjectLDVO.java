package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.ProjectLDVO.java
 *  
 *  2018年3月30日	上午10:22:19  
*/

public class ProjectLDVO extends ProjectInfo implements Serializable {

	private static final long serialVersionUID = -6988542868512645332L;

	private Map<String, Integer> methodLDs;
	
	private Map<String, AnalysisRank> rankedMethodLDs;

	public Map<String, AnalysisRank> getRankedMethodLDs() {
		return rankedMethodLDs;
	}

	public void setRankedMethodLDs(Map<String, AnalysisRank> rankedMethodLDs) {
		this.rankedMethodLDs = rankedMethodLDs;
	}

	public Map<String, Integer> getMethodLDs() {
		return methodLDs;
	}

	public void setMethodLDs(Map<String, Integer> methodLDs) {
		this.methodLDs = methodLDs;
	}

	public ProjectLDVO(long id, String projectName, String groupName, long version, Map<String, Integer> methodLDs,
			Map<String, AnalysisRank> rankedMethodLDs) {
		super(id, projectName, groupName, version);
		this.methodLDs = methodLDs;
		this.rankedMethodLDs = rankedMethodLDs;
	}

	public ProjectLDVO(long id, String projectName, long version, Map<String, Integer> methodLDs) {
		super(id, projectName, version);
		this.methodLDs = methodLDs;
	}

	public ProjectLDVO() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Cyclomatic Complexity] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if(methodLDs == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods List: \n");
		Iterator<String> keys = methodLDs.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			sBuffer.append(key);
			sBuffer.append("  Logic Depth: ");
			sBuffer.append(methodLDs.get(key));
			sBuffer.append(" Analysis Rank: ");
			sBuffer.append(methodLDs.get(key));
			sBuffer.append("\n");
		}
		sBuffer.append("Methods Count: ");
		sBuffer.append(methodLDs.size());
		return sBuffer.toString();
	}
}
