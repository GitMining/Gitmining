package cn.nju.iSE.GitMining.web.logic.system.impl;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.model.system.GroupScore;
import cn.nju.iSE.GitMining.service.system.GroupScoreService;
import cn.nju.iSE.GitMining.service.system.GroupService;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.wrapper.system.GroupVOWrapper;
import cn.nju.iSE.GitMining.web.logic.system.GroupLogic;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.web.logic.system.impl.GroupLogicImpl.java
 * <p>
 * 2018年4月24日	上午10:24:11
 */
@Service
public class GroupLogicImpl implements GroupLogic {

    @Autowired
    private GroupVOWrapper groupVOWrapper;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupScoreService groupScoreService;

    @Override
    public List<GroupVO> getGroups() {
        List<Group> groups = groupService.getGroups();
        return groupVOWrapper.wrap(groups);
    }

    @Override
    public Page<GroupVO> getGroups(Pageable pageable) {
        Page<Group> groups = groupService.getGroups(pageable);
        return groups.map(new Converter<Group, GroupVO>() {
            @Override
            public GroupVO convert(Group custom) {
                return groupVOWrapper.wrap(custom);
            }
        });
    }

    @Override
    public List<GroupVO> getGroups(long start, long end) {
        List<Group> groups = groupService.getGroups(start, end);
        List<GroupVO> groupVOs = new ArrayList<GroupVO>();
        groups.forEach(group -> {
            GroupVO groupVO = groupVOWrapper.wrap(group);
            groupVOs.add(groupVO);
        });
        return groupVOs;
    }

    @Override
    public Page<GroupVO> getGroups(long start, long end, Pageable pageable) {
        Page<Group> groups = groupService.getGroups(start, end, pageable);
        return groups.map(new Converter<Group, GroupVO>() {
            @Override
            public GroupVO convert(Group custom) {
                return groupVOWrapper.wrap(custom);
            }
        });
    }

    @Override
    public List<GroupVO> getUncontrolledGroups(String baseAPI, String charset, String token) {
        List<Group> groups = new ArrayList<Group>();
        List<GroupVO> groupVOs = new ArrayList<GroupVO>();
        String url = baseAPI + "?private_token=" + token + "&per_page=10&page=";
        int i = 1;
        List<Group> temp;
        while (groups.size() < 14) {
            temp = groupService.downloadGroups(url + String.valueOf(i), charset);
            i++;
            if (temp == null || temp.isEmpty())
                continue;
            groups.addAll(temp);
        }
        groups.forEach(group -> {
            GroupVO groupVO = groupVOWrapper.wrap(group);
            groupVOs.add(groupVO);
        });
        ;
        return groupVOs;
    }

    @Override
    public void saveUncontrolledGroups(List<GroupVO> groupVOs) {
        List<Group> groups = new ArrayList<Group>();
        groupVOs.forEach(groupVO -> {
            Group group = groupVOWrapper.unwrap(groupVO);
            groups.add(group);
        });
        groupService.save(groups);
    }

    @Override
    public void saveUncontrolledGroup(GroupVO groupVO) {
        Group group = groupVOWrapper.unwrap(groupVO);
        groupService.save(group);
    }

    @Override
    public JSONArray getGroupsWithScore(long start, long end) {
        JSONArray groups = new JSONArray();
        List<GroupVO> groupVOS = getGroups(start, end);
        List<Long> ids = new ArrayList<>();
        groupVOS.forEach(vo -> ids.add(vo.getId()));
        List<GroupScore> groupScores = groupScoreService.getGroupScore(ids);
        JSONObject group;
        for(GroupScore groupScore: groupScores){
            group = new JSONObject();
            group.put("id", groupScore.getId());
            group.put("name", groupScore.getGroupName());
            group.put("platform", From.GitLab_SECIII);
            double score1 = groupScore.getPhaseI() >= 0 ? groupScore.getPhaseI() * groupScore.getWeightI() : 0;
            double score2 = groupScore.getPhaseII() >= 0 ? groupScore.getPhaseII() * groupScore.getWeightII() : 0;
            double score3 = groupScore.getPhaseIII() >= 0 ? groupScore.getPhaseIII() * groupScore.getWeightIII() : 0;
            group.put("score", (score1+score2+score3)/(groupScore.getWeightI() + groupScore.getPhaseII() + groupScore.getWeightIII()));
            GroupVO temp = new GroupVO();
            temp.setId(groupScore.getId());
            boolean flag = temp.equals(groupVOS.get(0));
            int idx = groupVOS.indexOf(temp);
            temp = idx >= 0? groupVOS.get(idx): null;
            List<Long> pIds = temp != null ? new ArrayList<>(temp.getProjectIds()) : new ArrayList<>();
            group.put("projectIds", pIds);
            groups.add(group);
        }
        return groups;
    }

}
