package cn.nju.iSE.GitMining.web.ctrl.code;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectPDVO;
import cn.nju.iSE.GitMining.web.logic.code.ProjectPDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.ctrl.code.ProjectPDController.java
 *  
 *  2018年3月31日	下午12:31:50  
*/
@RestController
@RequestMapping(Route.ParametersDeclaration.ParametersDeclaration)
public class ProjectPDController {

	@Autowired
	ProjectPDLogic pdLogic;
	
	@RequestMapping(value = Route.ParametersDeclaration.PdOverview, method = RequestMethod.GET)
	public ProjectPDVO getProjectPDOverviewInfo(@RequestParam(value = "id") Long id) {
		return pdLogic.getCheckResult(id);
	}
}