package cn.nju.iSE.GitMining.web.data.wrapper.code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsParams;
import cn.nju.iSE.GitMining.model.code.vab.MethodParams;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.FileMethodsPDVO;
import cn.nju.iSE.GitMining.web.data.code.MethodPDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.FileMethodPDVOWrapper.java
 *  
 *  2018年3月30日	下午7:31:21  
*/
@Service
public class FileMethodPDVOWrapper extends BaseWrappper<FileMethodsPDVO, FileMethodsParams> {

	@Autowired
	MethodPDVOWrapper methodPDVOWrapper;
	
	@Override
	public FileMethodsPDVO wrap(FileMethodsParams model) {
		FileMethodsPDVO fileMethodsPDVO = new FileMethodsPDVO();
		String pth = model.getPath().substring(Route.CLONE_PATH.length());
		fileMethodsPDVO.setFileName(pth);
		List<MethodPDVO> methodPDVOs = new ArrayList<MethodPDVO>();
		List<MethodParams> methodParams = model.getMethods();
		Map<MethodPDVO, AnalysisRank> methodPDRank = new HashMap<MethodPDVO, AnalysisRank>();
		for(MethodParams mp: methodParams) {
			MethodPDVO methodPDVO = methodPDVOWrapper.wrap(mp);
			methodPDVOs.add(methodPDVO);
			int paramsCount = methodPDVO.getParams().size();
			methodPDRank.put(methodPDVO, rankRslt(paramsCount));
		}
		fileMethodsPDVO.setMethodPDVOs(methodPDVOs);
		fileMethodsPDVO.setMethodPDRank(methodPDRank);
		return fileMethodsPDVO;
	}

	@Override
	public FileMethodsParams unwrap(FileMethodsPDVO vo) {
		FileMethodsParams fileMethodsParams = new FileMethodsParams();
		fileMethodsParams.setPath(Route.CLONE_PATH + vo.getFileName());
		List<MethodPDVO> methodPDVOs = vo.getMethodPDVOs();
		List<MethodParams> methodParams = new ArrayList<MethodParams>();
		for(MethodPDVO mp: methodPDVOs) {
			methodParams.add(methodPDVOWrapper.unwrap(mp));
		}
		fileMethodsParams.setMethods(methodParams);
		return null;
	}
	
	protected AnalysisRank rankRslt(int result) {
		if(result <= Constant.CodeQualityIndicator.PD_INFO_WARNING)
			return AnalysisRank.INFO;
		if(result <= Constant.CodeQualityIndicator.PD_WARNING_AWFUL)
			return AnalysisRank.WARNING;
		return AnalysisRank.AWFUL;
	}

}
