package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.data.ProjecrtPDVO.java
 * 
 *           2018年3月30日 上午11:13:01
 */

public class ProjectPDVO extends ProjectInfo implements Serializable {

	private static final long serialVersionUID = -4168955111542088160L;

	private List<FileMethodsPDVO> fileMethodsPDVOs;

	public Map<String, AnalysisRank> ranks;

	public Map<String, AnalysisRank> getRanks() {
		return ranks;
	}

	public void setRanks(Map<String, AnalysisRank> ranks) {
		this.ranks = ranks;
	}

	public List<FileMethodsPDVO> getFileMethodsPDVOs() {
		return fileMethodsPDVOs;
	}

	public void setFileMethodsPDVOs(List<FileMethodsPDVO> fileMethodsPDVOs) {
		this.fileMethodsPDVOs = fileMethodsPDVOs;
	}

	public ProjectPDVO(long id, String projectName, long version, List<FileMethodsPDVO> fileMethodsPDVOs,
			Map<String, AnalysisRank> ranks) {
		super(id, projectName, version);
		this.fileMethodsPDVOs = fileMethodsPDVOs;
		this.ranks = ranks;
	}

	public ProjectPDVO(long id, String projectName, String groupName, long version,
			List<FileMethodsPDVO> fileMethodsPDVOs, Map<String, AnalysisRank> ranks) {
		super(id, projectName, groupName, version);
		this.fileMethodsPDVOs = fileMethodsPDVOs;
		this.ranks = ranks;
	}

	public ProjectPDVO() {
		super();
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectParameters] ");
		sBuffer.append(super.toString());
		if (fileMethodsPDVOs == null) {
			sBuffer.append(" Files list is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(" Files: \n");
		for (FileMethodsPDVO fmp : fileMethodsPDVOs) {
			Iterator<Entry<MethodPDVO, AnalysisRank>> iterator = fmp.getMethodPDRank().entrySet().iterator();
			sBuffer.append(fmp.toString());
			sBuffer.append(" Analysis Rank: ");
			while (iterator.hasNext()) {
				Entry<MethodPDVO, AnalysisRank> entry = iterator.next();
				sBuffer.append("[");
				sBuffer.append(entry.getKey().getMethodName());
				sBuffer.append(": ");
				sBuffer.append(entry.getValue());
				sBuffer.append("] ");
			}
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}
}
