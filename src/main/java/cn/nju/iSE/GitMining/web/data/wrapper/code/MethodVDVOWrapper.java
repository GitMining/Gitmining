package cn.nju.iSE.GitMining.web.data.wrapper.code;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.MethodVabsDec;
import cn.nju.iSE.GitMining.web.data.code.MethodVDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.code.MethodVDVOWrapper.java
 *  
 *  2018年3月30日	下午9:33:31  
*/
@Service
public class MethodVDVOWrapper extends BaseWrappper<MethodVDVO, MethodVabsDec> {
	
	@Override
	public MethodVDVO wrap(MethodVabsDec model) {
		MethodVDVO methodVDVO = new MethodVDVO();
		methodVDVO.setMethodName(model.getMethodName());
		methodVDVO.setVariables(model.getVariables());
		return methodVDVO;
	}

	@Override
	public MethodVabsDec unwrap(MethodVDVO vo) {
		MethodVabsDec methodVabsDec = new MethodVabsDec();
		methodVabsDec.setMethodName(vo.getMethodName());
		methodVabsDec.setVariables(vo.getVariables());
		return methodVabsDec;
	}

}
