package cn.nju.iSE.GitMining.web.data.wrapper.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.web.data.system.GroupVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.system.GroupVOWrapper.java
 *  
 *  2018年4月23日	上午12:41:57  
*/
@Service
public class GroupVOWrapper extends BaseWrappper<GroupVO, Group> {

	@Override
	public GroupVO wrap(Group model) {
		GroupVO groupVO = new GroupVO();
		groupVO.setId(model.getId());
		groupVO.setName(model.getOwnerName());
		groupVO.setPlatform(model.getPlatform());
		groupVO.setProjectIds(model.getProjectIds());
		return groupVO;
	}

	@Override
	public Group unwrap(GroupVO vo) {
		Group group = new Group();
		group.setId(vo.getId());
		group.setOwnerId(vo.getId());
		group.setOwnerName(vo.getName());
		group.setPlatform(From.GitLab_SECIII);
		group.setTime(new Date().getTime());
		if(vo.getProjectIds() != null && vo.getProjectIds().size() != 0)
			group.setProjectIds(vo.getProjectIds());
		return group;
	}

	@Override
	public List<GroupVO> wrap(List<Group> models) {
		List<GroupVO> groupVOs = new ArrayList<GroupVO>();
		for(Group g: models)
			groupVOs.add(wrap(g));
		return groupVOs;
	}

	@Override
	public List<Group> unwrap(List<GroupVO> vos) {
		return super.unwrap(vos);
	}

}
