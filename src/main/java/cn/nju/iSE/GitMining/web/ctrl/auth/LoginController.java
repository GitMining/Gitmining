package cn.nju.iSE.GitMining.web.ctrl.auth;

import cn.nju.iSE.GitMining.common.util.CommonUtil;
import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.service.auth.LoginService;
import cn.nju.iSE.GitMining.service.auth.UserInfoService;
import cn.nju.iSE.GitMining.web.constant.Route;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(Route.Auth.Login)
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private UserInfoService userInfoService;

    @PostMapping(Route.Auth.Auth)
    public JSONObject authLogin(@RequestBody @NotNull JSONObject jsonObject) {
        CommonUtil.hasAllRequired(jsonObject, "username, password");
        return loginService.authLogin(jsonObject);
    }

    @PostMapping(Route.Auth.Logout)
    public JSONObject logout(@RequestBody @NotNull JSONObject jsonObject) {
        return loginService.logout(jsonObject);
    }

    @GetMapping(Route.Auth.SignIn)
    public JSONObject addUser(@RequestParam("username") @NotNull String username,
                              @RequestParam("password") @NotNull String password){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        jsonObject.put(Constant.AuthProperties.UserProperties.salt, "ordinary_user");
        jsonObject.put(Constant.AuthProperties.UserProperties.isAdmin, false);
        return userInfoService.addUser(jsonObject);
    }
}
