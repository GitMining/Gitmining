package cn.nju.iSE.GitMining.web.logic.code;

import java.util.List;

import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;

public interface ProjectLOALogic{

	List<LineOfAnnotateInSingleFileEntity> getCheckResult(Long id);

}
