package cn.nju.iSE.GitMining.web.data.system;

import java.io.Serializable;

/**
 *  This class aim to help analyst concern the overview of project, 
 *  and the ProjectDetailVO provides the detail of project, including the whole added or removed lines, 
 *  whole ADDED, DELETED or MODIFIED files count or others.
 *  
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.system.ProjectVO.java
 *  
 *  2018年4月23日	上午12:50:20  
*/

public class ProjectVO implements Serializable{

	private static final long serialVersionUID = 3965196366749516280L;

	private long id;
	
	private String name;
	
	private String namespace;
	
	private String url;
	
	private long createTime;

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ProjectVO(long id, String name, String namespace, String url, long createTime) {
		super();
		this.id = id;
		this.name = name;
		this.namespace = namespace;
		this.url = url;
		this.createTime  =createTime;
	}

	public ProjectVO() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectVO {id=" + id + ", name=" + name + ", namespace=" + namespace + ", url=" + url + "}";
	}
	
}
