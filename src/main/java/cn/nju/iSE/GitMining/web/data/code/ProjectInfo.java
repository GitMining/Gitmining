package cn.nju.iSE.GitMining.web.data.code;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.ProjectInfo.java
 *  
 *  2018年3月29日	上午10:31:16  
*/

public class ProjectInfo {

	private long id;
	private String projectName;
	private String groupName;
	private long version;
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public ProjectInfo(long id, String projectName, String groupName, long version) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.groupName = groupName;
		this.version = version;
	}
	public ProjectInfo(long id, String projectName, long version) {
		super();
		this.id = id;
		this.projectName = projectName;
		this.version = version;
	}
	public ProjectInfo() {
		super();
	}
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectInfo] id: ");
		sBuffer.append(id);
		sBuffer.append(" ProjectName: ");
		sBuffer.append(projectName);
		sBuffer.append(" VERSION: ");
		sBuffer.append(version);
		return sBuffer.toString();
	}
}
