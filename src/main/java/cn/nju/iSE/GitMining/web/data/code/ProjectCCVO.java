package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.ProjectCCVO.java
 *  
 *  2018年3月30日	上午9:15:34  
*/

public class ProjectCCVO extends ProjectInfo implements Serializable {
	
	private static final long serialVersionUID = 6501497768629349263L;

	private Map<String, Integer> methodCCs;
	
	private Map<String, AnalysisRank> rankedMethodCCs;

	public Map<String, AnalysisRank> getRankedMethodCCs() {
		return rankedMethodCCs;
	}

	public void setRankedMethodCCs(Map<String, AnalysisRank> rankedMethodCCs) {
		this.rankedMethodCCs = rankedMethodCCs;
	}

	public Map<String, Integer> getMethodCCs() {
		return methodCCs;
	}

	public void setMethodCCs(Map<String, Integer> methodCCs) {
		this.methodCCs = methodCCs;
	}

	public ProjectCCVO(long id, String projectName, long version, Map<String, Integer> methodCCs,
			Map<String, AnalysisRank> rankedMethodCCs) {
		super(id, projectName, version);
		this.methodCCs = methodCCs;
		this.rankedMethodCCs = rankedMethodCCs;
	}

	public ProjectCCVO(long id, String projectName, String groupName, long version, Map<String, Integer> methodCCs,
			Map<String, AnalysisRank> rankedMethodCCs) {
		super(id, projectName, groupName, version);
		this.methodCCs = methodCCs;
		this.rankedMethodCCs = rankedMethodCCs;
	}

	public ProjectCCVO() {
		super();
	}
	
	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[Project Cyclomatic Complexity] id: ");
		sBuffer.append(getId());
		sBuffer.append(", ProjectName: ");
		sBuffer.append(getProjectName());
		sBuffer.append(" VERSION: ");
		sBuffer.append(getVersion());
		if(methodCCs == null) {
			sBuffer.append(" Methods List is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(", Methods List: \n");
		Iterator<String> keys = methodCCs.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			sBuffer.append(key);
			sBuffer.append("  Cyclomatic Complexity: ");
			sBuffer.append(methodCCs.get(key));
			sBuffer.append(" Analysis Rank: ");
			sBuffer.append(rankedMethodCCs.get(key));
			sBuffer.append("\n");
		}
		sBuffer.append("Methods Count: ");
		sBuffer.append(methodCCs.size());
		return sBuffer.toString();
	}
}
