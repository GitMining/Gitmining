package cn.nju.iSE.GitMining.web.data.wrapper.code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsVabsDec;
import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.FileMethodsVDVO;
import cn.nju.iSE.GitMining.web.data.code.MethodVDVO;
import cn.nju.iSE.GitMining.web.data.code.ProjectVDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectVDVOWrapper.java
 *  
 *  2018年3月30日	下午9:49:39  
*/
@Service
public class ProjectVDVOWrapper extends BaseWrappper<ProjectVDVO, ProjectVabsDec> {

	@Autowired
	FileMethodsVDVOWrapper fileMethodsVDVOWrapper;
	
	@Override
	public ProjectVDVO wrap(ProjectVabsDec model) {
		ProjectVDVO projectVDVO = new ProjectVDVO();
		projectVDVO.setId(model.getId());
		projectVDVO.setProjectName(model.getProjectName());
		projectVDVO.setVersion(model.getVersion());
		List<FileMethodsVabsDec> fileMethodsVabsDecs = model.getFiles();
		List<FileMethodsVDVO> methodVDVOs = new ArrayList<FileMethodsVDVO>();
		Map<String, AnalysisRank> ranks = new HashMap<String, AnalysisRank>();
		for(FileMethodsVabsDec fmv: fileMethodsVabsDecs) {
			FileMethodsVDVO fileMethodsVDVO = fileMethodsVDVOWrapper.wrap(fmv);
			methodVDVOs.add(fileMethodsVDVO);
			String filename = fileMethodsVDVO.getFileName();
			Map<MethodVDVO, AnalysisRank> methodVDranks = fileMethodsVDVO.getMethodVDRank();
			Set<Entry<MethodVDVO, AnalysisRank>> entrys = methodVDranks.entrySet();
			Iterator<Entry<MethodVDVO, AnalysisRank>> iterator = entrys.iterator();
			for(Entry<MethodVDVO, AnalysisRank> entry: entrys)
				ranks.put(filename + ": " + entry.getKey().getMethodName(), entry.getValue());
		}
		projectVDVO.setRanks(ranks);
		projectVDVO.setGroupName(model.getGroupName());
		projectVDVO.setFileMethodsVDVOs(methodVDVOs);
		return projectVDVO;
	}

	@Override
	public ProjectVabsDec unwrap(ProjectVDVO vo) {
		ProjectVabsDec projectVabsDec = new ProjectVabsDec();
		projectVabsDec.setProjectName(vo.getProjectName());
		projectVabsDec.setprojectPath(Route.CLONE_PATH + vo.getProjectName());
		List<FileMethodsVabsDec> fileMethodsVabsDecs = new ArrayList<FileMethodsVabsDec>();
		List<FileMethodsVDVO> fileMethodsVDVOs = vo.getFileMethodsVDVOs();
		for(FileMethodsVDVO fmv: fileMethodsVDVOs) {
			fileMethodsVabsDecs.add(fileMethodsVDVOWrapper.unwrap(fmv));
		}
		projectVabsDec.setFiles(fileMethodsVabsDecs);
		return projectVabsDec;
	}

}
