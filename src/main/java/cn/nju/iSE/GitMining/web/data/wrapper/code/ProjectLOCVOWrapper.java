package cn.nju.iSE.GitMining.web.data.wrapper.code;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.loc.JavaLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.MethodLOCEntity;
import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.data.code.ProjectLOCVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.data.wrapper.LOCVOWrapper.java
 * 
 *           2018年3月29日 上午10:24:53
 */
@Service
public class ProjectLOCVOWrapper extends BaseWrappper<ProjectLOCVO, ProjectLOC> {

	@Override
	public ProjectLOCVO wrap(ProjectLOC model) {
		ProjectLOCVO projectLOCVO = new ProjectLOCVO();
		projectLOCVO.setId(model.getId());
		List<JavaLOCEntity> javas = model.getJavaLOCEntities();
		projectLOCVO.setJavaLOCEntities(javas);
		projectLOCVO.setNonJavaLOCEntities(model.getNonJavaLOCEntities());
		Map<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>> javaLocRanks = 
				new HashMap<JavaLOCEntity, Map<MethodLOCEntity, AnalysisRank>>();
		Map<String, AnalysisRank> ranks = new HashMap<String, AnalysisRank>();
		for (JavaLOCEntity jle : javas) {
			String filename = jle.getPth();
			List<MethodLOCEntity> methods = jle.getMethodList();
			Map<MethodLOCEntity, AnalysisRank> RankedMethods = new HashMap<MethodLOCEntity, AnalysisRank>();
			for (MethodLOCEntity mle : methods) {
				AnalysisRank rank = rankRslt(mle.getBlankLines());
				ranks.put(filename + ": " + mle.getMethodName(), rank);
				RankedMethods.put(mle, rank);
			}
			javaLocRanks.put(jle, RankedMethods);
		}
		projectLOCVO.setGroupName(model.getGroupName());
		projectLOCVO.setRanks(ranks);
		projectLOCVO.setJavaLocRanks(javaLocRanks);
		projectLOCVO.setProjectName(model.getProjectName());
		projectLOCVO.setVersion(model.getVersion());
		return projectLOCVO;
	}

	@Override
	public ProjectLOC unwrap(ProjectLOCVO vo) {
		ProjectLOC projectLOC = new ProjectLOC();
		projectLOC.setId(vo.getId());
		projectLOC.setProjectName(vo.getProjectName());
		return projectLOC;
	}

	@Override
	protected AnalysisRank rankRslt(long result) {
		if(result <= Constant.CodeQualityIndicator.LOC_INFO_WARNING)
			return  AnalysisRank.INFO;
		if(result <= Constant.CodeQualityIndicator.LOC_WARNING_AWFUL)
			return AnalysisRank.WARNING;
		return AnalysisRank.AWFUL;
	}
}
