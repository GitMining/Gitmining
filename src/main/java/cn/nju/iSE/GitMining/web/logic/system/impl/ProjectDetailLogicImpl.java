package cn.nju.iSE.GitMining.web.logic.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import cn.nju.iSE.GitMining.service.system.ProjectDetailService;
import cn.nju.iSE.GitMining.web.data.system.ProjectDetailVO;
import cn.nju.iSE.GitMining.web.data.wrapper.system.ProjectDetailVOWrapper;
import cn.nju.iSE.GitMining.web.logic.system.ProjectDetailLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.system.impl.ProjectDetailLogicImpl.java
 *  
 *  2018年4月24日	下午7:22:32  
*/
@Service
public class ProjectDetailLogicImpl implements ProjectDetailLogic {

	@Autowired
	private ProjectDetailService projectDetailService;
	
	@Autowired
	private ProjectDetailVOWrapper projectDetailVOWrapper;
	
	@Override
	public ProjectDetailVO projectCommitsExtract(Project project) {
		ProjectDetail projectDetail = projectDetailService.projectCommitsExtract(project);
		return projectDetailVOWrapper.wrap(projectDetail);
	}

	@Override
	public ProjectDetailVO getProjectDetail(long id) {
		ProjectDetail projectDetail = projectDetailService.getProjectDetail(id);
		return projectDetailVOWrapper.wrap(projectDetail);
	}

	@Override
	public ProjectDetailVO getProjectDetail(String namespace, String name) {
		ProjectDetail projectDetail = projectDetailService.getProjectDetail(namespace, name);
		return projectDetailVOWrapper.wrap(projectDetail);
	}

	@Override
	public List<ProjectDetailVO> getprojectDetails(String namespace) {
		List<ProjectDetail> projectDetails = projectDetailService.getprojectDetails(namespace);
		return projectDetailVOWrapper.wrap(projectDetails);
	}

}
