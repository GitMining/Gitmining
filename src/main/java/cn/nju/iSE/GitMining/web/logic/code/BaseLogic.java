package cn.nju.iSE.GitMining.web.logic.code;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.BaseLogic.java
 *  
 *  2018年3月29日	上午10:42:12  
*/

public interface BaseLogic<VO> {

	VO getCheckResult(long id);

	VO getCheckResult(String groupName, String projectName);

	void startCheckByProjectName(String name, String groupName, long pId);
}
