package cn.nju.iSE.GitMining.web.logic.code;

import cn.nju.iSE.GitMining.web.data.code.ProjectPDVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectPDLogic.java
 *  
 *  2018年3月30日	下午8:19:53  
*/

public interface ProjectPDLogic extends BaseLogic<ProjectPDVO> {

}
