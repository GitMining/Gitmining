package cn.nju.iSE.GitMining.web.data.wrapper.code;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.logic.MethodCC;
import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.data.code.ProjectCCVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.ProjectCCVOWrapper.java
 *  
 *  2018年3月30日	上午9:21:16  
*/
@Service
public class ProjectCCVOWrapper extends BaseWrappper<ProjectCCVO, ProjectCC> {

	@Override
	public ProjectCCVO wrap(ProjectCC model) {
		ProjectCCVO projectCCVO  = new ProjectCCVO();
		projectCCVO.setId(model.getId());
		String projectName = model.getProjectName();
		projectCCVO.setProjectName(projectName);
		projectCCVO.setVersion(model.getVersion());
		List<MethodCC> methodCCs = model.getMethodsCC();
		Map<String, Integer> mccs = new HashMap<String, Integer>();
		Map<String, AnalysisRank> rankedMethodCCs = new HashMap<String, AnalysisRank>();
		for(MethodCC mcc: methodCCs) {
			String temp = projectName.replaceAll("/", ".");
			temp = temp.substring(1);
			String pth = temp.length() <= mcc.getPath().length() ? mcc.getPath().substring(temp.length() + 1) : mcc.getPath();
			int result = mcc.getCcidx();
			mccs.put(pth, result);
			rankedMethodCCs.put(pth, rankRslt(result));
		}
		projectCCVO.setGroupName(model.getGroupName());
		projectCCVO.setMethodCCs(mccs);
		projectCCVO.setRankedMethodCCs(rankedMethodCCs);
		return projectCCVO;
	}

	@Override
	public ProjectCC unwrap(ProjectCCVO vo) {
		ProjectCC projectCC = new ProjectCC();
		projectCC.setId(vo.getId());
		projectCC.setProjectName(vo.getProjectName());
		return projectCC;
	}

	@Override
	protected AnalysisRank rankRslt(int result) {
		if(result <= Constant.CodeQualityIndicator.CC_INFO_WARNING)
			return AnalysisRank.INFO;
		else if(result <= Constant.CodeQualityIndicator.CC_WARNING_AWFUL)
			return  AnalysisRank.WARNING;
		return AnalysisRank.AWFUL;
	}
}
