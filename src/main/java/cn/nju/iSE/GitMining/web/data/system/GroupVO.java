package cn.nju.iSE.GitMining.web.data.system;

import cn.nju.iSE.GitMining.common.enumeration.From;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.web.data.system.GroupVO.java
 * <p>
 * 2018年4月23日	上午12:36:16
 */

public class GroupVO implements Serializable {

    private static final long serialVersionUID = -347717418279416263L;

    private long id;

    private String name;

    private List<Long> projectIds;

    private From platform;

    public GroupVO() {
        super();
    }

    public GroupVO(long id, String name, List<Long> projectIds, From platform) {
        super();
        this.id = id;
        this.name = name;
        this.projectIds = projectIds;
        this.platform = platform;
    }

    public From getPlatform() {
        return platform;
    }

    public void setPlatform(From platform) {
        this.platform = platform;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getProjectIds() {
        return projectIds;
    }

    public void setProjectIds(List<Long> projectIds) {
        this.projectIds = projectIds;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        GroupVO group = (GroupVO) obj;
        return group.getId() == id;
    }

    @Override
    public String toString() {
        StringBuffer sBuffer = new StringBuffer();
        sBuffer.append("[Group] id: ");
        sBuffer.append(getId());
        sBuffer.append(", Group Name: ");
        sBuffer.append(name);
        sBuffer.append(", Platform: ");
        sBuffer.append(getPlatform());
        if (getProjectIds() != null && getProjectIds().size() != 0) {
            sBuffer.append("\nProjects: ");
            sBuffer.append(getProjectIds());
        }
        return sBuffer.toString();
    }
}
