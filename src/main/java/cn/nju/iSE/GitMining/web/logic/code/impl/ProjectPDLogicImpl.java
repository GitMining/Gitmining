package cn.nju.iSE.GitMining.web.logic.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import cn.nju.iSE.GitMining.service.code.ProjectParameters;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.ProjectPDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.code.ProjectPDVOWrapper;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectPDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.impl.ProjectPDLogicImpl.java
 *  
 *  2018年3月30日	下午8:20:43  
*/
@Service
public class ProjectPDLogicImpl implements ProjectPDLogic {

	@Autowired
	ProjectPDVOWrapper projectPDVOWrapper;
	
	@Autowired
	ProjectParameters projectParameters;
	
	@Override
	public ProjectPDVO getCheckResult(long id) {
		ProjectParams projectParams = projectParameters.getProject(id);
		if(projectParams == null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectPDVOWrapper.wrap(projectParams);
	}

	@Override
	public ProjectPDVO getCheckResult(String groupName, String projectName) {
		ProjectParams projectParams = projectParameters.getProject(groupName, projectName);
		if(projectParams == null)
			throw new HttpBadRequestException("Can not find project by group name and project name: " + groupName + " and " + projectName);
		return projectPDVOWrapper.wrap(projectParams);
	}

	@Override
	public void startCheckByProjectName(String name, String groupName, long pId) {
		projectParameters.create(Route.CLONE_PATH + name, groupName, pId);
	}

}
