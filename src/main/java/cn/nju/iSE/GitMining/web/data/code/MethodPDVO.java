package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.List;

import cn.nju.iSE.GitMining.model.code.vab.Variable;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.MethodPDVO.java
 *  
 *  2018年3月30日	上午11:19:52  
*/

public class MethodPDVO implements Serializable {

	private static final long serialVersionUID = 5642754218535799934L;
	
	private String methodName;
	
	private List<Variable> params;

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<Variable> getParams() {
		return params;
	}

	public void setParams(List<Variable> params) {
		this.params = params;
	}

	public MethodPDVO(String methodName, List<Variable> params) {
		super();
		this.methodName = methodName;
		this.params = params;
	}
	
	public MethodPDVO() {
		super();
	}

	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[Method] Method Name: ");
		stringBuffer.append(methodName);
		if(params == null) {
			stringBuffer.append(" Parameters list is empty.");
			return stringBuffer.toString();
		}
		stringBuffer.append(", [Parameter]: [");
		for(Variable v: params) {
			stringBuffer.append(v.toString());
			stringBuffer.append("; ");
		}
		stringBuffer.append("]");
		return stringBuffer.toString();
	}
}
