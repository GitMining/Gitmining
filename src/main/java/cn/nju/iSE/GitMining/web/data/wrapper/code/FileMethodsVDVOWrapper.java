package cn.nju.iSE.GitMining.web.data.wrapper.code;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.nju.iSE.GitMining.common.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.FileMethodsVabsDec;
import cn.nju.iSE.GitMining.model.code.vab.MethodVabsDec;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.data.code.FileMethodsVDVO;
import cn.nju.iSE.GitMining.web.data.code.MethodVDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.code.FileMethodsVDVOWrapper.java
 *  
 *  2018年3月30日	下午9:40:52  
*/
@Service
public class FileMethodsVDVOWrapper extends BaseWrappper<FileMethodsVDVO, FileMethodsVabsDec> {

	@Autowired
	private MethodVDVOWrapper methodVDVOWrapper;
	
	@Override
	public FileMethodsVDVO wrap(FileMethodsVabsDec model) {
		FileMethodsVDVO fileMethodsVDVO = new FileMethodsVDVO();
		fileMethodsVDVO.setFileName(model.getPath().substring(Route.CLONE_PATH.length()));
		List<MethodVabsDec> methodVabsDecs = model.getMethods();
		List<MethodVDVO> methodVDVOs = new ArrayList<MethodVDVO>();
		Map<MethodVDVO, AnalysisRank> methods = new HashMap<MethodVDVO, AnalysisRank>();
		for(MethodVabsDec mv: methodVabsDecs) {
			MethodVDVO method = methodVDVOWrapper.wrap(mv);
			methodVDVOs.add(method);
			int result = method.getVariables().size();
			methods.put(method, rankRslt(result));
		}
		fileMethodsVDVO.setMethodVDRank(methods);
		fileMethodsVDVO.setMethodVDVOs(methodVDVOs);
		return fileMethodsVDVO;
	}

	@Override
	public FileMethodsVabsDec unwrap(FileMethodsVDVO vo) {
		FileMethodsVabsDec fileMethodsVabsDec = new FileMethodsVabsDec();
		fileMethodsVabsDec.setPath(Route.CLONE_PATH + vo.getFileName());
		List<MethodVDVO> methodVDVOs = vo.getMethodVDVOs();
		List<MethodVabsDec> methodVabsDecs = new ArrayList<MethodVabsDec>();
		for(MethodVDVO mv: methodVDVOs) {
			methodVabsDecs.add(methodVDVOWrapper.unwrap(mv));
		}
		fileMethodsVabsDec.setMethods(methodVabsDecs);
		return fileMethodsVabsDec;
	}
	
	protected AnalysisRank rankRslt(int result) {
		if(result < Constant.CodeQualityIndicator.VD_INFO_WARNING)
			return AnalysisRank.INFO;
		if(result < Constant.CodeQualityIndicator.VD_WARNING_AWFUL)
			return AnalysisRank.WARNING;
		return AnalysisRank.AWFUL;
	}

}
