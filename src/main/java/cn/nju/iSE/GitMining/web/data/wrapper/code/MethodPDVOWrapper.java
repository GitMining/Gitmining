package cn.nju.iSE.GitMining.web.data.wrapper.code;

import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.vab.MethodParams;
import cn.nju.iSE.GitMining.web.data.code.MethodPDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.MethodPDVOWrapper.java
 *  
 *  2018年3月30日	下午7:21:16  
*/
@Service
public class MethodPDVOWrapper extends BaseWrappper<MethodPDVO, MethodParams> {

	@Override
	public MethodPDVO wrap(MethodParams model) {
		MethodPDVO methodPDVO = new MethodPDVO();
		methodPDVO.setMethodName(model.getMethodName());
		methodPDVO.setParams(model.getParams());
		return methodPDVO;
	}

	@Override
	public MethodParams unwrap(MethodPDVO vo) {
		MethodParams methodParams = new MethodParams();
		methodParams.setMethodName(vo.getMethodName());
		methodParams.setParams(vo.getParams());
		return methodParams;
	}
	

}
