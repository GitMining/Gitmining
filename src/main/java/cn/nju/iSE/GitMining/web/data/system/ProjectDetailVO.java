package cn.nju.iSE.GitMining.web.data.system;

import java.io.Serializable;

/**
 *  This class aims to help analyst concern the details of project, 
 *  and provides the detail of project, including the whole added or removed lines, 
 *  whole ADDED, DELETED or MODIFIED files count or others:).
 *  
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.data.system.ProjectDetailVO.java
 *  
 *  2018年4月23日	上午1:37:26  
*/

public class ProjectDetailVO implements Serializable{

	private static final long serialVersionUID = 6616106111136674191L;

	private ProjectVO projectVO;
	
	private int commitsCount;
	
	private long addedLines;
	
	private long removedLines;
	
	private long addedFiles;
	
	private long deletedFiles;
	
	private long modifiedFiles;

	public ProjectVO getProjectVO() {
		return projectVO;
	}

	public void setProjectVO(ProjectVO projectVO) {
		this.projectVO = projectVO;
	}

	public int getCommitsCount() {
		return commitsCount;
	}

	public void setCommitsCount(int commitsCount) {
		this.commitsCount = commitsCount;
	}

	public long getAddedLines() {
		return addedLines;
	}

	public void setAddedLines(long addedLines) {
		this.addedLines = addedLines;
	}

	public long getRemovedLines() {
		return removedLines;
	}

	public void setRemovedLines(long removedLines) {
		this.removedLines = removedLines;
	}

	public long getAddedFiles() {
		return addedFiles;
	}

	public void setAddedFiles(long addedFiles) {
		this.addedFiles = addedFiles;
	}

	public long getDeletedFiles() {
		return deletedFiles;
	}

	public void setDeletedFiles(long deletedFiles) {
		this.deletedFiles = deletedFiles;
	}

	public long getModifiedFiles() {
		return modifiedFiles;
	}

	public void setModifiedFiles(long modifiedFiles) {
		this.modifiedFiles = modifiedFiles;
	}

	public ProjectDetailVO(ProjectVO projectVO, int commitsCount, long addedLines, long removedLines, long addedFiles,
			long deletedFiles, long modifiedFiles) {
		super();
		this.projectVO = projectVO;
		this.commitsCount = commitsCount;
		this.addedLines = addedLines;
		this.removedLines = removedLines;
		this.addedFiles = addedFiles;
		this.deletedFiles = deletedFiles;
		this.modifiedFiles = modifiedFiles;
	}

	public ProjectDetailVO() {
		super();
	}

	@Override
	public String toString() {
		return "ProjectDetailVO [projectVO=" + projectVO + ", commitsCount=" + commitsCount + ", addedLines="
				+ addedLines + ", removedLines=" + removedLines + ", addedFiles=" + addedFiles + ", deletedFiles="
				+ deletedFiles + ", modifiedFiles=" + modifiedFiles + "]";
	}
}
