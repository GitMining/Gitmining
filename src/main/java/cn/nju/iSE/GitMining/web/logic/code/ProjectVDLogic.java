package cn.nju.iSE.GitMining.web.logic.code;

import cn.nju.iSE.GitMining.web.data.code.ProjectVDVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.ProjectVDLogic.java
 *  
 *  2018年3月30日	下午11:02:35  
*/

public interface ProjectVDLogic extends BaseLogic<ProjectVDVO> {

}
