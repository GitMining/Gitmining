package cn.nju.iSE.GitMining.web.logic.code;

import java.util.List;

import iSESniper.code.entity.em.EmptyMethodEntity;

public interface ProjectEMLogic {

	List<EmptyMethodEntity> getCheckResult(Long id);

}
