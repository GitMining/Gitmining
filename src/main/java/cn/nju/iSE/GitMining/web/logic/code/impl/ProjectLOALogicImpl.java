package cn.nju.iSE.GitMining.web.logic.code.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;
import cn.nju.iSE.GitMining.service.code.ProjectLineOfAnnotation;
import cn.nju.iSE.GitMining.web.exception.HttpBadRequestException;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLOALogic;
import iSESniper.code.entity.loa.LineOfAnnotateInSingleFileEntity;
@Service
public class ProjectLOALogicImpl implements ProjectLOALogic {

	@Autowired
	ProjectLineOfAnnotation projectLineOfAnnotation;
	@Override
	public List<LineOfAnnotateInSingleFileEntity> getCheckResult(Long id) {
		ProjectLOA projectLOA = projectLineOfAnnotation.getProject(id);
		if(projectLOA==null) {
			throw new HttpBadRequestException("Can not find project by id: " + id);
		}
		return projectLOA.getLineOfAnnotateEntities();
	}


}
