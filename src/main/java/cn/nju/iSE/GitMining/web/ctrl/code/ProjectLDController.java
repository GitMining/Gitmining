package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectLDLogic;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.ctrl.code.ProjectLDController.java
 *  
 *  2018年3月31日	下午12:26:10  
*/
@RestController
@RequestMapping(Route.LogicDepth.LogicDepth)
public class ProjectLDController {

	@Autowired
	ProjectLDLogic projectLDLogic;
	
	@RequestMapping(value = Route.LogicDepth.LdOverview, method = RequestMethod.GET)
	public Map<String, AnalysisRank> getProjectLDInfo(@RequestParam(value = "id") Long id) {
		return projectLDLogic.getCheckResult(id).getRankedMethodLDs();
	}
	@RequestMapping(value = Route.LogicDepth.LdDetail, method = RequestMethod.GET)
	public Map<String, Integer> getProjectLDOverviewInfo(@RequestParam(value = "id") Long id) {
		return projectLDLogic.getCheckResult(id).getMethodLDs();
	}
}