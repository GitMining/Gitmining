package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectEMLogic;
import iSESniper.code.entity.em.EmptyMethodEntity;

@RestController
@RequestMapping(Route.EmptyCode.EmptyCode)
public class ProjectEMController {
	@Autowired
	ProjectEMLogic projectEMLogic;
	@RequestMapping(value = Route.EmptyCode.EM, method = RequestMethod.GET)
	public List<EmptyMethodEntity> getProjectEMInfo(@RequestParam(value = "id") Long id) {
		return projectEMLogic.getCheckResult(id);
	}
}
