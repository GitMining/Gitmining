package cn.nju.iSE.GitMining.web.constant;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.constant.Route.java
 * 
 *           2018年3月29日 上午11:13:51
 */

public final class Route {

	// dev CLONE_ROUTE
	 // public static final String CLONE_PATH = "D:/work/GitMining/Repos/samples/";
	// build CLONE_ROUTE
	public static final String CLONE_PATH = "/opt/GitMining/repositories/";

	public static final String HOME = "";

	public class LinesOfCode {
		public static final String LinesOfCode = "/Project/code/LOC";

		public static final String LocOverview = "/getLOCOverview";

		public static final String LocDetail = "/getLOC";

	}

	public class CyclomaticComplexity {

		public static final String CyclomaticComplexity = "/Project/code/CC";

		public static final String CcOverview = "/getCCOverview";

		public static final String CcDetail = "/getCC";
	}

	public class LogicDepth {

		public static final String LogicDepth = "/Project/code/LD";

		public static final String LdOverview = "/getLDOverview";

		public static final String LdDetail = "/getLD";
	}

	public class ParametersDeclaration {

		public static final String ParametersDeclaration = "/Project/code/PD";

		public static final String PdOverview = "/getPDOverview";

		public static final String PdDetail = "/getPD";
	}

	public class VariablesDeclaration {

		public static final String VariablesDeclaration = "/Project/code/VD";

		public static final String VdOverview = "/getVDOverview";

		public static final String VdDetail = "/getVD";
	}

	public class DuplicationCode {
		public static final String DuplicationCode = "/Project/code/DC";
		public static final String DC = "/getDC";
	}

	public class LineOfAnnoation {
		public static final String LineOfAnnoation = "/Project/code/LOA";
		public static final String LOA = "/getLOA";
	}

	public class EmptyCode {
		public static final String EmptyCode = "/Project/code/EM";
		public static final String EM = "/getEM";
	}

	public class System {

		public static final String LetsStart = "/LetsStart";

		public static final String AddNewGroups = "/newgroups";

		public static final String StartAnalyze = "/startanalyze";

		public static final String Reanalyze = "/reanalyze";

		public static final String CertainPeriodProjects = "/getprojectsbetween";

		public static final String CertainPeriodGroups = "/getgroupsbetween";

		public static final String CertainPeriodGroupsWithScore = "/getgroupscorebetween";

		public static final String GetProjectDetailById = "/getprojectdetailbyid";

		public static final String GetCertainProjectDetail = "selectprojectdetail";

		public static final String GetAllGroups = "/getallgroups";

		public static final String GetGroupProjects = "/getgroupprojects";

		public static final String GetProjectsCommits = "/getprojectscommits";

		public static final String GetProjectsCommitsWithState = "/getprojectscommitsstate";

		public static final String GetProjects = "/getprojects";

		public static final String AnalyzingProject = "/analyzeproject";
	}

	public class DeveloperFile {

		public static final String Developer2File = "/DeveloperFile";

		public static final String FileSibling = "/sibling";

		public static final String Contributor = "/contributor";

		public static final String FileGrowth = "/fgrowth";
	}

	public class DeveloperCoop {

		public static final String DeveloperCooperation = "/Cooperation";

		public static final String CommitDetails = "/commits";

		public static final String Diversity = "/diversity";

		public static final String Contribution = "/contribution";
	}

	public class Auth {
		public static final String Login = "/Login";

		public static final String Auth = "/auth";

		public static final String Logout = "/logout";

		public static final String SignIn = "/signin";
	}
}
