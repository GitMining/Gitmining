package cn.nju.iSE.GitMining.web.data.wrapper.code;

import cn.nju.iSE.GitMining.common.util.Constant;
import cn.nju.iSE.GitMining.model.code.logic.MethodLD;
import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import cn.nju.iSE.GitMining.web.constant.AnalysisRank;
import cn.nju.iSE.GitMining.web.data.code.ProjectLDVO;
import cn.nju.iSE.GitMining.web.data.wrapper.BaseWrappper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author :   Magister
 * @fileName :   cn.nju.iSE.GitMining.web.data.wrapper.ProjectLDVOWrapper.java
 * <p>
 * 2018年3月30日	上午10:27:04
 */
@Service
public class ProjectLDVOWrapper extends BaseWrappper<ProjectLDVO, ProjectLD> {

    @Override
    public ProjectLDVO wrap(ProjectLD model) {
        ProjectLDVO projectLDVO = new ProjectLDVO();
        projectLDVO.setId(model.getId());
        String projectName = model.getProjectName();
        projectLDVO.setProjectName(projectName);
        projectLDVO.setVersion(model.getVersion());
        List<MethodLD> methodLDs = model.getMethodLDs();
        Map<String, Integer> mlds = new HashMap<String, Integer>();
        Map<String, AnalysisRank> rankedMethodLDs = new HashMap<String, AnalysisRank>();
        for (MethodLD mld : methodLDs) {
            int result = mld.getLdIdx();
            String temp = projectName.replaceAll("/", ".");
            temp = temp.replaceAll("\\\\", ".");
            String tempPth = mld.getPath().replaceAll("/", ".");
            tempPth = tempPth.replaceAll("\\\\", ".");
            if (temp.length() + 1 >= tempPth.length())
                continue;
            String pth = tempPth.substring(temp.length() + 1);
            mlds.put(pth, result);
            rankedMethodLDs.put(pth, rankRslt(result));
        }
        projectLDVO.setGroupName(model.getGroupName());
        projectLDVO.setMethodLDs(mlds);
        projectLDVO.setRankedMethodLDs(rankedMethodLDs);
        return projectLDVO;
    }

    @Override
    public ProjectLD unwrap(ProjectLDVO vo) {
        ProjectLD projectLD = new ProjectLD();
        projectLD.setId(vo.getId());
        projectLD.setProjectName(vo.getProjectName());
        return projectLD;
    }

    @Override
    protected AnalysisRank rankRslt(int result) {
        if(result <= Constant.CodeQualityIndicator.LD_INFO_WARNING)
            return AnalysisRank.INFO;
        else if(result <= Constant.CodeQualityIndicator.LD_WARNING_AWFUL)
            return  AnalysisRank.WARNING;
        return AnalysisRank.AWFUL;
    }
}
