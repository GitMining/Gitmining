package cn.nju.iSE.GitMining.web.ctrl.code;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.web.constant.Route;
import cn.nju.iSE.GitMining.web.logic.code.ProjectDCLogic;
import iSESniper.code.entity.cpd.CpdEntity;

@RestController
@RequestMapping(Route.DuplicationCode.DuplicationCode)
public class ProjectDCController {
	@Autowired
	ProjectDCLogic projectDCLogic;

	@RequestMapping(value = Route.DuplicationCode.DC, method = RequestMethod.GET)
	public List<CpdEntity> getProjectEMInfo(@RequestParam(value = "id") Long id) {
		return projectDCLogic.getCheckResult(id);
	}

}
