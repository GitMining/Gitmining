package cn.nju.iSE.GitMining.web.ctrl.cooperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesSibling;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesLifeCircleService;
import cn.nju.iSE.GitMining.service.cooperation.ProjectFilesSiblingService;
import cn.nju.iSE.GitMining.web.constant.Route;
import iSESniper.cooperation.entity.Author;
import iSESniper.cooperation.entity.FileLifeCircle;

/**
 * @author : Magister
 * @fileName :
 *           cn.nju.iSE.GitMining.web.ctrl.cooperation.DeveloperAndFileController.java
 * 
 *           2018年4月26日 下午3:52:28
 */
@RestController
@RequestMapping(Route.DeveloperFile.Developer2File)
public class DeveloperAndFileController {

	@Autowired
	private ProjectFilesLifeCircleService projectFilesLifeCircleService;

	@Autowired
	private ProjectFilesSiblingService projectFilesSiblingService;

	/**
	 * According to certain project id, get the files sibling relationships.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = Route.DeveloperFile.FileSibling, method = RequestMethod.GET)
	public Map<Author, Map<String, List<String>>> getSibling(@RequestParam("id") @NotNull long id) {
		ProjectFilesLifeCircle projectFilesLifeCircle = projectFilesLifeCircleService.getProjectFilesLifeCircle(id);
		ProjectFilesSibling projectFilesSibling = projectFilesSiblingService.getSiblings(projectFilesLifeCircle);
		projectFilesSiblingService.getContributeRelation(projectFilesLifeCircle, projectFilesSibling);
		return projectFilesSiblingService.getTypesFiles(projectFilesSibling.getFilesSibling());
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = Route.DeveloperFile.Contributor, method = RequestMethod.GET)
	public List<Map> getAuthorRelation(@RequestParam("id") @NotNull long id,
			@RequestBody @NotNull Author author) {
		List<Map> relationships = new ArrayList<Map>();
		ProjectFilesLifeCircle projectFilesLifeCircle = projectFilesLifeCircleService.getProjectFilesLifeCircle(id);
		ProjectFilesSibling projectFilesSibling = projectFilesSiblingService.getSiblings(projectFilesLifeCircle);
		Map<Author, Integer> contributeRel = projectFilesSibling.getContributeRel().get(author);
		Map<Author, List<String>> fileRel = projectFilesSibling.getFilesSibling();
		Map<Author, Set<String>> authorFileTypes = new HashMap<Author, Set<String>>();
		
		Iterator<Author> iterator = fileRel.keySet().iterator();
		while (iterator.hasNext()) {
			Author fileOwner = iterator.next();
			Set<String> types = projectFilesSiblingService.getFileTypes(fileRel.get(fileOwner));
			authorFileTypes.put(fileOwner, types);
		}
		relationships.add(fileRel);
		relationships.add(contributeRel);
		relationships.add(authorFileTypes);
		return relationships;
	}

	@RequestMapping(value = Route.DeveloperFile.FileGrowth, method = RequestMethod.GET)
	public FileLifeCircle getFileLifeCircle(@RequestParam("id") @NotNull long id,
			@RequestParam("fileName") @NotNull String fileName) {
		List<FileLifeCircle> fileLifeCircles = projectFilesLifeCircleService.getProjectFilesLifeCircle(id)
				.getFileLifeCircles();
		for (FileLifeCircle fCircle : fileLifeCircles) {
			if (fCircle.getFileName().equals(fileName))
				return fCircle;
		}
		return null;
	}
}
