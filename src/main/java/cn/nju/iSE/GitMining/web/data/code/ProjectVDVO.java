package cn.nju.iSE.GitMining.web.data.code;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cn.nju.iSE.GitMining.web.constant.AnalysisRank;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.web.data.code.ProjectVDVO.java
 * 
 *           2018年3月30日 下午9:08:32
 */

public class ProjectVDVO extends ProjectInfo implements Serializable {

	private static final long serialVersionUID = 3950935711584038757L;

	private List<FileMethodsVDVO> fileMethodsVDVOs;

	private Map<String, AnalysisRank> ranks;

	public Map<String, AnalysisRank> getRanks() {
		return ranks;
	}

	public void setRanks(Map<String, AnalysisRank> ranks) {
		this.ranks = ranks;
	}

	public List<FileMethodsVDVO> getFileMethodsVDVOs() {
		return fileMethodsVDVOs;
	}

	public void setFileMethodsVDVOs(List<FileMethodsVDVO> fileMethodsVDVOs) {
		this.fileMethodsVDVOs = fileMethodsVDVOs;
	}

	public ProjectVDVO(long id, String projectName, long version, List<FileMethodsVDVO> fileMethodsVDVOs,
			Map<String, AnalysisRank> ranks) {
		super(id, projectName, version);
		this.fileMethodsVDVOs = fileMethodsVDVOs;
		this.ranks = ranks;
	}

	public ProjectVDVO(long id, String projectName, String groupName, long version,
			List<FileMethodsVDVO> fileMethodsVDVOs, Map<String, AnalysisRank> ranks) {
		super(id, projectName, groupName, version);
		this.fileMethodsVDVOs = fileMethodsVDVOs;
		this.ranks = ranks;
	}

	public ProjectVDVO() {
		super();
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append("[ProjectParameters] ");
		sBuffer.append(super.toString());
		if (fileMethodsVDVOs == null) {
			sBuffer.append(" Files list is empty.");
			return sBuffer.toString();
		}
		sBuffer.append(" Files: \n");
		for (FileMethodsVDVO fmv : fileMethodsVDVOs) {
			Iterator<Entry<MethodVDVO, AnalysisRank>> iterator = fmv.getMethodVDRank().entrySet().iterator();
			sBuffer.append(fmv.toString());
			sBuffer.append(" Analysis Rank: ");
			while (iterator.hasNext()) {
				Entry<MethodVDVO, AnalysisRank> entry = iterator.next();
				sBuffer.append("[");
				sBuffer.append(entry.getKey().getMethodName());
				sBuffer.append(": ");
				sBuffer.append(entry.getValue());
				sBuffer.append("] ");
			}
			sBuffer.append("\n");
		}
		return sBuffer.toString();
	}

}
