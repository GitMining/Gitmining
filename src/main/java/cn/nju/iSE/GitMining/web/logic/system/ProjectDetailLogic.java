package cn.nju.iSE.GitMining.web.logic.system;

import java.util.List;

import cn.nju.iSE.GitMining.model.system.Project;
import cn.nju.iSE.GitMining.web.data.system.ProjectDetailVO;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.web.logic.system.ProjectDetailLogic.java
 *  
 *  2018年4月24日	下午7:17:02  
*/

public interface ProjectDetailLogic {

	public ProjectDetailVO projectCommitsExtract(Project project);
	
	public ProjectDetailVO getProjectDetail(long id);
	
	public ProjectDetailVO getProjectDetail(String namespace, String name);
	
	public List<ProjectDetailVO> getprojectDetails(String namespace);
	
	
}
