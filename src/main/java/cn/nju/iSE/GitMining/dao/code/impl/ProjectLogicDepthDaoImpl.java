package cn.nju.iSE.GitMining.dao.code.impl;

import cn.nju.iSE.GitMining.dao.code.ProjectLogicDepthDao;
import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;


@Service
public class ProjectLogicDepthDaoImpl implements ProjectLogicDepthDao {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public void update(ProjectLD projectLD) {
        Query query = new Query(Criteria.where("id").is(projectLD.getId()));
        ProjectLD oldLd = mongoTemplate.findOne(query, ProjectLD.class);
        if(oldLd == null) {
            mongoTemplate.save(projectLD);
            return;
        }
        Update update = new Update().set("methodLDs", projectLD.getMethodLDs())
                .set("version", projectLD.getVersion())
                .set("projectPath", projectLD.getProjectPath());
        mongoTemplate.updateFirst(query, update, ProjectLD.class);
    }
}
