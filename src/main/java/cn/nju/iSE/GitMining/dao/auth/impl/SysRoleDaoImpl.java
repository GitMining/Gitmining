package cn.nju.iSE.GitMining.dao.auth.impl;

import cn.nju.iSE.GitMining.dao.auth.SysRoleDao;
import cn.nju.iSE.GitMining.model.auth.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysRoleDaoImpl implements SysRoleDao {
    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public void update(SysRole role) {
        Query query = new Query(Criteria.where("id").is(role.getId()));
        SysRole oldRole = mongoTemplate.findOne(query, SysRole.class);
        if(oldRole == null){
            mongoTemplate.save(role);
            return;
        }
        List<Integer> oldPids = oldRole.getPermissionIds();
        int size = oldPids.size();
        for(Integer id: role.getPermissionIds())
            if(!oldPids.contains(id))
                oldPids.add(id);
        if(oldPids.size() == size)
            return;
        role.setPermissionIds(oldPids);
        Update update = new Update().set("permissionIds", role.getPermissionIds());
        mongoTemplate.updateFirst(query, update, SysRole.class);
    }
}
