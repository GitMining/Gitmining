package cn.nju.iSE.GitMining.dao.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.code.ProjectDuplicationCodeDao;
import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;

@Service
public class ProjectDuplicationCodeDaoImpl implements ProjectDuplicationCodeDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void update(ProjectDC projectDC) {
		Query query = new Query(Criteria.where("id").is(projectDC.getId()));
		ProjectDC oldDc = mongoTemplate.findOne(query, ProjectDC.class);
		if (oldDc == null) {
			mongoTemplate.save(projectDC);
			return;
		} else {
			Update update = new Update().set("cpdEntities", projectDC.getCpdEntities())
					.set("version", projectDC.getVersion()).set("projectPath", projectDC.getProjectPath());
			mongoTemplate.updateFirst(query, update, ProjectDC.class);
		}

	}

}
