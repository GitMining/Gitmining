package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;

public interface ProjectCyclomaticComplexityDao {
    void update(ProjectCC projectCC);
}
