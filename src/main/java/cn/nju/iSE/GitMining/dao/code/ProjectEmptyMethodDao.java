package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.em.ProjectEM;

public interface ProjectEmptyMethodDao {
	public void update(ProjectEM projectEm);
}
