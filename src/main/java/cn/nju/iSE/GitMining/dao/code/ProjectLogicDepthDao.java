package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;

public interface ProjectLogicDepthDao {
    public void update(ProjectLD projectLD);
}
