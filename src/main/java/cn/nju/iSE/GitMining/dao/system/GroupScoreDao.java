package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.GroupScore;

import java.util.List;

public interface GroupScoreDao {

    /**
     * Update group's scores, that marked by teachers.
     * @param groupScore
     */
    public int updateScore(GroupScore groupScore);

    public int updateScore(List<GroupScore> groupScores);
}
