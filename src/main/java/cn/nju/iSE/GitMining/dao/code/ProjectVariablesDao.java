package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;

public interface ProjectVariablesDao {
    void update(ProjectVabsDec projectVabsDec);
}
