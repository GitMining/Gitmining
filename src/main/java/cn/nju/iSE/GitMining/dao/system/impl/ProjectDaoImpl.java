package cn.nju.iSE.GitMining.dao.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import cn.nju.iSE.GitMining.dao.system.ProjectDao;
import cn.nju.iSE.GitMining.model.system.Project;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.impl.ProjectDaoImpl.java
 *  
 *  2018年4月23日	下午1:01:35  
*/
@Repository
public class ProjectDaoImpl implements ProjectDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public void appendProjectCommits(Project project) {
		Query query = new Query(Criteria.where("id").is(project.getId()));
		Project oldProject = mongoTemplate.findOne(query, Project.class);
		if(oldProject == null) {
			mongoTemplate.save(project);
			return;
		}
		List<String> commitIds = oldProject.getCommitIds();
		removeDuplication(commitIds, project.getCommitIds());
		Update update = new Update().set("commitIds", commitIds);
		mongoTemplate.updateFirst(query, update, Project.class);
	}

	private void removeDuplication(List<String> commitIds, List<String> commitIds2) {
		for(String id: commitIds2)
			if(!commitIds.contains(id))
				commitIds.add(id);
	}

}
