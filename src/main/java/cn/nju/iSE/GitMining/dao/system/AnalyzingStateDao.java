package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.AnalyzingState;

public interface AnalyzingStateDao {
    public void updateState(AnalyzingState analyzingState);
}
