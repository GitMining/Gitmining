package cn.nju.iSE.GitMining.dao.cooperation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import cn.nju.iSE.GitMining.dao.cooperation.ProjectFilesLifeCircleDao;
import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.impl.ProjectFilesLifeCircleDaoImpl.java
 *  
 *  2018年4月25日	下午8:30:24  
*/
@Repository
public class ProjectFilesLifeCircleDaoImpl implements ProjectFilesLifeCircleDao {

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public void update(ProjectFilesLifeCircle projectFilesLifeCircle) {
		Query query = new Query(Criteria.where("id").is(projectFilesLifeCircle.getId()));
		ProjectFilesLifeCircle pCircle = mongoTemplate.findOne(query, ProjectFilesLifeCircle.class);
		if (pCircle == null) {
			mongoTemplate.save(projectFilesLifeCircle);
			return;
		}
		Update update = new Update().set("fileLifeCircles", projectFilesLifeCircle.getFileLifeCircles());
		mongoTemplate.updateFirst(query, update, ProjectFilesLifeCircle.class);
	}

}
