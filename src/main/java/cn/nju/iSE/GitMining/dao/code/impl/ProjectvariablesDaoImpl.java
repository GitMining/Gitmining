package cn.nju.iSE.GitMining.dao.code.impl;

import cn.nju.iSE.GitMining.dao.code.ProjectVariablesDao;
import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class ProjectvariablesDaoImpl implements ProjectVariablesDao {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public void update(ProjectVabsDec projectVabsDec) {
        Query query = new Query(Criteria.where("id").is(projectVabsDec.getId()));
        ProjectVabsDec oldVabs = mongoTemplate.findOne(query, ProjectVabsDec.class);
        if(oldVabs == null){
            mongoTemplate.save(projectVabsDec);
            return;
        }
        Update update = new Update().set("files", projectVabsDec.getFiles()).set("version", projectVabsDec.getVersion())
                .set("projectPath", projectVabsDec.getProjectPath());
        mongoTemplate.updateFirst(query, update, ProjectVabsDec.class);
    }
}
