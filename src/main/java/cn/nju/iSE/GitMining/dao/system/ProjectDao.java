package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.Project;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.ProjectDao.java
 *  
 *  2018年4月23日	下午1:00:36  
*/

public interface ProjectDao {

	public void appendProjectCommits(Project project);
}
