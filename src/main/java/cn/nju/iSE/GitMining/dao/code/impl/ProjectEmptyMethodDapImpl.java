package cn.nju.iSE.GitMining.dao.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.code.ProjectEmptyMethodDao;
import cn.nju.iSE.GitMining.model.code.em.ProjectEM;

@Service
public class ProjectEmptyMethodDapImpl implements ProjectEmptyMethodDao {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void update(ProjectEM projectEM) {
		Query query = new Query(Criteria.where("id").is(projectEM.getId()));
		ProjectEM oldEm = mongoTemplate.findOne(query, ProjectEM.class);
		if (oldEm == null) {
			mongoTemplate.save(projectEM);
			return;
		} else {
			Update update = new Update().set("emptyMethodEntities", projectEM.getEmptyMethodEntities())
					.set("version", projectEM.getVersion()).set("projectPath", projectEM.getProjectPath());
			mongoTemplate.updateFirst(query, update, ProjectEM.class);
		}

	}

}
