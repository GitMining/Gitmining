package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;

public interface ProjectLinesOfCodeDao {
    void update(ProjectLOC projectLOC);
}
