package cn.nju.iSE.GitMining.dao.auth;

import cn.nju.iSE.GitMining.model.auth.SysRole;

public interface SysRoleDao {
    public void update(SysRole role);
}
