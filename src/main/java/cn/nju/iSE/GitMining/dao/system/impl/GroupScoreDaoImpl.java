package cn.nju.iSE.GitMining.dao.system.impl;

import cn.nju.iSE.GitMining.dao.system.GroupScoreDao;
import cn.nju.iSE.GitMining.model.system.GroupScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupScoreDaoImpl implements GroupScoreDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int updateScore(GroupScore groupScore) {
        Query query = new Query(Criteria.where("id").is(groupScore.getId()));
        double finalScore = groupScore.getFinalScore() > 0 ? groupScore.getFinalScore() : 0;
        double score1 = groupScore.getPahseIScore() > 0 ? groupScore.getPahseIScore() : 0;
        double score2 = groupScore.getPhaseIIScore() > 0 ? groupScore.getPhaseIIScore() : 0;
        double score3 = groupScore.getPhaseIIIScore() > 0 ? groupScore.getPhaseIIIScore() : 0;
        Update update = new Update().set("finalScore", finalScore)
                .set("pahseIScore", score1)
                .set("phaseIIScore", score2)
                .set("phaseIIIScore", score3);
        return mongoTemplate.updateFirst(query, update, GroupScore.class).getN();
    }

    @Override
    public int updateScore(List<GroupScore> groupScores) {
        int n = 0;
        for(GroupScore groupScore: groupScores)
            n+=updateScore(groupScore);
        return n;
    }
}
