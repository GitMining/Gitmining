package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.ProjectScore;

public interface ProjectScoreDao {
    int update(ProjectScore projectScore);
}
