package cn.nju.iSE.GitMining.dao.system.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.system.ProjectDetailDao;
import cn.nju.iSE.GitMining.model.system.ProjectDetail;

/**
 * @author : Magister
 * @fileName : cn.nju.iSE.GitMining.dao.system.impl.ProjectDetailDaoImpl.java
 * 
 *           2018年5月1日 下午9:58:09
 */
@Service
public class ProjectDetailDaoImpl implements ProjectDetailDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public boolean update(ProjectDetail projectDetail) {
		Criteria.where("namespace").is(projectDetail.getNamespace());
		Query query = new Query(Criteria.where("name").is(projectDetail.getName()));
		ProjectDetail project = mongoTemplate.findOne(query, ProjectDetail.class);
		if (project == null) {
			if (projectDetail.getId() <= 0)
				return false;
			mongoTemplate.save(projectDetail);
		}
		Update update = new Update().set("addedFiles", projectDetail.getAddedFiles())
				.set("addedLines", projectDetail.getAddedLines()).set("commitCount", projectDetail.getCommitCount())
				.set("deletedFiles", projectDetail.getDeletedFiles())
				.set("modifiedFiles", projectDetail.getModifiedFiles())
				.set("removedLines", projectDetail.getRemovedLines());
		mongoTemplate.updateFirst(query, update, ProjectDetail.class);
		return true;
	}

}
