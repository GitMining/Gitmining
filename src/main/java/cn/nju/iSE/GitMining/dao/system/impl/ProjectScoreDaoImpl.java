package cn.nju.iSE.GitMining.dao.system.impl;

import cn.nju.iSE.GitMining.dao.system.ProjectScoreDao;
import cn.nju.iSE.GitMining.model.system.ProjectScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class ProjectScoreDaoImpl implements ProjectScoreDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int update(ProjectScore projectScore) {
        Query query = new Query(Criteria.where("id").is(projectScore.getId()));
        Update update = new Update().set("locScore", projectScore.getLocScore())
                .set("loaScore", projectScore.getLoaScore())
                .set("ccScore", projectScore.getCcScore())
                .set("dcScore", projectScore.getDcScore())
                .set("emScore", projectScore.getEmScore())
                .set("ldScore", projectScore.getLdScore())
                .set("pdScore", projectScore.getPdScore())
                .set("vdScore", projectScore.getVdScore());
        return mongoTemplate.updateFirst(query, update, ProjectScore.class).getN();
    }
}
