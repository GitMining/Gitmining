package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;

public interface ProjectParamsDao {
    void update(ProjectParams projectParams);
}
