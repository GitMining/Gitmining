package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.ProjectDetail;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.ProjectDetailDao.java
 *  
 *  2018年5月1日	下午9:56:23  
*/

public interface ProjectDetailDao {
	/**
	 * Update projectDetail information by searching for group_name and project_name.
	 * If it does not exist in database, the method would add it into MongoDB.
	 * @param projectDetail
	 * @return
	 */
	public boolean update(ProjectDetail projectDetail);
}
