package cn.nju.iSE.GitMining.dao.system.impl;

import cn.nju.iSE.GitMining.dao.system.AnalyzingStateDao;
import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import cn.nju.iSE.GitMining.mongo.system.AnalyzingStateMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class AnalyzingStateDaoImpl implements AnalyzingStateDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AnalyzingStateMongoRepository analyzingStateMongoRepository;

    @Override
    public void updateState(AnalyzingState analyzingState) {
        Query query = new Query(Criteria.where("id").is(analyzingState.getId()));
        Update update = new Update().set("state", analyzingState.getState()).set("lastUpdate", analyzingState.getLastUpdate());
        mongoTemplate.updateFirst(query, update, AnalyzingState.class);
    }
}
