package cn.nju.iSE.GitMining.dao.code.impl;

import cn.nju.iSE.GitMining.dao.code.ProjectParamsDao;
import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class ProejctParamsDaoImpl implements ProjectParamsDao {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public void update(ProjectParams projectParams) {
        Query query = new Query(Criteria.where("id").is(projectParams.getId()));
        ProjectParams oldParams = mongoTemplate.findOne(query, ProjectParams.class);
        if(oldParams == null){
            mongoTemplate.save(projectParams);
            return;
        }
        Update update = new Update().set("files", projectParams.getFiles()).set("version", projectParams.getVersion())
                .set("projectPath", projectParams.getProjectPath());
        mongoTemplate.updateFirst(query, update, ProjectParams.class);
    }
}
