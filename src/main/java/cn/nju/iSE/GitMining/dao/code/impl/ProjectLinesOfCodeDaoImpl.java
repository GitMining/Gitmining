package cn.nju.iSE.GitMining.dao.code.impl;

import cn.nju.iSE.GitMining.dao.code.ProjectLinesOfCodeDao;
import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;


@Service
public class ProjectLinesOfCodeDaoImpl implements ProjectLinesOfCodeDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void update(ProjectLOC projectLOC) {
        Query query = new Query(Criteria.where("id").is(projectLOC.getId()));
        ProjectLOC oldLoc = mongoTemplate.findOne(query, ProjectLOC.class);
        if(oldLoc == null){
            mongoTemplate.save(projectLOC);
            return;
        }
        Update update = new Update().set("nonJavaLOCEntities", projectLOC.getNonJavaLOCEntities())
                .set("javaLOCEntities", projectLOC.getNonJavaLOCEntities())
                .set("version", projectLOC.getVersion())
                .set("projectPath", projectLOC.getProjectPath());
        mongoTemplate.updateFirst(query, update, ProjectLOC.class);
    }
}
