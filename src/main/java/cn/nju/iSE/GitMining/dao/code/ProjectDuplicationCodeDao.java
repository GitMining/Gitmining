package cn.nju.iSE.GitMining.dao.code;

import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;

public interface ProjectDuplicationCodeDao {
	 public void update(ProjectDC projectDC);
}
