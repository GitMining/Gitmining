package cn.nju.iSE.GitMining.dao.code.impl;

import cn.nju.iSE.GitMining.dao.code.ProjectCyclomaticComplexityDao;
import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;


@Service
public class ProjectCyclomaticComplexityDaoImpl implements ProjectCyclomaticComplexityDao {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void update(ProjectCC projectCC) {
        Query query = new Query(Criteria.where("id").is(projectCC.getId()));
        ProjectCC oldCC = mongoTemplate.findOne(query, ProjectCC.class);
        if(oldCC == null){
            mongoTemplate.save(projectCC);
            return ;
        }
        Update update = new Update().set("version", projectCC.getVersion()).set("methodsCC", projectCC.getMethodsCC())
                .set("projectPath", projectCC.getProjectPath());
        mongoTemplate.updateFirst(query, update, ProjectCC.class);
    }
}
