package cn.nju.iSE.GitMining.dao.system;

import cn.nju.iSE.GitMining.model.system.Group;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.GroupDao.java
 *  
 *  2018年4月22日	上午11:17:00  
*/

public interface GroupDao {
	
	public void appendGroupProjects(Group group);
}
