package cn.nju.iSE.GitMining.dao.code.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.code.ProjectLineOfAnnoationDao;
import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;

@Service
public class ProjectLineOfAnnoationDaoImpl implements ProjectLineOfAnnoationDao {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void update(ProjectLOA projectLOA) {
		Query query = new Query(Criteria.where("id").is(projectLOA.getId()));
		ProjectLOA oldLoa = mongoTemplate.findOne(query, ProjectLOA.class);
		if (oldLoa == null) {
			mongoTemplate.save(projectLOA);
			return;
		} else {
			Update update = new Update().set("lineOfAnnotateEntities", projectLOA.getLineOfAnnotateEntities())
					.set("version", projectLOA.getVersion()).set("projectPath", projectLOA.getProjectPath());
			mongoTemplate.updateFirst(query, update, ProjectLOA.class);
		}

	}

}
