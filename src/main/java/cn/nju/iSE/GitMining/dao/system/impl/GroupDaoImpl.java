package cn.nju.iSE.GitMining.dao.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import cn.nju.iSE.GitMining.dao.system.GroupDao;
import cn.nju.iSE.GitMining.model.system.Group;
import cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.system.impl.GroupDaoImpl.java
 *  
 *  2018年4月22日	下午4:25:51  
*/
@Service
public class GroupDaoImpl implements GroupDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private GroupMongoRepository groupMongoRepository;

	@Override
	public void appendGroupProjects(Group group) {
		Query query = new Query(Criteria.where("id").is(group.getId()));
		Group oldGroup = mongoTemplate.findOne(query, Group.class);
		if(oldGroup == null) {
			groupMongoRepository.save(group);
			return;
		}
		List<Long> projectIds = oldGroup.getProjectIds();
		removeDuplication(projectIds, group.getProjectIds());
		Update update = new Update().set("projectIds", projectIds).set("time", group.getTime());
		mongoTemplate.updateFirst(query, update, Group.class);
	}

	private void removeDuplication(List<Long> projectIds, List<Long> projectIds2) {
		for(Long l: projectIds2) {
			if(!projectIds.contains(l))
				projectIds.add(l);
		}
	}

}
