package cn.nju.iSE.GitMining.dao.cooperation;

import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.dao.cooperation.ProjectFilesLifeCircleDao.java
 *  
 *  2018年4月25日	下午8:29:10  
*/

public interface ProjectFilesLifeCircleDao {

	public void update(ProjectFilesLifeCircle projectFilesLifeCircle);
}
