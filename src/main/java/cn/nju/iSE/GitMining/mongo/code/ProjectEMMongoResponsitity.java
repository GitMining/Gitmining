package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.em.ProjectEM;

public interface ProjectEMMongoResponsitity extends MongoRepository<ProjectEM, Long>{
	List<ProjectEM> findByProjectName(String projectName);
	List<ProjectEM> findByProjectPath(String path);
	ProjectEM findByGroupNameAndProjectName(String groupName, String projectName);
	ProjectEM findById(Long id);
	long countByProjectPath(String path);
}
