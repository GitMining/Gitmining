package cn.nju.iSE.GitMining.mongo.system;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.ProjectOverview;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.system.ProjectOverviewMongoRepository.java
 *  
 *  2018年3月29日	上午10:00:48  
*/
public interface ProjectOverviewMongoRepository extends MongoRepository<ProjectOverview, Long> {
	
	List<ProjectOverview> findByProjectName(String prjtName);
	ProjectOverview findById(Long id);
}
