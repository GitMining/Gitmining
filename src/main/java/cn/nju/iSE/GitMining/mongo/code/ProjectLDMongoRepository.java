package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.logic.ProjectLD;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.code.ProjectLDMongoRepository.java
 *  
 *  2018年3月18日	下午1:14:43  
*/
public interface ProjectLDMongoRepository extends MongoRepository<ProjectLD, Long> {
	List<ProjectLD> findByProjectName(String prjtName);
	List<ProjectLD> findByProjectPath(String path);
	ProjectLD findByGroupNameAndProjectName(String groupName, String projectName);
	ProjectLD findById(Long id);
	long countByProjectPath(String path);
}
