package cn.nju.iSE.GitMining.mongo.system;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.system.ProjectDetail;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.system.ProjectDetailMongoRepository.java
 *  
 *  2018年4月23日	下午3:46:15  
*/
public interface ProjectDetailMongoRepository extends MongoRepository<ProjectDetail, Long> {

	ProjectDetail findById(long id);

	List<ProjectDetail> findByNamespace(String namespace);

	ProjectDetail findByNamespaceAndName(String namespace, String name);
}
