package cn.nju.iSE.GitMining.mongo.auth;

import cn.nju.iSE.GitMining.model.auth.SysPermission;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SysPermissionMongoRepository extends MongoRepository<SysPermission, Integer> {
    SysPermission findById(int id);
}
