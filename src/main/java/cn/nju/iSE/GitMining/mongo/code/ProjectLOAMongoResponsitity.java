package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.loa.ProjectLOA;

public interface ProjectLOAMongoResponsitity extends MongoRepository<ProjectLOA, Long>{
	List<ProjectLOA> findByProjectName(String projectName);
	List<ProjectLOA> findByProjectPath(String path);
	ProjectLOA findByGroupNameAndProjectName(String groupName, String projectName);
	ProjectLOA findById(Long id);
	long countByProjectPath(String path);
}
