package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.dc.ProjectDC;

public interface ProjectDCMongoRepositity extends MongoRepository<ProjectDC, Long> {
	List<ProjectDC> findByProjectName(String projectName);
	List<ProjectDC> findByProjectPath(String path);
	ProjectDC findByGroupNameAndProjectName(String groupName, String projectName);
	ProjectDC findById(Long id);
	long countByProjectPath(String path);
}
