package cn.nju.iSE.GitMining.mongo.cooperation;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.cooperation.ProjectFilesLifeCircle;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.cooperation.ProjectFilesLCMongoRepository.java
 *  
 *  2018年4月25日	下午7:11:23  
*/
public interface ProjectFilesLCMongoRepository extends MongoRepository<ProjectFilesLifeCircle, Long> {

	ProjectFilesLifeCircle findById(long id);
	
	ProjectFilesLifeCircle findByProjectNameAndGroupName(String projectName, String groupName);

}
