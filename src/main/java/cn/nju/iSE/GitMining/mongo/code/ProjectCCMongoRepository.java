package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.logic.ProjectCC;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.code.ProjectCCMongoRepository.java
 *  
 *  2018年3月18日	上午9:58:56  
*/
public interface ProjectCCMongoRepository extends MongoRepository<ProjectCC, Long> {

	ProjectCC findById(long id);
	
	List<ProjectCC> findByProjectName(String name);
	ProjectCC findByGroupNameAndProjectName(String groupName, String projectName);
	List<ProjectCC> findByProjectPath(String path);
	long countByProjectPath(String path);
}
