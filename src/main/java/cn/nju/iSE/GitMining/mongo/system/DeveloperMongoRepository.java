package cn.nju.iSE.GitMining.mongo.system;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Developer;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.OwnerMongoRepository.java
 *  
 *  2018年4月21日	下午3:41:41  
*/
public interface DeveloperMongoRepository extends MongoRepository<Developer, Long> {

	Developer findById(long id);
	
	Developer findByIdAndPlatform(long id, From paltform);
	
	Developer findByOwnerName(String ownerName);

	List<Developer> findAllByTimeBetween(long start, long end);
	
	List<Developer> findAllByTimeBetweenAndPlatform(long start, long end, From platform);

	List<Developer> findAllByPlatform(From platform);
}
