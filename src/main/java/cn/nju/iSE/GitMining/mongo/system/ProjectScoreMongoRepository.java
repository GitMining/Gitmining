package cn.nju.iSE.GitMining.mongo.system;

import cn.nju.iSE.GitMining.model.system.ProjectScore;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProjectScoreMongoRepository extends MongoRepository<ProjectScore, Long> {
    ProjectScore findById(long id);

    ProjectScore findByProjectNameAndGroupName(String projectName, String groupName);

    List<ProjectScore> findByGroupName(String groupName);
}
