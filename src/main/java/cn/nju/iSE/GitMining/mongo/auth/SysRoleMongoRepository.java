package cn.nju.iSE.GitMining.mongo.auth;

import cn.nju.iSE.GitMining.model.auth.SysRole;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SysRoleMongoRepository extends MongoRepository<SysRole, Integer> {
    SysRole findById(int id);

    SysRole findByRoleName(String roleName);
}
