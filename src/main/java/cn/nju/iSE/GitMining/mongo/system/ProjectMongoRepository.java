package cn.nju.iSE.GitMining.mongo.system;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.system.Project;
import org.springframework.stereotype.Repository;

public interface ProjectMongoRepository extends MongoRepository<Project, Long> {

    Project findById(long id);

    List<Project> findByNamespace(String namespace);
    
    Page<Project> findAll(Pageable pageable);
    
    Project findByNameAndNamespace(String name, String namespace);

    List<Project> findAllByTimeBetween(long start, long end);
    
    Page<Project> findAllByTimeBetween(long start, long end, Pageable pageable);
}
