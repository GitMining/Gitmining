package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.loc.ProjectLOC;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.code.ProjectLOCMongoRepository.java
 *  
 *  2018年3月17日	上午10:24:23  
*/
public interface ProjectLOCMongoRepository extends MongoRepository<ProjectLOC, Long>{
	List<ProjectLOC> findByProjectName(String prjtName);
	List<ProjectLOC> findByProjectPath(String path);
	ProjectLOC findByGroupNameAndProjectName(String groupName, String projectName);
	ProjectLOC findById(Long id);
	long countByProjectPath(String path);
}
