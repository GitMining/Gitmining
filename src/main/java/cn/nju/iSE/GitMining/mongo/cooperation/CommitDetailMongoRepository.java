package cn.nju.iSE.GitMining.mongo.cooperation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.cooperation.CommitDetail;
import iSESniper.cooperation.entity.Author;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.cooperation.CommitDetailMongoRepository.java
 *  
 *  2018年4月23日	下午2:44:52  
*/
public interface CommitDetailMongoRepository extends MongoRepository<CommitDetail, String> {
	List<CommitDetail> findAllByProject(String project);
	
	CommitDetail findById(String id);
	
	List<CommitDetail> findByAuthor(Author author);
}
