package cn.nju.iSE.GitMining.mongo.auth;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.auth.UserInfo;

public interface UserInfoMongoRepository extends MongoRepository<UserInfo, String>{
    UserInfo findByUsername(String name);
    UserInfo findByUsernameAndPassword(String name, String password);
}
