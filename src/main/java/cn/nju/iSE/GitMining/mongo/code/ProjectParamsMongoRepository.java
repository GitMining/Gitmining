package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.vab.ProjectParams;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.code.ProjectParamsMongoRepository.java
 *  
 *  2018年3月19日	上午11:14:18  
*/
public interface ProjectParamsMongoRepository extends MongoRepository<ProjectParams, Long> {

	List<ProjectParams> findByProjectName(String prjtName);
	List<ProjectParams> findByProjectPath(String path);
	ProjectParams findById(Long id);
	ProjectParams findByGroupNameAndProjectName(String groupName, String projectName);
	long countByProjectPath(String path);
}
