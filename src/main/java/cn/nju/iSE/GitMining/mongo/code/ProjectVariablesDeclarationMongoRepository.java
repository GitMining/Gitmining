package cn.nju.iSE.GitMining.mongo.code;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.model.code.vab.ProjectVabsDec;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.code.ProjectVariablesDeclarationMongoRepository.java
 *  
 *  2018年3月19日	下午3:34:54  
*/
public interface ProjectVariablesDeclarationMongoRepository extends MongoRepository<ProjectVabsDec, Long> {

	List<ProjectVabsDec> findByProjectName(String prjtName);
	List<ProjectVabsDec> findByProjectPath(String path);
	ProjectVabsDec findById(Long id);
	ProjectVabsDec findByGroupNameAndProjectName(String groupName, String projectName);
	long countByProjectPath(String path);
}
