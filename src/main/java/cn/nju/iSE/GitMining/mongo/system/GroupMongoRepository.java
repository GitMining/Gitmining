package cn.nju.iSE.GitMining.mongo.system;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import cn.nju.iSE.GitMining.common.enumeration.From;
import cn.nju.iSE.GitMining.model.system.Group;
import org.springframework.stereotype.Repository;

/**
 *  @author   :   Magister
 *  @fileName :   cn.nju.iSE.GitMining.mongo.system.GroupMongoRepository.java
 *  
 *  2018年4月21日	下午8:41:03  
*/
public interface GroupMongoRepository extends MongoRepository<Group, Long> {

	Group findById(long id);
	
	Group findByOwnerName(String ownerName);
	
	List<Group> findAllByTimeBetween(long start, long end);
	
	Page<Group> findAllByTimeBetween(long start, long end, Pageable pageable);
	
	Page<Group> findAll(Pageable pageable);
	
	List<Group> findAllByPlatform(From platform);
}
