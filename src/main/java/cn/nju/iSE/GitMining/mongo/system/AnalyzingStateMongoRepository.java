package cn.nju.iSE.GitMining.mongo.system;

import cn.nju.iSE.GitMining.common.enumeration.State;
import cn.nju.iSE.GitMining.model.system.AnalyzingState;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnalyzingStateMongoRepository extends MongoRepository<AnalyzingState, Long> {
    /**
     * Find projects' analyzing state.
     * @param excludedState The state of being excluded.
     * @return The list of projects analyzing state.
     */
    List<AnalyzingState> findAllByStateNot(State excludedState);

    /**
     * Find projects' analyzing state.
     * @param state A state is needed.
     * @return The list of projects analyzing state.
     */
    List<AnalyzingState> findAllByState(State state);

    List<AnalyzingState> findAllByProjectNamespace(String namespace);

    List<AnalyzingState> findAllByGroupIdIn(List<Long> gids);

    AnalyzingState findById(long id);
}
