package cn.nju.iSE.GitMining.mongo.system;

import cn.nju.iSE.GitMining.model.system.GroupScore;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface GroupScoreMongoRepository extends MongoRepository<GroupScore, Long> {
    GroupScore findById(long id);

    GroupScore findByGroupName(String groupName);

    List<GroupScore> findByFinalScoreBetween(double start, double end);

    List<GroupScore> findAllByIdIn(List<Long> ids);
}
